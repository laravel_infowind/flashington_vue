import Vue from 'vue'
import Router from 'vue-router'
import Landing from './views/Landing.vue'
import {AppSettings} from "./config";
import ViewProfile from './views/profiles/ViewProfile'
import ManageProfile from "./views/profiles/ManageProfile";
import AddProfile from "./views/profiles/AddProfile";
import EditProfile from "./views/profiles/EditProfile";
import SingleChannel from "./views/SingleChannel"
import Home from "./views/Home"
// import {apiService} from "./api-service"
import PaymentOption from "./views/PaymentOption";
import PayPerView from "./views/PayPerView";
import Channels from "./views/Channels";
import EasyVideo from "./views/EasyVideo";
import Browse from "./views/Browse";
import Title from "./views/Title";
import Subscriptions from "./views/Subscriptions";
import Invoice from "./views/Invoice";
import ForgotPassword from "./views/ForgotPassword";
import AccountSetting from "./views/AccountSetting";
import PaymentDetails from "./views/PaymentDetails";
import EditAccount from "./views/EditAccount";
import DeleteAccount from "./views/DeleteAccount";
import BillingDetails from "./views/BillingDetails";
import SubscriptionSuccess from "./views/SubscriptionSuccess";
import GenreVideo from "./views/GenreVideo";
import TrailerVideo from "./views/TrailerVideo";
import AddCard from "./views/AddCard";
import Search from "./views/Search";
import Slider from "./components/Slider";
import CardList from "./views/CardList";
import CastCrew from "./views/CastCrew";
import PayPerViewSuccess from "./views/PayPerViewSuccess"
import PaymentFailure from "./views/PaymentFailure";
import Movies from "./views/Movies";
import Music from "./views/Music";
import Tvshow from "./views/Tvshow";
import Spamlist from "./views/Spamlist";


Vue.use(Router);
// const API = new apiService();
export default new Router({
    routes: [
        {
            path: '/',
            name: 'landing',
            component: Landing,
            meta: {
                title:  AppSettings.title + ' | Landing',
            }
        },
       

        {
            path: '/view-profiles',
            name: 'view-profiles',
            meta: {title: AppSettings.title + ' | profile'},
            component: ViewProfile,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },

        {
            path: '/manage-profiles',
            name: 'manage-profiles',
            meta: {title: AppSettings.title + ' | Manage profile'},
            component: ManageProfile,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },

        {
            path: '/add-profile',
            name: 'add-profile',
            meta: {title: AppSettings.title + ' | Add profile'},
            component: AddProfile,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/edit-profile/:id',
            name: 'edit-profile',
            meta: {title: AppSettings.title + ' | Edit profile'},
            component: EditProfile,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/payment-option/:id',
            name: 'payment-option',
            meta: {title: AppSettings.title + ' | Payment Option'},
            component: PaymentOption,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/video/:id',
            name: 'easy-video',
            meta: {title: AppSettings.title + ' | View Video'},
            component: EasyVideo,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/pay-per-view/:id',
            name: 'pay-per-view',
            meta: {title: AppSettings.title + ' | Pay Per View'},
            component: PayPerView,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/pay-per-view-success/:id',
            name: 'pay-per-view-success',
            meta: {title: AppSettings.title + ' | Pay Per View Success'},
            component: PayPerViewSuccess,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/home/:id',
            name: 'home',
            meta: {title: AppSettings.title + ' | Home'},
            component: Home,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/search/:id',
            name: 'search',
            meta: {title: AppSettings.title + ' | Search'},
            component: Search,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/browse/:id',
            name: 'browse',
            meta: {title: AppSettings.title + ' | Recently Added'},
            component: Browse,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/Movies/:id',
            name: 'movies',
            meta: {title: AppSettings.title + ' | Movies'},
            component: Movies,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/Music/:id',
            name: 'music',
            meta: {title: AppSettings.title + ' | Music'},
            component: Music,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
         {
            path: '/Tvshow/:id',
            name: 'tvshow',
            meta: {title: AppSettings.title + ' | TV Shows'},
            component: Tvshow,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/spamlist',
            name: 'spamlist',
            meta: {title: AppSettings.title + ' | Spamlist'},
            component: Spamlist,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/video/:id',
            name: 'video',
            meta: {title: AppSettings.title + ' | profile'},
            component: Home,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/channels',
            name: 'channels',
            meta: {title: AppSettings.title + ' | Channels'},
            component: Channels,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },

        {
            path: '/channel/:id',
            name: 'SingleChannel',
            meta: {title: AppSettings.title + ' | Single Channel'},
            component: SingleChannel,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/account-setting',
            name: 'account-setting',
            meta: {title: AppSettings.title + ' | profile'},
            component: Home,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path: '/payment-details',
            name: 'payment-details',
            meta: {title: AppSettings.title + ' | Payment-Details'},
            component: PaymentDetails,
            // beforeEnter: (to, from, next) => {
            //     if(!API.checkAuth()) next('/');
            //     else next();
            // }
        },
        {
            path:'/title/:title',
            name: 'title',
            meta: {title: AppSettings.title + ' | Wishlist'},
            component: Title,
        },
        {
            path:'/title/:title/:id',
            name: 'title',
            meta: {title: AppSettings.title + ' | Category'},
            component: Title,
        },
        {
            path:'/subscriptions/:id',
            name: 'subscriptions',
            meta: {title: AppSettings.title + ' | Subscriptions'},
            component: Subscriptions,
        },
        {
            path:'/invoice/:id',
            name: 'invoice',
            meta: {title: AppSettings.title + ' | Invoices'},
            component: Invoice,
        },
        {
            path:'/forgot-password/',
            name: 'ForgotPassword',
            meta: {title: AppSettings.title + ' | Forgot Password'},
            component: ForgotPassword,
        },
        {
            path:'/change-password/:id',
            name: 'ChangePassword',
            meta: {title: AppSettings.title + ' | Change Password'},
            component: ForgotPassword,
        },
        {
            path:'/account-setting/:id',
            name: 'AccountSetting',
            meta: {title: AppSettings.title + ' | Account Setting'},
            component: AccountSetting,
        },
        {
            path:'/edit-account/:id',
            name: 'AccountSetting',
            meta: {title: AppSettings.title + ' | Account Setting'},
            component: EditAccount,
        },
        {
            path:'/delete-account/:id',
            name: 'AccountSetting',
            meta: {title: AppSettings.title + ' | Account Setting'},
            component: DeleteAccount,
        },
        {
            path:'/billing-details/:id',
            name: 'BillingDetails',
            meta: {title: AppSettings.title + ' | Billing Details'},
            component: BillingDetails,
        },
        {
            path:'/subscription-success',
            name: 'SubscriptionSuccess',
            meta: {title: AppSettings.title + ' | Subscription Success'},
            component: SubscriptionSuccess,
        },
        {
            path:'/trailer_video/:id',
            name: 'TrailerVideo',
            meta: {title: AppSettings.title + ' | Trailer Video'},
            component: TrailerVideo,
        },
        {
            path:'/genre-video',
            name: 'GenreVideo',
            meta: {title: AppSettings.title + ' | Subscription Success'},
            component: GenreVideo,
        },
        {
            path:'/add-card/:id',
            name: 'AddCard',
            meta: {title: AppSettings.title + ' | Add Card'},
            component: AddCard,
        },
        {
            path:'/card-list/:id',
            name: 'CardList',
            meta: {title: AppSettings.title + ' | Card List'},
            component: CardList,
        },
        {
            path:'/slider',
            name: 'Slider',
            meta: {title: AppSettings.title + ' | Slider'},
            component: Slider,
        },
        {
            path:'/actor/:id',
            name: 'CastCrew',
            meta: {title: AppSettings.title + ' | Actors'},
            component: CastCrew,
        },
        {
            path:'/writer/:id',
            name: 'CastCrew',
            meta: {title: AppSettings.title + ' | Writers'},
            component: CastCrew,
        },
        {
            path:'/director/:id',
            name: 'CastCrew',
            meta: {title: AppSettings.title + ' | Directors'},
            component: CastCrew,
        },
        {
            path:'/payment-failure',
            name: 'PaymentFailure',
            meta: {title: AppSettings.title + ' | Payment Failure'},
            component: PaymentFailure,
        },
        {
            path: '**', 
            redirect: '/'
        },
    ]
})

// import {apiService} from './api-service.js';
// let APIService = new apiService();
