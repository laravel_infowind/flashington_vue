/* Main */
(function (window){
	
	var FWDUVPlayer = function(props){
		
		var self = this;
		this.isInstantiate_bl = false;
		this.displayType = props.displayType || FWDUVPlayer.RESPONSIVE;
		
		if(self.displayType.toLowerCase() != FWDUVPlayer.RESPONSIVE 
		   && self.displayType.toLowerCase() != FWDUVPlayer.FULL_SCREEN
		   && self.displayType.toLowerCase() != FWDUVPlayer.STICKY
		   && self.displayType.toLowerCase() != FWDUVPlayer.AFTER_PARENT
		   && self.displayType.toLowerCase() != FWDUVPlayer.LIGHTBOX){
			self.displayType = FWDUVPlayer.RESPONSIVE;
		}
		self.displayType = self.displayType.toLowerCase();
		
		this.stickyOnScroll = props.stickyOnScroll || "no";
		this.stickyOnScroll = self.stickyOnScroll == "yes" ? true : false;
		if(self.displayType != FWDUVPlayer.RESPONSIVE) this.stickyOnScroll = false;
		self.isMinShowed = true;
		
		self.stickyOnScrollWidth = props.stickyOnScrollWidth || 700;
		self.stickyOnScrollHeight = props.stickyOnScrollHeight || 394; 

		this.maxWidth = props.maxWidth || 640;
		this.maxHeight = props.maxHeight || 380;
		this.embeddedPlaylistId;
		this.embeddedVideoId;
		this.isEmbedded_bl = false;
		
		this.useYoutube_bl = props.useYoutube || "no"; 
		this.useYoutube_bl = self.useYoutube_bl == "yes" ? true : false;
		
		this.useVimeo_bl = props.useVimeo || "no"; 
		this.useVimeo_bl = self.useVimeo_bl == "yes" ? true : false;
		
		self.mainFolderPath_str = props.mainFolderPath;
		if((self.mainFolderPath_str.lastIndexOf("/") + 1) != self.mainFolderPath_str.length){
			self.mainFolderPath_str += "/";
		}
		
		this.skinPath_str = props.skinPath;
		if((self.skinPath_str.lastIndexOf("/") + 1) != self.skinPath_str.length){
			self.skinPath_str += "/";
		}
		
		this.warningIconPath_str = self.mainFolderPath_str + this.skinPath_str + "warningIcon.png";

		if(!FWDUVPlayer.iFrame && FWDUVPUtils.isChrome && !FWDUVPUtils.isMobile){
			FWDUVPlayer.iFrame = document.createElement("iframe");
			FWDUVPlayer.iFrame.src = self.mainFolderPath_str + 'audio/silent.mp3';
			FWDUVPlayer.iFrame.style.position = 'absolute';
			FWDUVPlayer.iFrame.style.top = '-500px';
			document.documentElement.appendChild(FWDUVPlayer.iFrame);
		}
		
		FWDUVPlayer.YTAPIReady = false;
		this.fillEntireVideoScreen_bl = false;
	
		/* init gallery */
		self.init = function(){
			if(self.isInstantiate_bl) return;
			FWDUVPlayer.instaces_ar.push(this);
			
			FWDTweenLite.ticker.useRAF(false);
			this.props_obj = props;
			
			this.mustHaveHolderDiv_bl = false;
			this.instanceName_str = this.props_obj.instanceName;
			
			if(!this.instanceName_str){
				alert("FWDUVPlayer instance name is required please make sure that the instanceName parameter exsists and it's value is uinique.");
				return;
			}
			
			if(window[this.instanceName_str]){
				alert("FWDUVPlayer instance name " + this.instanceName_str +  " is already defined and contains a different instance reference, set a different instance name.");
				return;
			}else{
				window[this.instanceName_str] = this;
			}
		
			if(!this.props_obj){
				alert("FWDUVPlayer constructor properties object is not defined!");
				return;
			}
			
			if(!this.props_obj.parentId){		
				alert("Property parentId is not defined in the FWDUVPlayer constructor, self property represents the div id into which the megazoom is added as a child!");
				return;
			}
			
			if(self.displayType == FWDUVPlayer.RESPONSIVE) self.mustHaveHolderDiv_bl = true;
		
			if(self.mustHaveHolderDiv_bl && !FWDUVPUtils.getChildById(self.props_obj.parentId)){
				alert("FWDUVPlayer holder div is not found, please make sure that the div exsists and the id is correct! " + self.props_obj.parentId);
				return;
			}
			
			this.body = document.getElementsByTagName("body")[0];
			if(self.displayType == FWDUVPlayer.STICKY){
				this.stageContainer = document.createElement("div");
				this.stageContainer.style.position = "fixed";
				this.stageContainer.style.width = "100%";
				this.stageContainer.style.zIndex = "999999";
				this.stageContainer.style.height = "0px";
			
				document.documentElement.appendChild(this.stageContainer);
				this.stageContainer.style.overflow = "visible";
				
			}else if(self.displayType == FWDUVPlayer.FULL_SCREEN  || self.displayType == FWDUVPlayer.LIGHTBOX){
				self.stageContainer = document.documentElement;
			}else{
				this.stageContainer = FWDUVPUtils.getChildById(self.props_obj.parentId);
			}
		
			this.position_str = self.props_obj.verticalPosition;
			if(!this.position_str) this.position_str = FWDUVPlayer.POSITION_TOP;
			if(this.position_str == "bottom"){
				this.position_str = FWDUVPlayer.POSITION_BOTTOM;
			}else{
				this.position_str = FWDUVPlayer.POSITION_TOP;
			}
			
			this.horizontalPosition_str = self.props_obj.horizontalPosition;
			if(!this.horizontalPosition_str) this.horizontalPosition_str = FWDUVPlayer.CENTER;
			if(this.horizontalPosition_str == "center"){
				this.horizontalPosition_str = FWDUVPlayer.CENTER;
			}else if(this.horizontalPosition_str == "left"){
				this.horizontalPosition_str = FWDUVPlayer.LEFT;
			}else if(this.horizontalPosition_str == "right"){
				this.horizontalPosition_str = FWDUVPlayer.RIGHT;
			}else{
				this.horizontalPosition_str = FWDUVPlayer.CENTER;
			}
			
			self.isMin = false;
			this.lightBox_do = null;
			this.listeners = {events_ar:[]};
			this.customContextMenu_do = null;
			this.info_do = null;
			this.categories_do = null;
			this.playlist_do = null;
			this.main_do = null;
			this.ytb_do = null;
			this.preloader_do = null;
			this.controller_do = null;
			this.videoScreen_do = null;
			this.flash_do = null;
			this.flashObject = null;
			this.videoPoster_do = null;
			this.largePlayButton_do = null;
			this.hider = null;
			this.videoHolder_do = null;
			this.videoHider_do = null;
			this.disableClick_do = null;
			this.embedWindow_do = null;
			this.spaceBetweenControllerAndPlaylist = self.props_obj.spaceBetweenControllerAndPlaylist || 1;
			this.autoScale_bl = self.props_obj.autoScale;
			this.autoScale_bl = self.autoScale_bl == "yes" ? true : false;
			
			self.showPreloader_bl = self.props_obj.showPreloader;
			self.showPreloader_bl = self.showPreloader_bl == "yes" ? true : false;
			
			self.preloaderColors = self.props_obj.preloaderColors || ["#666666", "#FFFFFF"];
			
			this.backgroundColor_str = self.props_obj.backgroundColor || "transparent";
			this.videoBackgroundColor_str = self.props_obj.videoBackgroundColor || "transparent";
			this.flashObjectMarkup_str =  null;
			
			self.mainBackgroundImagePath_str = self.props_obj.mainBackgroundImagePath;
			if(self.mainBackgroundImagePath_str && self.mainBackgroundImagePath_str.length < 3) self.mainBackgroundImagePath_str = undefined;
			
			self.animate_bl = self.props_obj.animatePlayer; 
			self.animate_bl = self.animate_bl == "yes" ? true : false;
			this.isShowedFirstTime_bl = true;
			
			this.offsetX = parseInt(self.props_obj.offsetX) || 0;
			this.offsetY = parseInt(self.props_obj.offsetY) || 0
			this.lastX = 0;
			this.lastY = 0;
			this.tempStageWidth = 0;
			this.tempStageHeight = 0;
			this.tempVidStageWidth = 0;
			this.tempVidStageHeight = 0;
			this.stageWidth = 0;
			this.stageHeight = 0;
			this.vidStageWidth = 0;
			this.vidStageHeight = 0;
			this.firstTapX;
			this.firstTapY;
			this.curTime;
			this.totalTime;
			this.catId = -1;
			this.id = -1;
			this.totalVideos = 0;
			this.prevCatId = -1;
			this.totalTimePlayed = 0;
			
			this.videoSourcePath_str = "";
			this.prevVideoSourcePath_str;
			this.posterPath_str = self.props_obj.posterPath;
			this.videoType_str;
			this.videoStartBehaviour_str;
			this.prevVideoSource_str;
			this.prUVPosterSource_str;
			this.finalVideoPath_str;
			this.playListThumbnailWidth = self.props_obj.thumbnailWidth || 80;
			this.playListThumbnailHeight = self.props_obj.thumbnailHeight || 80;
			this.playlistWidth = self.props_obj.playlistRightWidth || 250;
			this.playlistHeight = 0;
		
			this.resizeHandlerId_to;
			this.resizeHandler2Id_to;
			this.hidePreloaderId_to;
			this.orientationChangeId_to;
			this.disableClickId_to;
			this.clickDelayId_to;
			this.secondTapId_to;
			this.videoHiderId_to;
			
			this.showPlaylistButtonAndPlaylist_bl = self.props_obj.showPlaylistButtonAndPlaylist;
			this.showPlaylistButtonAndPlaylist_bl = self.showPlaylistButtonAndPlaylist_bl == "no" ? false : true;
			
			this.isPlaylistShowed_bl = self.props_obj.showPlaylistByDefault;
			this.isPlaylistShowed_bl = self.isPlaylistShowed_bl == "no" ? false : true;
			
			self.showErrorInfo_bl = self.props_obj.showErrorInfo; 
			self.showErrorInfo_bl = self.showErrorInfo_bl == "no" ? false : true;
			
			self.showAnnotationsPositionTool_bl =  self.props_obj.showAnnotationsPositionTool;
			self.showAnnotationsPositionTool_bl = self.showAnnotationsPositionTool_bl == "yes" ? true : false;
			if(self.showAnnotationsPositionTool_bl) self.isPlaylistShowed_bl = false;
			
			//this.playlistPosition_str = self.props_obj.playlistPosition || "bottom";
			//var test = self.playlistPosition_str == "bottom" || self.playlistPosition_str == "right";		   
			//if(!test) self.playlistPosition_str = "right";
			
			if(FWDUVPlayer.videoStartBehaviour != "pause" 
			&& FWDUVPlayer.videoStartBehaviour != "stop"
			&& FWDUVPlayer.videoStartBehaviour != "default"
			){
				FWDUVPlayer.videoStartBehaviour = "pause";
			}
			
			this.lightBoxBackgroundOpacity = self.props_obj.lightBoxBackgroundOpacity || 1;
			this.lightBoxBackgroundColor_str = self.props_obj.lightBoxBackgroundColor || "transparent";
			self.preloaderBackgroundColor = self.props_obj.preloaderBackgroundColor || "#000000";
			self.preloaderFillColor = self.props_obj.preloaderFillColor || "#FFFFFF";
			self.addPrevId = Math.random() * 999999999;
			this.isVideoPlayingWhenOpenWindows_bl = false;
			this.isFirstPlaylistLoaded_bl = false;
			this.isVideoHiderShowed_bl = false;
			this.isSpaceDown_bl = false;
			this.isPlaying_bl = false;
			this.firstTapPlaying_bl = false;
			this.stickOnCurrentInstanceKey_bl = false;
			this.isFullScreen_bl = false;
			this.isFlashScreenReady_bl = false;
			this.orintationChangeComplete_bl = true;
			this.disableClick_bl = false;
			this.isAPIReady_bl = false;
			this.isInstantiate_bl = true;
			this.isPlaylistLoaded_bl = false;
			this.isPlaylistLoadedFirstTime_bl = false;
			this.useDeepLinking_bl = self.props_obj.useDeepLinking;
			this.useDeepLinking_bl = self.useDeepLinking_bl == "yes" ? true : false;
			this.isAdd_bl = false;
			
			this.isMobile_bl = FWDUVPUtils.isMobile;
			this.hasPointerEvent_bl = FWDUVPUtils.hasPointerEvent;
			
			this.lightBoxWidth = self.props_obj.maxWidth || 500;
			this.lightBoxHeight =  self.props_obj.maxHeight || 400;
			
			self.isShowed_bl = self.props_obj.showPlayerByDefault; 
			self.isShowed_bl = self.isShowed_bl == "yes" ? true : false;
			
			self.googleAnalyticsTrackingCode = self.props_obj.googleAnalyticsTrackingCode; 
			if(!window["ga"] && self.googleAnalyticsTrackingCode){
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
				ga('create', self.googleAnalyticsTrackingCode, 'auto');
				ga('send', 'pageview');
			}else if(window["ga"] && self.googleAnalyticsTrackingCode){
				ga('create', self.googleAnalyticsTrackingCode, 'auto');
				ga('send', 'pageview');
			}
		
			if(self.displayType == FWDUVPlayer.LIGHTBOX){
				self.setupLightBox();
			}else if(self.displayType == FWDUVPlayer.STICKY){
				self.setupPlayer();
				this.startResizeHandler();
			}else{
				if(self.initializeOnlyWhenVisible_bl){
					this.startResizeHandler();
					window.addEventListener("scroll", self.onInitlalizeScrollHandler);
					setTimeout(self.onInitlalizeScrollHandler, 500);
				}else{
					self.setupPlayer();
					this.startResizeHandler();
				}
			}
		};

		self.addMinOnScroll = function(){
			if(self.displayType != FWDUVPlayer.RESPONSIVE) return;
			if(self.stickyOnScroll) window.addEventListener("scroll", self.minimizeOnScrollHandler);
		}

		self.removeMinOnScroll = function(){
			if(self.stickyOnScroll) window.removeEventListener("scroll", self.minimizeOnScrollHandler);
		}

		self.minimizeOnScrollHandler = function(e){
			var scrollOffsets = FWDUVPUtils.getScrollOffsets();
			self.pageXOffset = scrollOffsets.x;
			self.pageYOffset = scrollOffsets.y;
			
			if(self.stageContainer.getBoundingClientRect().bottom < 0){
				self.setMinimized();
			}else{
				self.setNormal();
			}
		}

		self.setMinimized = function(){
			if(self.isMin || self.isFullscreen_bl) return;
			self.isMin = true;
			self.main_do.getStyle().position = 'fixed';
			self.main_do.getStyle().zIndex = 9999999999999;
			self.main_do.setAlpha(0);
			self.startPosisionOnMin();
		}

		self.startPosisionOnMin = function(){
			self.wasPlaylistShowed_bl = self.isPlaylistShowed_bl;
			self.showPlaylist();
			self.resizeHandler();
			self.positionOnMin();
		}

		self.setNormal = function(){
			if(!self.isMin) return;
			self.isMinShowed = true;
			self.isMin = false;
			self.main_do.getStyle().position = "relative";
			self.main_do.getStyle().zIndex = 0;
			FWDAnimation.killTweensOf(self.main_do);
			self.main_do.setAlpha(1);
			self.main_do.setX(0);
			self.main_do.setY(0);
			if(self.opener_do) self.opener_do.setX(-1000);
						
			self.startPosisionOnNormal();
		}

		self.startPosisionOnNormal = function(){
			if(self.opener_do) self.opener_do.showCloseButton();
			self.isPlaylistShowed_bl = self.wasPlaylistShowed_bl;
			if(self.isPlaylistShowed_bl) self.hidePlaylist();
			self.resizeHandler();
		}
		
		self.positionOnMin = function(animate){
			if(!self.isMin && !animate) return;
			var offset = 5;
			var dl = .2;
			if(self.isMobile_bl) offset= 0;
			var offsetTop = 0;
			if(!self.isMinShowed){
				dl = 0;
				offsetTop = Math.round(self.tempStageHeight) + offset;
			} 

			if(self.opener_do){
				var oX = self.ws.w - self.opener_do.w - offset;
				var oY = self.ws.h - self.tempStageHeight - offset + offsetTop - self.opener_do.h;
			}
			
			self.main_do.setX(self.ws.w - self.tempStageWidth - offset);
			if(self.main_do.alpha == 0 || animate){
				if(self.main_do.alpha == 0){
					self.main_do.setY(self.ws.h);
					if(self.opener_do){
						self.opener_do.setX(oX);
						self.opener_do.setY(self.ws.h);
					}
				}
				FWDAnimation.to(self.main_do, .8, {alpha:1, y:self.ws.h - self.tempStageHeight - offset + offsetTop, delay:dl, ease:Expo.easeInOut});
				if(self.opener_do){
					FWDAnimation.killTweensOf(self.opener_do);
					FWDAnimation.to(self.opener_do, .8, {x:oX, y:oY, delay:dl, ease:Expo.easeInOut});
				}
			}else{
				
				FWDAnimation.killTweensOf(self.main_do);
				self.main_do.setAlpha(1);
				self.main_do.setY(self.ws.h - self.tempStageHeight - offset + offsetTop);
				if(self.opener_do){
					FWDAnimation.killTweensOf(self.opener_do);
					self.opener_do.setX(oX);
					self.opener_do.setY(oY);
				}
			}			
		}

		self.onInitlalizeScrollHandler = function(){
			var scrollOffsets = FWDUVPUtils.getScrollOffsets();
			self.pageXOffset = scrollOffsets.x;
			self.pageYOffset = scrollOffsets.y;
			
			if(self.main_do.getRect().top >= -self.stageHeight && self.main_do.getRect().top < self.ws.h){
				window.removeEventListener("scroll", self.onInitlalizeScrollHandler);
				self.setupPlayer();
			}
		};
		
		this.setupPlayer = function(){
			if(!self.main_do){
				self.setupMainDo();
				self.setupInfo();
				self.setupData();
			}
		}
		
		//#############################################//
		/* setup  lighbox...*/
		//#############################################//
		self.setupLightBox = function(){
			
			FWDUVPLightBox.setPrototype();
			self.lightBox_do =  new FWDUVPLightBox(self, 
					self.lightBoxBackgroundColor_str, 
					self.backgroundColor_str, 
					self.lightBoxBackgroundOpacity, 
					self.lightBoxWidth, 
					self.lightBoxHeight);
					
			self.lightBox_do.addListener(FWDUVPLightBox.SHOW, self.lightBoxShowHandler);
			self.lightBox_do.addListener(FWDUVPLightBox.CLOSE, self.lightBoxCloseHandler);
			self.lightBox_do.addListener(FWDUVPLightBox.HIDE_COMPLETE, self.lightBoxHideCompleteHandler);
			self.lighboxAnimDoneId_to = setTimeout(self.setupPlayer, 1200);
		};
		
		self.lightBoxShowHandler = function(){
			//self.startResizeHandler();
		}
		
		self.lightBoxCloseHandler = function(){
		
			self.stop();
			self.stopResizeHandler();
		};
		
		self.lightBoxHideCompleteHandler = function(){
			self.dispatchEvent(FWDUVPlayer.HIDE_LIGHTBOX_COMPLETE);
		};
	
		//#############################################//
		/* setup main do */
		//#############################################//
		self.setupMainDo = function(){
		
			self.main_do = new FWDUVPDisplayObject("div", "relative");
			if(self.hasPointerEvent_bl) self.main_do.getStyle().touchAction = "none";
			self.main_do.getStyle().webkitTapHighlightColor = "rgba(0, 0, 0, 0)";
			self.main_do.getStyle().webkitFocusRingColor = "rgba(0, 0, 0, 0)";
			self.main_do.getStyle().width = "100%";
			self.main_do.getStyle().height = "100%";
			self.main_do.setBackfaceVisibility();
			self.main_do.setBkColor(self.backgroundColor_str);
			if(!FWDUVPUtils.isMobile || (FWDUVPUtils.isMobile && FWDUVPUtils.hasPointerEvent)) self.main_do.setSelectable(false);	
			self.videoHolder_do = new FWDUVPDisplayObject("div");
			self.main_do.addChild(self.videoHolder_do);

			if(self.displayType ==  FWDUVPlayer.STICKY){
				self.background_do = new FWDUVPDisplayObject("div");
				self.background_do.getStyle().width = "100%";
				if(self.mainBackgroundImagePath_str){
					self.mainBackground_do =  new FWDUVPDisplayObject("div");
					self.stageContainer.appendChild(self.mainBackground_do.screen);
				}
				self.stageContainer.appendChild(self.background_do.screen);
				self.stageContainer.appendChild(self.main_do.screen);
			}else if(self.displayType == FWDUVPlayer.FULL_SCREEN){	
				self.stageContainer.style.overflow = "hidden";
				self.main_do.getStyle().position = "absolute";
				document.documentElement.appendChild(self.main_do.screen);
				self.stageContainer.style.zIndex = 9999999999998;
				self.main_do.getStyle().zIndex = 9999999999998;
			}else if(self.displayType == FWDUVPlayer.BACKGROUND_VIDEO){	
				document.documentElement.appendChild(self.main_do.screen);
				self.main_do.getStyle().zIndex = -9999999999998;
				self.main_do.getStyle().position = "fixed";
				document.documentElement.insertBefore(self.main_do.screen, document.documentElement.firstChild);
			}else if(self.displayType == FWDUVPlayer.LIGHTBOX){
				self.main_do.getStyle().position = "absolute";
				self.stageContainer.appendChild(self.main_do.screen);
				self.main_do.setX(-10000);
				self.main_do.setY(-10000);
				self.main_do.setWidth(0);
				self.main_do.setHeight(0);
			}else{
				self.stageContainer.style.overflow = "hidden";
				self.stageContainer.appendChild(self.main_do.screen);
			}	

			if(self.isEmbedded_bl) self.main_do.getStyle().zIndex = 9999999999998;
		};
		
		
		//#############################################//
		/* setup info_do */
		//#############################################//
		self.setupInfo = function(){
			FWDUVPInfo.setPrototype();
			self.info_do = new FWDUVPInfo(self, self.warningIconPath_str, self.showErrorInfo_bl);
			self.info_do.getStyle().zIndex = "9999999999999999";
		};		
		
		//#############################################//
		/* resize handler */
		//#############################################//
		self.startResizeHandler = function(){
			
			if(self.displayType == FWDUVPlayer.STICKY){
				if(FWDUVPUtils.isAndroid) window.addEventListener("orientationchange", self.orientationChange);
				window.addEventListener("scroll", self.onScrollHandler);
			}
			
			if(self.displayType == FWDUVPlayer.LIGHTBOX){
				window.addEventListener("scroll", self.onScrollHandler);
			}
			
			window.addEventListener("resize", self.onResizeHandler);
		
			self.onResizeHandler(true);
			self.resizeHandlerId_to = setTimeout(function(){self.resizeHandler();}, 500);
		};
		
		this.orientationChange = function(){
			self.orintationChangeComplete_bl = false;	
			clearTimeout(self.resizeHandlerId_to);
			clearTimeout(self.resizeHandler2Id_to);
			clearTimeout(self.orientationChangeId_to);
		
			self.orientationChangeId_to = setTimeout(function(){
				self.orintationChangeComplete_bl = true; 
				self.resizeHandler(true);
				}, 1000);
			
			self.stageContainer.style.left = "-5000px";
			if(self.preloader_do) self.preloader_do.setX(-5000);	
		};
		
		self.onScrollHandler = function(e){
			if(self.displayType == FWDUVPlayer.STICKY) self.onResizeHandler();
			if(self.lightbox_do && !self.lightbox_do.isShowed_bl) return;
			self.scrollHandler();
			var scrollOffsets = FWDUVPUtils.getScrollOffsets();
			self.scrollOffsets = scrollOffsets;
		};
		
		self.scrollHandler = function(){
			var scrollOffsets = FWDUVPUtils.getScrollOffsets();
			self.pageXOffset = scrollOffsets.x;
			self.pageYOffset = scrollOffsets.y;
			if(self.displayType == FWDUVPlayer.LIGHTBOX){
				self.lightBox_do.setX(scrollOffsets.x);
				self.lightBox_do.setY(scrollOffsets.y);
			}else if(self.isFullScreen_bl || self.displayType == FWDUVPlayer.FULL_SCREEN){	
				self.main_do.setX(scrollOffsets.x);
				self.main_do.setY(scrollOffsets.y);
			}
		};
		
		self.stopResizeHandler = function(){
			if(window.removeEventListener){
				window.removeEventListener("resize", self.onResizeHandler);
			}else if(window.detachEvent){
				window.detachEvent("onresize", self.onResizeHandler);
			}	
			clearTimeout(self.resizeHandlerId_to);
		};

		
		self.onResizeHandler = function(e){
			self.resizeHandler();
			clearTimeout(self.resizeHandler2Id_to);
			self.resizeHandler2Id_to = setTimeout(function(){self.resizeHandler();}, 300);
		};
	
		self.resizeHandler = function(allowToResizeFinal, resizePlaylistWithAnim){
			self.tempPlaylistPosition_str;
			
			var viewportSize = FWDUVPUtils.getViewportSize();
			var scrollOffsets = FWDUVPUtils.getScrollOffsets();
			self.ws = viewportSize;
			
			if(self.displayType == FWDUVPlayer.STICKY  && !self.isFullScreen_bl){	
				self.main_do.getStyle().width = "100%";
				if(self.main_do.getWidth() > self.maxWidth){
					self.main_do.setWidth(self.maxWidth);
				}
				
				self.stageWidth = self.main_do.getWidth();
				if(self.autoScale_bl){
					self.stageHeight = parseInt(self.maxHeight * (self.stageWidth/self.maxWidth));
				}else{
					self.stageHeight = self.maxHeight;
				}
			
			}else if(self.displayType == FWDUVPlayer.LIGHTBOX && !self.isFullScreen_bl){
				if(!self.lightBox_do.isShowed_bl ||  !self.main_do) return;
				if(self.lightBoxWidth > viewportSize.w){
					self.finalLightBoxWidth = viewportSize.w;
					self.finalLightBoxHeight = parseInt(self.lightBoxHeight * (viewportSize.w/self.lightBoxWidth));
				}else{
					self.finalLightBoxWidth = self.lightBoxWidth;
					self.finalLightBoxHeight = self.lightBoxHeight;
				}
				self.lightBox_do.setWidth(viewportSize.w);
				self.lightBox_do.setHeight(viewportSize.h);
				self.lightBox_do.setX(scrollOffsets.x);
				self.lightBox_do.setY(scrollOffsets.y);
				self.lightBox_do.mainLightBox_do.setX(parseInt((viewportSize.w - self.finalLightBoxWidth)/2));
				self.lightBox_do.mainLightBox_do.setY(parseInt((viewportSize.h - self.finalLightBoxHeight)/2));
				if(self.lightBox_do.closeButton_do && self.lightBox_do.isShowed_bl){ 
					self.lightBox_do.closeButton_do.setX(viewportSize.w - self.lightBox_do.closeButton_do.w - 4);
					self.lightBox_do.closeButton_do.setY(4);
				}
				self.main_do.setX(0);
				self.main_do.setY(0);
				self.lightBox_do.mainLightBox_do.setWidth(self.finalLightBoxWidth);
				self.lightBox_do.mainLightBox_do.setHeight(self.finalLightBoxHeight);	
				self.stageWidth = self.finalLightBoxWidth;
				self.stageHeight = self.finalLightBoxHeight;
			}else if(self.isFullScreen_bl || self.displayType == FWDUVPlayer.FULL_SCREEN){	
				self.main_do.setX(0);
				self.main_do.setY(0);
				self.stageWidth = viewportSize.w;
				self.stageHeight = viewportSize.h;
			}else if(self.displayType == FWDUVPlayer.AFTER_PARENT){
				self.main_do.setX(0);
				self.main_do.setY(0);
				self.stageWidth = self.stageContainer.offsetWidth;
				self.stageHeight = self.stageContainer.offsetHeight;
				
			}else{
				self.stageContainer.style.width = "100%";
				if(self.stageContainer.offsetWidth > self.maxWidth){
					self.stageContainer.style.width = self.maxWidth + "px";
				}
				self.stageWidth = self.stageContainer.offsetWidth;
				if(self.autoScale_bl){
					self.stageHeight = parseInt(self.maxHeight * (self.stageWidth/self.maxWidth));
					self.tempStageHeight = self.stageHeight;
				}else{
					self.stageHeight = self.maxHeight;
					self.tempStageHeight = self.stageHeight;
				}
			}

			
			
			if(FWDUVPUtils.isIEAndLessThen9 && self.stageWidth < 400) self.stageWidth = 400;
			//if(self.stageHeight < 320) self.stageHeight = 320;
			
			if(self.stageHeight > viewportSize.h && self.isFullScreen_bl) self.stageHeight = viewportSize.h;
			if(self.data && self.playlist_do){
				self.playlistHeight = parseInt(self.data.playlistBottomHeight * (self.stageWidth/self.maxWidth));
				//if(self.playlistHeight < 200) self.playlistHeight = 200;
			}

			if(self.isMin && !self.isFullScreen_bl){
				self.stageWidth = Math.min(self.stickyOnScrollWidth - 10, self.ws.w - 10)
				self.stageHeight = parseInt(self.stickyOnScrollHeight * (self.stageWidth/self.stickyOnScrollWidth));
				self.tempStageHeight = self.stageHeight;
			}
		
			
			if(self.data){
				self.tempPlaylistPosition_str = self.data.playlistPosition_str;
				if(self.stageWidth < 600){
					self.tempPlaylistPosition_str = "bottom";
				}
				self.playlistPosition_str = self.tempPlaylistPosition_str;
				if(self.playlist_do) self.playlist_do.position_str = self.tempPlaylistPosition_str;
			}
			
			if(self.playlist_do && self.isPlaylistShowed_bl){
				if(self.playlistPosition_str == "bottom"){
					self.vidStageWidth = self.stageWidth;
					self.stageHeight += self.playlistHeight + self.spaceBetweenControllerAndPlaylist;
					self.vidStageHeight = self.stageHeight - self.playlistHeight - self.spaceBetweenControllerAndPlaylist;
					if(self.displayType == FWDUVPlayer.FULL_SCREEN) self.controller_do.disablePlaylistButton();
				}else if(self.playlistPosition_str == "right"){
					if(self.isFullScreen_bl){
						self.vidStageWidth = self.stageWidth;
					}else{
						self.vidStageWidth = self.stageWidth - self.playlistWidth - self.spaceBetweenControllerAndPlaylist;
					}
					if(self.controller_do) self.controller_do.enablePlaylistButton();
					self.vidStageHeight = self.stageHeight;
				}
			}else{
				self.vidStageWidth = self.stageWidth;
				self.vidStageHeight = self.stageHeight;
			}
				
			if(self.controller_do){
				if(self.playlist_do){
					if(self.playlistPosition_str == "right"){
						if(self.isFullScreen_bl){
							self.controller_do.disablePlaylistButton();
						}else{
							self.controller_do.enablePlaylistButton();
						}
					}else if(self.isEmbedded_bl){
						self.controller_do.disablePlaylistButton();
					}
				}
			}
			
			if(self.mainBackground_do){
				self.mainBackground_do.setWidth(self.ws.w);
				self.mainBackground_do.setHeight(self.stageHeight);
			}
			
			if(!allowToResizeFinal || self.isMobile_bl){
				FWDAnimation.killTweensOf(self);
				
				self.tempStageWidth = self.stageWidth;
				self.tempStageHeight = self.stageHeight;
				self.tempVidStageWidth = self.vidStageWidth;
				self.tempVidStageHeight = Math.max(0, self.vidStageHeight);
				
				self.resizeFinal(resizePlaylistWithAnim);
				self.setStageContainerFinalHeightAndPosition(resizePlaylistWithAnim);
			}	
			
		};

		
		this.resizeFinal = function(resizePlaylistWithAnim){
		
			if(self.displayType != FWDUVPlayer.STICKY && !self.isMin) self.stageContainer.style.height = self.tempStageHeight + "px";

			if(self.mainBackground_do){
				self.mainBackground_do.setWidth(self.ws.w);
				self.mainBackground_do.setHeight(self.tempStageHeight);
			}
			
			self.main_do.setWidth(self.tempStageWidth);
			self.videoHolder_do.setWidth(self.tempVidStageWidth);
			self.videoHolder_do.setHeight(self.tempVidStageHeight);
			
			if(self.showPlaylistButtonAndPlaylist_bl && self.isPlaylistShowed_bl && self.playlistPosition_str == "bottom"){
				self.main_do.setHeight(self.tempStageHeight);
			}else{
				self.main_do.setHeight(self.tempStageHeight);
			}
			
			if(self.displayType == FWDUVPlayer.LIGHTBOX) self.lightBox_do.mainLightBox_do.setY(parseInt((self.ws.h - self.tempStageHeight)/2));
			
			/*
			if(self.flash_do && self.videoType_str == FWDUVPlayer.HLS_JS){
				self.flash_do.setWidth(self.tempVidStageWidth);
				self.flash_do.setHeight(self.tempVidStageHeight);
			}else if(self.flash_do){
				self.flash_do.setWidth(1);
				self.flash_do.setHeight(1);
			}
			*/
			
			if(self.audioScreen_do && self.videoType_str == FWDUVPlayer.MP3){
				self.audioScreen_do.resizeAndPosition(self.tempVidStageWidth, self.tempVidStageHeight);
			}
			
			if(self.ytb_do && self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.ytb_do.setWidth(self.tempVidStageWidth);
				self.ytb_do.setHeight(self.tempVidStageHeight);
			}
			
			if(self.logo_do) self.logo_do.positionAndResize();
			if(self.playlist_do && !FWDAnimation.isTweening(self)){
				if(self.isMobile_bl){
					self.playlist_do.resizeAndPosition(false);
				}else{
					self.playlist_do.resizeAndPosition(resizePlaylistWithAnim);
				}
			}
			
			if(self.annotations_do){
				if(FWDAnimation.isTweening(self)){
					self.annotations_do.position(true);
				}else{
					self.annotations_do.position(false);
				}
			}

			if(self.controller_do) self.controller_do.resizeAndPosition();
			if(self.categories_do) self.categories_do.resizeAndPosition();
			
			if(self.videoScreen_do && (self.videoType_str == FWDUVPlayer.VIDEO || self.videoType_str == FWDUVPlayer.HLS_JS)){
				if(self.fillEntireVideoScreen_bl){
					if(self.videoScreen_do && self.videoScreen_do.video_el && self.videoScreen_do.video_el.videoWidth != 0){
						
						var originalW = self.videoScreen_do.video_el.videoWidth;
						var originalH = self.videoScreen_do.video_el.videoHeight
						var scaleX = self.tempVidStageWidth/originalW;
						var scaleY = self.tempVidStageHeight/originalH;
						
						totalScale = 0;
						if(scaleX >= scaleY){
							totalScale = scaleX;
						}else if(scaleX <= scaleY){
							totalScale = scaleY;
						}
						
						self.finalVideoScreenW = parseInt(originalW * totalScale);
						self.finalVideoScreenH = parseInt(originalH * totalScale);
						self.finalVideoScreenX = parseInt((self.tempVidStageWidth - self.finalVideoScreenW)/2);
						self.finalVideoScreenY = parseInt((self.tempVidStageHeight - self.finalVideoScreenH)/2);
					}
				}else{
					self.finalVideoScreenW = self.tempVidStageWidth;
					self.finalVideoScreenH = self.tempVidStageHeight;
					self.finalVideoScreenX = self.finalVideoScreenY = 0;
					
				}
				self.videoScreen_do.resizeAndPosition(self.finalVideoScreenW, self.finalVideoScreenH, self.finalVideoScreenX, self.finalVideoScreenY);
				
			}
		
			if(self.ytb_do && self.ytb_do.ytb && self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.ytb_do.resizeAndPosition();
			}
			
			if(self.vimeo_do && self.videoType_str == FWDUVPlayer.VIMEO){
				self.vimeo_do.resizeAndPosition();
			}
			
			
			
			if(self.preloader_do) self.positionPreloader();
			if(self.dumyClick_do){
				if(self.is360 && self.videoType_str == FWDUVPlayer.YOUTUBE){
					self.dumyClick_do.setWidth(0);
				}else{
					self.dumyClick_do.setWidth(self.tempVidStageWidth);
					if(self.isMobile_bl){
						self.dumyClick_do.setHeight(self.tempVidStageHeight);
					}else{
						if(self.videoType_str == FWDUVPlayer.YOUTUBE && !self.isAdd_bl){
							self.dumyClick_do.setHeight(self.tempVidStageHeight);
							//self.dumyClick_do.setHeight(0);
						}else{
							self.dumyClick_do.setHeight(self.tempVidStageHeight);
						}
					}
				}
			}
			
			if(self.videoHider_do) self.resizeVideoHider();
			if(self.largePlayButton_do) self.positionLargePlayButton();
			if(self.videoPoster_do && self.videoPoster_do.allowToShow_bl) self.videoPoster_do.positionAndResize();
			if(self.popw_do && self.popw_do.isShowed_bl) self.popw_do.positionAndResize();
			if(self.embedWindow_do && self.embedWindow_do.isShowed_bl) self.embedWindow_do.positionAndResize();
			if(self.passWindow_do && self.passWindow_do.isShowed_bl) self.passWindow_do.positionAndResize();
			if(self.infoWindow_do && self.infoWindow_do.isShowed_bl) self.infoWindow_do.positionAndResize();
			if(self.info_do && self.info_do.isShowed_bl) self.info_do.positionAndResize();
			if(self.shareWindow_do && self.shareWindow_do.isShowed_bl) self.shareWindow_do.positionAndResize();
			if(self.adsStart_do) self.positionAds();
			if(self.subtitle_do) self.subtitle_do.position(resizePlaylistWithAnim);
			if(self.popupAds_do) self.popupAds_do.position(resizePlaylistWithAnim);
			self.positionAdsImage();
			
			self.positionOnMin();
			
		};
		
		this.setStageContainerFinalHeightAndPosition = function(animate){
			if(self.displayType != FWDUVPlayer.STICKY) return;
			//if(!self.allowToResizeAndPosition_bl) return;
			
			self.allowToResizeAndPosition_bl = true;
			clearTimeout(self.showPlaylistWithDelayId_to);
			
			if(self.horizontalPosition_str == FWDUVPlayer.LEFT){
				self.main_do.setX(self.offsetX);
				if(self.opener_do){
					if(self.data.openerAlignment_str == "right"){
						self.opener_do.setX(Math.round(self.stageWidth - self.opener_do.w + self.offsetX));
					}else{
						self.opener_do.setX(self.offsetX);
					}
				}
			}else if(self.horizontalPosition_str == FWDUVPlayer.CENTER){
				self.main_do.setX(Math.round((self.ws.w - self.stageWidth)/2));
				if(self.opener_do){
					if(self.data.openerAlignment_str == "right"){
						self.opener_do.setX(parseInt((self.ws.w - self.stageWidth)/2) + self.stageWidth - self.opener_do.w);
					}else{
						self.opener_do.setX(self.main_do.x);
					}
				}
			}else if(self.horizontalPosition_str == FWDUVPlayer.RIGHT){
				self.main_do.setX(Math.round(self.ws.w - self.stageWidth - self.offsetX));
				if(self.opener_do){
					if(self.data.openerAlignment_str == "right"){
						self.opener_do.setX(Math.round(self.ws.w - self.opener_do.w - self.offsetX));
					}else{
						self.opener_do.setX(Math.round(self.ws.w - self.stageWidth - self.offsetX));
					}
				}
			}
			
			if(animate){		
				if(self.position_str ==  FWDUVPlayer.POSITION_TOP){
					if(self.isShowed_bl && !self.isShowedFirstTime_bl){
						FWDAnimation.to(self.stageContainer, .8, {css:{top:self.offsetY}, ease:Expo.easeInOut});
					}else{
						FWDAnimation.to(self.stageContainer, .8, {css:{top:-self.stageHeight}, ease:Expo.easeInOut});
					}
					
					if(self.isShowedFirstTime_bl){
						if(self.opener_do) FWDAnimation.to(self.opener_do, .8, {y:self.stageHeight - self.opener_do.h, ease:Expo.easeInOut});
					}else{
						if(self.opener_do) FWDAnimation.to(self.opener_do, .8, {y:self.stageHeight, ease:Expo.easeInOut});
					}
				}else{
					if(self.isShowed_bl && !self.isShowedFirstTime_bl){
						FWDAnimation.to(self.stageContainer, .8, {css:{top:self.ws.h - self.stageHeight - self.offsetY}, ease:Expo.easeInOut});
					}else{
						FWDAnimation.to(self.stageContainer, .8, {css:{top:self.ws.h}, ease:Expo.easeInOut, onComplete:self.moveWheyLeft});
					}
					
					if(self.isShowedFirstTime_bl){
						if(self.opener_do) FWDAnimation.to(self.opener_do, .8, {y:0, ease:Expo.easeInOut});
					}else{
						if(self.opener_do) FWDAnimation.to(self.opener_do, .8, {y:-self.opener_do.h, ease:Expo.easeInOut});
					}
				}
			}else{
				FWDAnimation.killTweensOf(self.stageContainer);
				if(self.position_str ==  FWDUVPlayer.POSITION_TOP){
					if(self.isShowed_bl && !self.isShowedFirstTime_bl){
						self.stageContainer.style.top = self.offsetY + "px";
					}else{
						self.stageContainer.style.top = -self.stageHeight + "px";
					}
					if(self.isShowedFirstTime_bl){
						if(self.opener_do) self.opener_do.setY(self.stageHeight - self.opener_do.h);
					}else{
						if(self.opener_do) self.opener_do.setY(self.stageHeight);
					}
				}else{
					
					if(self.isShowed_bl && !self.isShowedFirstTime_bl){
						self.stageContainer.style.top = (self.ws.h - self.stageHeight - self.offsetY) + "px";
					}else{
						self.stageContainer.style.top = self.ws.h + "px";
					}
					
					if(self.isShowedFirstTime_bl){
						if(self.opener_do) self.opener_do.setY(0);
					}else{
						if(self.opener_do) self.opener_do.setY(-self.opener_do.h);
					}
				}
			}
			
		}
		
		
		//###############################################//
		/* Setup click screen */
		//###############################################//
		this.setupClickScreen = function(){
			self.dumyClick_do = new FWDUVPDisplayObject("div");
			if(FWDUVPUtils.isIE){
				self.dumyClick_do.setBkColor("#00FF00");
				self.dumyClick_do.setAlpha(.000001);
			}
			
			if(self.hasPointerEvent_bl){
				self.dumyClick_do.screen.addEventListener("pointerdown", self.playPauseDownHandler);
				self.dumyClick_do.screen.addEventListener("pointerup", self.playPauseClickHandler);
				self.dumyClick_do.screen.addEventListener("pointermove", self.playPauseMoveHandler);
			}else{	
				if(!self.isMobile_bl){
					self.dumyClick_do.screen.addEventListener("mousedown", self.playPauseDownHandler);
					self.dumyClick_do.screen.addEventListener("mouseup", self.playPauseClickHandler);
					self.dumyClick_do.screen.addEventListener("mousemove", self.playPauseMoveHandler);
				}else{
					self.dumyClick_do.screen.addEventListener("click", self.playPauseClickHandler);
				}
				//self.dumyClick_do.screen.addEventListener("touchstart", self.playPauseDownHandler);
				//self.dumyClick_do.screen.addEventListener("touchend", self.playPauseClickHandler);
			}
			
			self.hideClickScreen();
			self.videoHolder_do.addChild(self.dumyClick_do);
		};
		
		this.playPauseDownHandler = function(e){
			self.isClickHandlerMoved_bl = false;
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);
			self.firstDommyTapX = viewportMouseCoordinates.screenX;
			self.firstDommyTapY = viewportMouseCoordinates.screenY;
			if(self.is360) self.dumyClick_do.getStyle().cursor = 'url(' + self.data.grabPath_str + '), default';
		}
		
		this.playPauseMoveHandler = function(e){
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);
			var dx;
			var dy;
			
			if(e.touches && e.touches.length != 1) return;
			dx = Math.abs(viewportMouseCoordinates.screenX - self.firstDommyTapX);   
			dy = Math.abs(viewportMouseCoordinates.screenY - self.firstDommyTapY); 
			
			if(self.isMobile_bl && (dx > 10 || dy > 10)){
				self.isClickHandlerMoved_bl = true;
			}else if(!self.isMobile_bl && (dx > 2 || dy > 2)){
				self.isClickHandlerMoved_bl = true;
			}
		}
		
		this.playPauseClickHandler = function(e){
			
			if(e.button == 2) return;
			if(self.isClickHandlerMoved_bl) return;
			
			if(self.isAdd_bl){
				
				if(self.data.adsPageToOpenURL_str && self.data.adsPageToOpenURL_str != "none"){
					if(self.ClickTracking) self.executeVastEvent(self.ClickTracking);
					window.open(self.data.adsPageToOpenURL_str, self.data.adsPageToOpenTarget_str);
					self.pause();
				}
				return;
			}
			
			if(self.disableClick_bl) return;
			self.firstTapPlaying_bl = self.isPlaying_bl;
			
			FWDUVPlayer.keyboardCurInstance = self;
			
			if(self.controller_do && self.controller_do.mainHolder_do.y != 0 && self.isMobile_bl) return;
			
			if(!self.isMobile_bl){
				if(FWDUVPlayer.videoStartBehaviour == FWDUVPlayer.PAUSE_ALL_VIDEOS){
					FWDUVPlayer.pauseAllVideos(self);
				}else if(FWDUVPlayer.videoStartBehaviour == FWDUVPlayer.STOP_ALL_VIDEOS){
					FWDUVPlayer.stopAllVideos(self);
				}
			}
			
			if(self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.ytb_do.togglePlayPause();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				self.audioScreen_do.togglePlayPause();
			}else if(FWDUVPlayer.hasHTML5Video){
				
				if(self.videoScreen_do) self.videoScreen_do.togglePlayPause();
				
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.togglePlayPause();
			}
			
			
			if(self.is360) self.dumyClick_do.getStyle().cursor = 'url(' + self.data.handPath_str + ')';
		};
		
		this.showClickScreen = function(){
			self.dumyClick_do.setVisible(true);
			if(self.isAdd_bl && self.data.adsPageToOpenURL_str && self.data.adsPageToOpenURL_str != "none"){
				self.dumyClick_do.setButtonMode(true);
			}else{	
				if(self.is360){
					self.dumyClick_do.getStyle().cursor = 'url(' + self.data.handPath_str + '), default';
				}else{
					self.dumyClick_do.setButtonMode(false);
				}
			}
		};
		
		this.hideClickScreen = function(){
			self.dumyClick_do.setVisible(false);
		};
		
		//#####################################//
		/* Setup disable click */
		//#####################################//
		this.setupDisableClick = function(){
			self.disableClick_do = new FWDUVPDisplayObject("div");
			if(FWDUVPUtils.isIE){
				self.disableClick_do.setBkColor("#ff0000");
				self.disableClick_do.setAlpha(0.001);
			}
			self.main_do.addChild(self.disableClick_do);
			
		};
		
		this.disableClick = function(){
			self.disableClick_bl = true;
			clearTimeout(self.disableClickId_to);
			if(self.disableClick_do){
				self.disableClick_do.setWidth(self.stageWidth);
				self.disableClick_do.setHeight(self.stageHeight);
			}
			self.disableClickId_to =  setTimeout(function(){
				if(self.disableClick_do){
					self.disableClick_do.setWidth(0);
					self.disableClick_do.setHeight(0);
				}
				self.disableClick_bl = false;
			}, 500);
		};
		
		this.showDisable = function(){
			if(self.disableClick_do.w == self.stageWidth) return;
			self.disableClick_do.setWidth(self.stageWidth);
			self.disableClick_do.setHeight(self.stageHeight);
		};
		
		this.hideDisable = function(){
			if(!self.disableClick_do) return;
			if(self.disableClick_do.w == 0) return;
			self.disableClick_do.setWidth(0);
			self.disableClick_do.setHeight(0);
		};
		
		//########################################//
		/* add double click and tap support */
		//########################################//
		this.addDoubleClickSupport = function(){
			
			if(self.hasPointerEvent_bl){
				self.dumyClick_do.screen.addEventListener("pointerdown", self.onFirstDown);
			}else{
				if(!self.isMobile_bl){
					self.dumyClick_do.screen.addEventListener("mousedown", self.onFirstDown);
					//if(FWDUVPUtils.isIEWebKit) self.dumyClick_do.screen.addEventListener("dblclick", self.onSecondDown);
				}
				self.dumyClick_do.screen.addEventListener("touchstart", self.onFirstDown);
			}
			self.setupVisualization();
		};
		
		this.onFirstDown = function(e){
			if(e.button == 2) return;
			if(self.isFullscreen_bl && e.preventDefault) e.preventDefault();
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);
			self.firstTapX = viewportMouseCoordinates.screenX - self.main_do.getGlobalX();
			self.firstTapY = viewportMouseCoordinates.screenY - self.main_do.getGlobalY();
		
			self.firstTapPlaying_bl = self.isPlaying_bl;
			
			if(FWDUVPUtils.isIEWebKit) return;
			if(self.hasPointerEvent_bl){
				
				self.dumyClick_do.screen.removeEventListener("pointerdown", self.onFirstDown);
				self.dumyClick_do.screen.addEventListener("pointerdown", self.onSecondDown);
			}else{
				if(!self.isMobile_bl){
					self.dumyClick_do.screen.addEventListener("mousedown", self.onSecondDown);
					self.dumyClick_do.screen.removeEventListener("mousedown", self.onFirstDown);
				}
				self.dumyClick_do.screen.addEventListener("touchstart", self.onSecondDown);
				self.dumyClick_do.screen.removeEventListener("touchstart", self.onFirstDown);
			}
			clearTimeout(self.secondTapId_to);
			self.secondTapId_to = setTimeout(self.doubleTapExpired, 500);
		};
		
		this.doubleTapExpired = function(){
			clearTimeout(self.secondTapId_to);
			if(self.hasPointerEvent_bl){
				self.dumyClick_do.screen.removeEventListener("pointerdown", self.onSecondDown);
				self.dumyClick_do.screen.addEventListener("pointerdown", self.onFirstDown);
			}else{
				self.dumyClick_do.screen.removeEventListener("touchstart", self.onSecondDown);
				self.dumyClick_do.screen.addEventListener("touchstart", self.onFirstDown);
				if(!self.isMobile_bl){
					self.dumyClick_do.screen.removeEventListener("mousedown", self.onSecondDown);
					self.dumyClick_do.screen.addEventListener("mousedown", self.onFirstDown);
				}
			}
		};
		
		this.onSecondDown = function(e){
			if(e.preventDefault) e.preventDefault();
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);
			var dx;
			var dy;
			
			if(FWDUVPUtils.isIEWebKit) self.firstTapPlaying_bl = self.isPlaying_bl;
			if(e.touches && e.touches.length != 1) return;
			dx = Math.abs((viewportMouseCoordinates.screenX - self.main_do.getGlobalX()) - self.firstTapX);   
			dy = Math.abs((viewportMouseCoordinates.screenY - self.main_do.getGlobalY()) - self.firstTapY); 
			if(dx > 10 || dy > 10) return;

			if(self.firstTapX < self.tempVidStageWidth * 0.33){
				if(!self.isPlaying_bl){
					self.skipOnDb_bl = true;
					self.rewind(10);
					self.addVisualization('left');
					setTimeout(function(){
						if(!self.isPlaying_bl) self.play();
					}, 200);
					setTimeout(function(){
						self.skipOnDb_bl = false;
					}, 500);
				} 
			}else if(self.firstTapX > self.tempVidStageWidth * 0.67){
					if(!self.isPlaying_bl){
						self.skipOnDb_bl = true;
						self.rewind(-10);
						self.addVisualization('right');
						setTimeout(function(){
							if(!self.isPlaying_bl) self.play();
						}, 200);
						setTimeout(function(){
							self.skipOnDb_bl = false;
						}, 500);
				} 
			}else{
				self.switchFullScreenOnDoubleClick();
				if(self.firstTapPlaying_bl){
					self.play();
				}else{
					self.pause();
				}
			}
		};
		
		this.switchFullScreenOnDoubleClick = function(){
			self.disableClick();
			if(!self.isFullScreen_bl){
				self.goFullScreen();
			}else{
				self.goNormalScreen();
			}
		};

		//############################################//
		/* Setup double click visualization */
		//############################################//
		self.lasPosition;
		this.setupVisualization = function(){
			self.mainVz_do = new FWDUVPDisplayObject('div');
			self.mainVz_do.getStyle().pointerEvents = 'none';
			self.mainVz_do.getStyle().backgroundColor = 'rgba(0,0,0,0.01)';
			self.mainVzBackgrond_do = new FWDUVPDisplayObject('div');
			self.mainVzBackgrond_do.getStyle().width = '100%';
			self.mainVzBackgrond_do.getStyle().height = '100%';
			self.mainVzBackgrond_do.getStyle().backgroundColor = 'rgba(255,255,255, .15)';
			self.mainVz_do.getStyle().borderRadius = '100%';
			self.mainVz_do.addChild(self.mainVzBackgrond_do);

			self.circle_do = new FWDUVPTransformDisplayObject('div');
			self.circle_do.getStyle().backgroundColor = 'rgba(255,255,255, .15)';
			self.circle_do.getStyle().borderRadius = '100%';
			self.mainVz_do.addChild(self.circle_do);


			var vzImg1 = new Image();
			vzImg1.src = self.mainFolderPath_str + this.skinPath_str + 'vis.png';
			self.vzImg1_do = new FWDUVPTransformDisplayObject('img');
			self.vzImg1_do.setScreen(vzImg1);
			self.vzImg1_do.setWidth(17);
			self.vzImg1_do.setHeight(23);
			self.mainVz_do.addChild(self.vzImg1_do);

			var vzImg2 = new Image();
			vzImg2.src = self.mainFolderPath_str + this.skinPath_str + 'vis.png';
			self.vzImg2_do = new FWDUVPTransformDisplayObject('img');
			self.vzImg2_do.setScreen(vzImg2);
			self.vzImg2_do.setWidth(17);
			self.vzImg2_do.setHeight(23);
			self.mainVz_do.addChild(self.vzImg2_do);

			var vzImg3 = new Image();
			vzImg3.src = self.mainFolderPath_str + this.skinPath_str + 'vis.png';
			self.vzImg3_do = new FWDUVPTransformDisplayObject('img');
			self.vzImg3_do.setScreen(vzImg3);
			self.vzImg3_do.setWidth(17);
			self.vzImg3_do.setHeight(23);
			self.mainVz_do.addChild(self.vzImg3_do);
		}

		this.addVisualization = function(pos){
			clearTimeout(self.vizFinisedId_to);
			clearTimeout(self.vizFinished2Id_to);
			var w = Math.round(self.tempVidStageWidth/2);
			var h = Math.round(self.tempVidStageHeight * 1.5);

			FWDAnimation.killTweensOf(self.mainVzBackgrond_do);
			if(self.lasPosition != pos) self.mainVzBackgrond_do.setAlpha(0);
			FWDAnimation.to(self.mainVzBackgrond_do, .4, {alpha:1});

			self.mainVz_do.setVisible(true);
			self.mainVz_do.setWidth(w);
			self.mainVz_do.setHeight(h);
			self.mainVz_do.setY((self.tempVidStageHeight - h)/2);
			var offsetY = Math.abs(self.mainVz_do.y);
			if(self.controller_do && self.controller_do.isShowed_bl) offsetY -= self.controller_do.stageHeight/2;
			if(!self.videoHolder_do.contains(self.mainVz_do)){
				if(self.controller_do){
					self.videoHolder_do.addChildAt(self.mainVz_do, self.videoHolder_do.getChildIndex(self.controller_do) - 1);
				}else{
					self.videoHolder_do.addChild(self.mainVz_do);
				}
			} 
			if(pos == 'right'){
				self.mainVz_do.getStyle().borderRadius = '100% 0% 0% 100%';
				self.mainVz_do.setX(w);
				self.vzImg1_do.setRotation(0);
				self.vzImg2_do.setRotation(0);
				self.vzImg3_do.setRotation(0);
			}else{
				self.mainVz_do.getStyle().borderRadius = '0% 100% 100% 0%';
				self.mainVz_do.setX(0);
				self.vzImg1_do.setRotation(180);
				self.vzImg2_do.setRotation(180);
				self.vzImg3_do.setRotation(180);
			}

			self.vzImg1_do.setX(Math.round(w - (self.vzImg1_do.w * 3))/2);
			self.vzImg1_do.setY(Math.round(offsetY + (self.tempVidStageHeight - self.vzImg1_do.h)/2));
			self.vzImg2_do.setX(self.vzImg1_do.x + self.vzImg1_do.w);
			self.vzImg2_do.setY(self.vzImg1_do.y);
			self.vzImg3_do.setX(self.vzImg2_do.x + self.vzImg2_do.w);
			self.vzImg3_do.setY(self.vzImg2_do.y);

			
			FWDAnimation.killTweensOf(self.vzImg1_do);
			FWDAnimation.killTweensOf(self.vzImg2_do);
			FWDAnimation.killTweensOf(self.vzImg3_do);
			self.vzImg1_do.setAlpha(0);
			self.vzImg2_do.setAlpha(0);
			self.vzImg3_do.setAlpha(0);
			if(pos == 'right'){
				FWDAnimation.to(self.vzImg1_do, .4, {alpha:1});
				FWDAnimation.to(self.vzImg1_do, .4, {alpha:0, delay:.3});
				FWDAnimation.to(self.vzImg2_do, .4, {alpha:1, delay:.3});
				FWDAnimation.to(self.vzImg2_do, .4, {alpha:0, delay:.6});
				FWDAnimation.to(self.vzImg3_do, .4, {alpha:1, delay:.6});
				FWDAnimation.to(self.vzImg3_do, .4, {alpha:0, delay:.9});
			}else{
				FWDAnimation.to(self.vzImg3_do, .4, {alpha:1});
				FWDAnimation.to(self.vzImg3_do, .4, {alpha:0, delay:.3});
				FWDAnimation.to(self.vzImg2_do, .4, {alpha:1, delay:.3});
				FWDAnimation.to(self.vzImg2_do, .4, {alpha:0, delay:.6});
				FWDAnimation.to(self.vzImg1_do, .4, {alpha:1, delay:.6});
				FWDAnimation.to(self.vzImg1_do, .4, {alpha:0, delay:.9});
			}

			FWDAnimation.killTweensOf(self.circle_do);
			self.circle_do.setAlpha(1);
			self.circle_do.setScale2(1);
			self.circle_do.setWidth(w);
			self.circle_do.setHeight(w);
			self.circle_do.setScale2(0);
			self.circle_do.setX(self.firstTapX - self.mainVz_do.x - self.circle_do.w/2);
			self.circle_do.setY(self.firstTapY + offsetY - self.circle_do.w/2);
			//FWDAnimation.to(self.circle_do, 1, {alpha:0});
			FWDAnimation.to(self.circle_do, .8, {scale:2, ease:Expo.easeInOut});

			self.vizFinisedId_to = setTimeout(function(){
				FWDAnimation.to(self.mainVzBackgrond_do, .4, {alpha:0});
				FWDAnimation.to(self.circle_do, .4, {alpha:0});
				self.vizFinished2Id_to = setTimeout(function(){
					self.mainVz_do.setVisible(false);
				}, 400)
			}, 800);

			self.lasPosition = pos;
		}

		this.stopVisualization =  function(){
			if(!self.mainVz_do) return;
			clearTimeout(self.vizFinisedId_to);
			clearTimeout(self.vizFinished2Id_to);
			self.mainVz_do.setVisible(false);
		}
		
		//############################################//
		/* Setup video hider */
		//############################################//
		this.setupVideoHider = function(){
			self.videoHider_do = new FWDUVPDisplayObject("div");
			self.videoHider_do.setBkColor(self.backgroundColor_str);
			self.videoHolder_do.addChild(self.videoHider_do);
		};
		
		this.showVideoHider = function(){
			if(self.isVideoHiderShowed_bl || !self.videoHider_do) return;
			self.isVideoHiderShowed_bl = true;
			self.videoHider_do.setVisible(true);
			self.resizeVideoHider();
		};
		
		this.hideVideoHider = function(){
			if(!self.isVideoHiderShowed_bl) return;
			self.isVideoHiderShowed_bl = false;
			clearTimeout(self.videoHilderId_to);
			self.videoHilderId_to = setTimeout(function(){
				self.videoHider_do.setVisible(false);
			}, 300);
		};
		
		this.resizeVideoHider = function(){
			if(self.isVideoHiderShowed_bl){
				self.videoHider_do.setWidth(self.tempStageWidth);
				self.videoHider_do.setHeight(self.tempStageHeight);
			}
		};
		
		//############################################//
		/* Setup youtube player */
		//############################################//
		this.setupYoutubeAPI = function(){
			if(self.ytb_do) return;
			if(typeof YT != "undefined" && YT.Player){
				self.setupYoutubePlayer();
				return;
			}else{
				if(FWDUVPlayer.isYoutubeAPILoadedOnce_bl){
					self.keepCheckingYoutubeAPI_int =  setInterval(function(){
						if(typeof YT != "undefined" && YT && YT.Player){
							if(self.videoSourcePath_str.indexOf("youtube.") == -1) clearInterval(self.keepCheckingYoutubeAPI_int);
							clearInterval(self.keepCheckingYoutubeAPI_int);
							self.setupYoutubePlayer();
						}
					}, 50);
					return;
				}
				
				var tag = document.createElement("script");
				tag.src = "https://www.youtube.com/iframe_api";
				var firstScriptTag = document.getElementsByTagName("script")[0];
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			
				tag.onload = function(){
					self.checkIfYoutubePlayerIsReadyId_int = setInterval(function(){
						if(YT && YT.Player){
							clearInterval(self.checkIfYoutubePlayerIsReadyId_int);
							self.setupYoutubePlayer();
						}
					}, 50);
				}
				
				tag.onerror = function(){
					setTimeout(function(){
						self.main_do.addChild(self.info_do);
						//self.info_do.allowToRemove_bl = false;
						self.info_do.showText("Error loading Youtube API");
						self.preloader_do.hide();
					}, 500);
					return;
				}
				
				FWDUVPlayer.isYoutubeAPILoadedOnce_bl = true;
			}
		};
		
		this.setupYoutubePlayer = function(){
			if(self.ytb_do) return;
			
			FWDUVPYoutubeScreen.setPrototype();
			self.ytb_do = new FWDUVPYoutubeScreen(self, self.data.volume);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.READY, self.youtubeReadyHandler);
			self.ytb_do.addListener(FWDUVPVideoScreen.ERROR, self.videoScreenErrorHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.SAFE_TO_SCRUBB, self.videoScreenSafeToScrubbHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.STOP, self.videoScreenStopHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.PLAY, self.videoScreenPlayHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.PAUSE, self.videoScreenPauseHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.UPDATE, self.videoScreenUpdateHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.UPDATE_TIME, self.videoScreenUpdateTimeHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.LOAD_PROGRESS, self.videoScreenLoadProgressHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.PLAY_COMPLETE, self.videoScreenPlayCompleteHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.CUED, self.youtubeScreenCuedHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.QUALITY_CHANGE, self.youtubeScreenQualityChangeHandler);
			self.ytb_do.addListener(FWDUVPYoutubeScreen.UPDATE_SUBTITLE, self.videoScreenUpdateSubtitleHandler);
	
		};
		
		this.youtubeReadyHandler = function(e){
			
			self.isYoutubeReady_bl = true;
		
			self.hidePreloaderId_to = setTimeout(function(){
				if(self.preloader_do) self.preloader_do.hide(true);}
			, 50);
			
			self.isTempYoutubeAdd_bl = self.isAdd_bl;
			if(self.isAdd_bl){
				if(self.videoType_str == FWDUVPlayer.YOUTUBE) self.setSource(self.addSource_str);
			}else{
				if(self.videoType_str == FWDUVPlayer.YOUTUBE) self.updateAds(0, true);
			}
			
			/*
			clearTimeout(self.updatePlaylistFirstTime);
			self.updatePlaylistFirstTime = setTimeout(function(){
				if(!self.isPlaylistLoadedFirstTime_bl) self.updatePlaylist();
				self.isPlaylistLoadedFirstTime_bl = true;
			}, 100);
			*/
		};
		
		this.youtubeScreenCuedHandler = function(){
			if(self.main_do) if(self.main_do.contains(self.info_do)) self.main_do.removeChild(self.info_do);
		};
		
		this.youtubeScreenQualityChangeHandler = function(e){
			if(self.videoType_str == FWDUVPlayer.VIDEO) self.curDurration = self.videoScreen_do.curDuration;
			if(self.controller_do) self.controller_do.updateQuality(e.levels, e.qualityLevel);
		};
		
		//############################################//
		/* Setup Vimeo API */
		//############################################//
		this.setupVimeoAPI = function(){
			if(self.vimeo_do) return;
			if(typeof Vimeo != "undefined" && Vimeo.Player){
				self.setupVimeoPlayer();
				return;
			}else{
				if(FWDUVPlayer.isVimeoAPILoadedOnce_bl){
					self.keepCheckingVimeoAPI_int =  setInterval(function(){
						if(typeof Vimeo != "undefined" && Vimeo && Vimeo.Player){
							if(self.videoSourcePath_str.indexOf("vimeo.") == -1) clearInterval(self.keepCheckingVimeoAPI_int);
							clearInterval(self.keepCheckingVimeoAPI_int);
							self.setupVimeoPlayer();
						}
					}, 50);
					return;
				}
				
				var tag = document.createElement("script");
				tag.src = "https://player.vimeo.com/api/player.js";
				var firstScriptTag = document.getElementsByTagName("script")[0];
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			
				tag.onload = function(){
					self.keepCheckingVimeoAPI_int = setInterval(function(){
						if(typeof Vimeo != "undefined" && Vimeo && Vimeo.Player){
							clearInterval(self.keepCheckingVimeoAPI_int);
							self.setupVimeoPlayer();
						}
					}, 50);
				}
										
				tag.onerror = function(){
					setTimeout(function(){
						self.main_do.addChild(self.info_do);
						//self.info_do.allowToRemove_bl = false;
						self.info_do.showText("Error loading Vimeo API");
						self.preloader_do.hide();
					}, 500);
					return;
				}
				
				FWDUVPlayer.isVimeoAPILoadedOnce_bl = true;
			}
		};
		
		//############################################//
		/* Setup Vimeo player */
		//############################################// 
		this.setupVimeoPlayer = function(){
			if(self.vimeo_do) return;
			FWDUVPVimeoScreen.setPrototype();

			self.vimeo_do = new FWDUVPVimeoScreen(self, self.data.volume);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.ERROR, self.vimeoInitErrorHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.READY, self.vimeoReadyHandler);
			
			self.vimeo_do.addListener(FWDUVPVimeoScreen.SAFE_TO_SCRUBB, self.videoScreenSafeToScrubbHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.STOP, self.videoScreenStopHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.PLAY, self.videoScreenPlayHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.PAUSE, self.videoScreenPauseHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.UPDATE, self.videoScreenUpdateHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.UPDATE_TIME, self.videoScreenUpdateTimeHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.LOAD_PROGRESS, self.videoScreenLoadProgressHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.PLAY_COMPLETE, self.videoScreenPlayCompleteHandler);
			self.vimeo_do.addListener(FWDUVPVimeoScreen.UPDATE_SUBTITLE, self.videoScreenUpdateSubtitleHandler);
			
		};
		
		this.vimeoInitErrorHandler = function(e){
			self.main_do.addChild(self.info_do);
			//self.info_do.allowToRemove_bl = false;
			self.info_do.showText(e.error);
			self.preloader_do.hide();
		};
		
		this.vimeoReadyHandler = function(e){
			self.isVimeoReady_bl = true;
	
			clearInterval(self.hidePreloaderId_to);
			self.hidePreloaderId_to = setTimeout(function(){
				if(self.preloader_do) self.preloader_do.hide(true);}
			, 50);
		
			if(self.isAdd_bl){
				if(self.videoType_str == FWDUVPlayer.VIMEO) self.setSource(self.addSource_str);
			}else{
				if(self.videoType_str == FWDUVPlayer.VIMEO) self.updateAds(0, true);
			}
		};		
		
		//#############################################//
		/* setup context menu */
		//#############################################//
		self.setupContextMenu = function(){
			FWDUVPContextMenu.setPrototype();
			self.customContextMenu_do = new FWDUVPContextMenu(self, self.data);
		};

		//#############################################//
		/* setup RSM */
		//#############################################//
		self.setupRSM = function(){
			window.addEventListener("beforeunload", function (e) {
				var test = Math.random() * 1000;
				if(self.isPlaying_bl){
					document.cookie = "fwduvp_video_path=" + self.videoSourcePath_str + "; expires=Thu, 18 Dec 2040 00:00:01 GMT; path=/";
					var curTime = self.getCurrentTime();
					if(curTime.length == 5) curTime = "00:" + curTime;
					document.cookie = "fwduvp_time=" + curTime + "; expires=Thu, 18 Dec 2040 00:00:01 GMT; path=/";	
				}
			});
		};
		
		
		//#############################################//
		/* setup data */
		//#############################################//
		self.setupData = function(){
			FWDUVPData.setPrototype();
			self.data = new FWDUVPData(self.props_obj, self.rootElement_el, self);
			self.data.useYoutube_bl = self.useYoutube_bl;
			
			if(self.mainBackground_do) self.mainBackground_do.getStyle().background = "url('" + self.mainBackgroundImagePath_str + "')";
			
			self.data.addListener(FWDUVPData.VAST_LOADED, self.vastLoaded);
			self.data.addListener(FWDUVPData.PRELOADER_LOAD_DONE, self.onPreloaderLoadDone);
			self.data.addListener(FWDUVPData.LOAD_ERROR, self.dataLoadError);
			self.data.addListener(FWDUVPData.SKIN_PROGRESS, self.dataSkinProgressHandler);
			self.data.addListener(FWDUVPData.SKIN_LOAD_COMPLETE, self.dataSkinLoadComplete);
			self.data.addListener(FWDUVPData.PLAYLIST_LOAD_COMPLETE, self.dataPlayListLoadComplete);
		};
		
		self.vastLoaded = function(e){
			self.data.playlist_ar[self.id].ads_ar = e.ads;
			self.updateAds(0);
		}
		
		self.onPreloaderLoadDone = function(){
			//if(self.background_do) self.background_do.getStyle().background = "url('" + self.data.skinPath_str + "main-background.png" + "')";
		
			if(self.showPreloader_bl) self.setupPreloader();
			if(!self.isMobile_bl) self.setupContextMenu();
			self.fillEntireVideoScreen_bl = self.data.fillEntireVideoScreen_bl;
			self.resizeHandler();
		};
		
		self.dataLoadError = function(e){
			self.main_do.addChild(self.info_do);
			self.info_do.showText(e.text);
			if(self.preloader_do) self.preloader_do.hide(false);
			self.resizeHandler();
			if(self.playlist_do) self.playlist_do.catId = -1;
			self.dispatchEvent(FWDUVPlayer.ERROR, {error:e.text});
		};
		
		self.dataSkinProgressHandler = function(e){};
		
		self.dataSkinLoadComplete = function(){
			if(location.protocol.indexOf("file:") != -1){
				if(FWDUVPUtils.isOpera || FWDUVPUtils.isIEAndLessThen9){
					self.main_do.addChild(self.info_do);
					self.info_do.allowToRemove_bl = false;
					self.info_do.showText("This browser can't play video local, please test online or use a browser like Firefox of Chrome.");
					if(self.preloader_do) self.preloader_do.hide();
					return;
				}
			}
			
			self.volume = self.data.volume;
			self.playlistHeight = self.data.playlistBottomHeight;
			
			if(self.displayType == FWDUVPlayer.FULL_SCREEN  && !FWDUVPUtils.hasFullScreen){
				self.data.showFullScreenButton_bl = false;
			}
		
			if(self.isEmbedded_bl){
				self.useDeepLinking_bl = false;
				self.data.playlistPosition_str = "right";
			}
			
			
			if(FWDUVPlayer.atLeastOnePlayerHasDeeplinking_bl) self.useDeepLinking_bl = false;
			if(self.useDeepLinking_bl) FWDUVPlayer.atLeastOnePlayerHasDeeplinking_bl = true;
			
			if(self.useDeepLinking_bl){
				setTimeout(function(){self.setupDL();}, 200);
			}else{
				if(self.isEmbedded_bl){
					self.catId = self.embeddedPlaylistId;
					self.id = self.embeddedVideoId;
				}else{
					var args = FWDUVPUtils.getHashUrlArgs(window.location.hash);
					if(self.useDeepLinking_bl && args.playlistId !== undefined && args.videoId !== undefined){
						self.catId = args.playlistId;
						self.id = args.videoId;
					}else{
						if(args.videoId){
							self.id = args.videoId;
						}else{
							self.id = self.data.startAtVideo;
						}

						if(args.playlistId){
							self.catId = args.playlistId;
						}else{
							self.catId = self.data.startAtPlaylist;
						}
						
					}
				}
				self.loadInternalPlaylist();
			}
			
			
			
		};
		
		this.dataPlayListLoadComplete = function(){
			
			self.loadAddFirstTime_bl = true;
			if(!self.isPlaylistLoadedFirstTime_bl){
				self.setupNormalVideoPlayers();
				
				if(!FWDUVPUtils.isIEAndLessThen9) self.updatePlaylist();
			}
			
			if(self.isPlaylistLoadedFirstTime_bl) self.updatePlaylist();	
			self.isPlaylistLoaded_bl = true;
			self.resizeHandler();
			setTimeout(function(){
				self.positionLargePlayButton();
				if(self.controller_do) self.controller_do.resizeAndPosition();
				if(self.playlist_do) self.playlist_do.resizeAndPosition();
			}, 350);
		};
		
		this.updatePlaylist = function(){
			self.videoType_str =  "none.:";
			
			
			if(self.main_do) if(self.main_do.contains(self.info_do)) self.main_do.removeChild(self.info_do);
			if(self.preloader_do) self.preloader_do.hide(true);
			self.totalVideos = self.data.playlist_ar.length;
			
	    	if(self.id < 0){
				self.id = 0;
			}else if(self.id > self.totalVideos - 1){
				self.id = self.totalVideos - 1;
			}
			
	    	
			if(self.playlist_do) self.playlist_do.updatePlaylist(self.data.playlist_ar, self.catId, self.id, self.data.cats_ar[self.catId].playlistName);
			self.hideVideoHider();
			
			if(self.data.startAtRandomVideo_bl){
				self.id = parseInt(Math.random() * self.data.playlist_ar.length);
				if(self.useDeepLinking_bl){
					FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
					return;
				}
	    	}
			
			self.prevSource = Math.random() * 99999999999;
	    	self.posterPath_str = self.data.playlist_ar[self.id].posterSource;
			//self.setSource(undefined, true);
			self.updateAds(0);
			
			if(self.isFirstPlaylistLoaded_bl && !self.isMobile_bl && !self.data.startAtRandomVideo_bl && self.data.autoPlay_bl) self.play();
			self.data.startAtRandomVideo_bl = false;
			self.isFirstPlaylistLoaded_bl = true;
			self.dispatchEvent(FWDUVPlayer.LOAD_PLAYLIST_COMPLETE);
			
			if(self.displayType == FWDUVPlayer.STICKY){
				setTimeout(function(){
					self.isShowedFirstTime_bl = false;
					self.setStageContainerFinalHeightAndPosition(self.animate_bl);
				}, 50);
			}
		};
		
		//############################################//
		/* Load playlists */
		//############################################//
		this.loadInternalPlaylist = function(){
			
			self.isPlaylistLoaded_bl = false;
			self.isAdd_bl = false;
			self.prvAdSource = Math.random() * 999999999;
			if(self.prevCatId == self.catId) return;
			self.prevCatId = self.catId;
		
			self.stop();
			if(self.hider) self.hider.stop();
			self.setPosterSource("none");
			if(self.videoPoster_do) self.videoPoster_do.hide(true);
			if(self.preloader_do){
				self.preloader_do.show(false);
				//self.preloader_do.startPreloader();
			}
			if(self.largePlayButton_do) self.largePlayButton_do.hide();
			if(self.controller_do) self.controller_do.hide(false, 10);
			self.showVideoHider();
			self.data.loadPlaylist(self.catId);
			if(self.logo_do) self.logo_do.hide(false, true);
			if(self.isAdd_bl){
				self.adsSkip_do.hide(false);
				self.adsStart_do.hide(false);
			}
			if(self.playlist_do) self.playlist_do.destroyPlaylist();
			if(self.preloader_do) self.positionPreloader();
			
			if(self.isAPIReady_bl) self.dispatchEvent(FWDUVPlayer.START_TO_LOAD_PLAYLIST);
		};
		
		//############################################//
		/* update deeplink */
		//############################################//
		this.setupDL = function(){
			FWDAddress.onChange = self.dlChangeHandler;
			if(self.isEmbedded_bl){
				FWDAddress.setValue("?playlistId=" + self.embeddedPlaylistId + "&videoId=" + self.embeddedVideoId);
			}
			self.dlChangeHandler();
		};
		
		this.dlChangeHandler = function(){
			if(self.hasOpenedInPopup_bl) return;
			var mustReset_bl = false;
			
			if(self.categories_do && self.categories_do.isOnDOM_bl){
				self.categories_do.hide();
				return;
			}
			
			
			
			
			self.prevId = self.id;
			self.prevEventCatId = self.catId;
			
			self.catId = parseInt(FWDAddress.getParameter("playlistId"));
			self.id = parseInt(FWDAddress.getParameter("videoId"));
			
			if(self.catId == undefined || self.id == undefined || isNaN(self.catId) || isNaN(self.id)){
				self.catId = self.data.startAtPlaylist;
				self.id = self.data.startAtVideo;
				mustReset_bl = true;
			}
		
			if(self.catId < 0 || self.catId > self.data.totalCategories - 1 && !mustReset_bl){
				self.catId = self.data.startAtPlaylist;
				self.id = self.data.startAtVideo;
				mustReset_bl = true;
			}
			
			if(self.data.playlist_ar){
				if(self.id < 0 && !mustReset_bl){
					self.id = self.data.startAtVideo;
					mustReset_bl = true;
				}else if(self.prevCatId == self.catId && self.id > self.data.playlist_ar.length - 1  && !mustReset_bl){
					self.id = self.data.playlist_ar.length - 1;
					mustReset_bl = true;
				}
			}
			
			if(mustReset_bl){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
				return;
			}
			
			if(self.prevId == -1) self.prevId  = self.id;
			if(self.prevEventCatId == -1) self.prevEventCatId = self.catId;
			
			if(self.prevCatId != self.catId){
				self.loadInternalPlaylist();
				self.prevCatId = self.catId;
			}else{
				self.isThumbClick_bl = true;
				self.updateAds(0, true);
				/*
				if(!self.data.startAtRandomVideo_bl){
					if(self.videoType_str == FWDUVPlayer.VIMEO){
						self.playVimeoWithDelay();
					}else{
						self.play();
						self.changeHLS_bl = true;
					}
				}
				*/
				self.data.startAtRandomVideo_bl = false;
			}
		};
		
		this.playVimeoWithDelay = function(){
			if(self.isMobile_bl) return;
			if(self.vimeo_do.isVideoLoaded_bl){
				self.hasVimeoStarted_bl = true;
				self.play();
				self.vimeo_do.play();
				clearTimeout(self.playVimeoWhenLoadedId_to);
			}else{
				self.playVimeoWhenLoadedId_to = setTimeout(self.playVimeoWithDelay, 50);
			}
		};
		
		
		//###########################################//
		/* Setup normal video players */
		//###########################################//
		this.setupNormalVideoPlayers = function(){
			if(self.videoScreen_do) return;
			
			self.isAPIReady_bl = true;
			self.setupRSM();
			self.setupVideoScreen();
			self.setupAudioScreen();
			self.setupVideoPoster();
			if(self.preloader_do) self.main_do.addChild(self.preloader_do);
			self.setupSubtitle();
			self.setupClickScreen();
			self.setupPopupAds();
			if(self.data.showLogo_bl) self.setupLogo();
			self.addDoubleClickSupport();
			self.setupVideoHider();
			self.setupAnnotations();
			if(self.data.showController_bl) self.setupController();
			self.setupAdsStart();
			if(self.data.showPlaylistButtonAndPlaylist_bl) self.setupPlaylist();
			self.setupLargePlayPauseButton();
			if(self.data.showController_bl) self.setupHider();
			if(self.data.showPlaylistsButtonAndPlaylists_bl) self.setupCategories();
			self.setupDisableClick();
			if(self.data.showEmbedButton_bl) self.setupEmbedWindow();
			self.setupPasswordWindow();
			if(self.data.showShareButton_bl) self.setupShareWindow();
			self.setupAopw();
			if(self.data.showInfoButton_bl) self.setupInfoWindow();
			if((self.data.showOpener_bl && self.displayType == FWDUVPlayer.STICKY)
				|| (self.data.stickyOnScrollShowOpener_bl && self.stickyOnScroll)){
				self.setupOpener();
			} 
			
			if(FWDUVPlayer.useYoutube == "no") self.isPlaylistLoadedFirstTime_bl = true;
			self.addMinOnScroll();
			self.isAPIReady_bl = true;
			self.dispatchEvent(FWDUVPlayer.READY);
			
			if(self.data.addKeyboardSupport_bl) self.addKeyboardSupport();
		
		};
		
			//###########################################//
		/* setup opener */
		//###########################################//
		this.setupOpener = function(){
	
			FWDUVPOpener.setPrototype();
			self.opener_do = new FWDUVPOpener(self.data, self.position_str, self.isShowed_bl);
			if(FWDUVPUtils.isIEAndLessThen9){
				self.opener_do.getStyle().zIndex = "2147483634";
			}else{
				self.opener_do.getStyle().zIndex = "99999999994";
			}
			self.opener_do.setX(-10000);
			if(self.isShowed_bl){
				self.opener_do.showCloseButton();
			}else{
				self.opener_do.showOpenButton();
			}
			self.opener_do.addListener(FWDUVPOpener.SHOW, self.openerShowHandler);
			self.opener_do.addListener(FWDUVPOpener.HIDE, self.openerHideHandler);
			self.opener_do.addListener(FWDUVPController.PLAY, self.controllerOnPlayHandler);
			self.opener_do.addListener(FWDUVPController.PAUSE, self.controllerOnPauseHandler);
			self.stageContainer.appendChild(self.opener_do.screen);
			if(self.stickyOnScroll){
				 self.opener_do.getStyle().position = 'fixed';
				 document.documentElement.appendChild(self.opener_do.screen);
			}
			
			
		};
		
		this.openerShowHandler = function(){
			self.showPlayer();
		};
		
		this.openerHideHandler = function(){
			self.hidePlayer();
		};
		
		//#############################################//
		/* setup preloader */
		//#############################################//
		this.setupPreloader = function(){
			FWDUVPPreloader.setPrototype();
			self.preloader_do = new FWDUVPPreloader(self, 'center', 10, self.preloaderBackgroundColor, self.preloaderFillColor, 3, 0.8);
			self.preloader_do.show(false);
			//self.preloader_do.startPreloader();
			if(self.showPreloader_bl){
				if(self.displayType == FWDUVPlayer.STICKY){
					document.documentElement.appendChild(self.preloader_do.screen);
				}else{
					self.main_do.addChild(self.preloader_do);
				}
				
			}
		};
	
		this.positionPreloader = function(){
			if(self.displayType == FWDUVPlayer.STICKY){
				
				if(!self.main_do.contains(self.preloader_do)){
					self.preloader_do.setX(Math.round((self.ws.w - self.preloader_do.w)/2));
					if(self.position_str == FWDUVPlayer.POSITION_BOTTOM){
						self.preloader_do.setY(Math.round((self.ws.h - self.preloader_do.h) - 10) + FWDUVPUtils.getScrollOffsets().y);
					}else{
						self.preloader_do.setY(10);
					}
				}else{
					self.preloader_do.setX(Math.round((self.tempVidStageWidth - self.preloader_do.w)/2));
					self.preloader_do.setY(Math.round((self.tempVidStageHeight - self.preloader_do.h)/2));
				}
			}else{
				self.preloader_do.setX(parseInt((self.tempVidStageWidth - self.preloader_do.w)/2));
				self.preloader_do.setY(parseInt((self.tempVidStageHeight - self.preloader_do.h)/2));
			}
		};
		
		//###########################################//
		/* setup categories */
		//###########################################//
		this.setupCategories = function(){
			FWDUVPCategories.setPrototype();
			self.categories_do = new FWDUVPCategories(self.data, self);
			self.categories_do.getStyle().zIndex = "2147483647";
			self.categories_do.addListener(FWDUVPCategories.HIDE_COMPLETE, self.categoriesHideCompleteHandler);
			if(self.data.showPlaylistsByDefault_bl){
				self.showCatWidthDelayId_to = setTimeout(function(){
					self.showCategories();
				}, 1400);
			};
		};
		
		this.categoriesHideCompleteHandler = function(e){
			if(self.controller_do){
				self.controller_do.setCategoriesButtonState("unselected");
				self.controller_do.categoriesButton_do.setNormalState(true);
			}
			//if(self.customContextMenu_do) self.customContextMenu_do.updateParent(self.main_do);
			
			if(self.useDeepLinking_bl){
				if(self.categories_do.id != self.catId){
					self.catId = self.categories_do.id;
					self.id = 0;
					FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
					return;
				}
			}else{
				if(self.catId == self.categories_do.id) return;
				self.catId = self.categories_do.id;
				self.id = 0;
				self.loadInternalPlaylist(self.catId);
			}
			
			if(self.isVideoPlayingWhenOpenWindows_bl) self.resume();
		};
		
		//##########################################//
		/* setup video poster */
		//##########################################//
		this.setupVideoPoster = function(){
			FWDUVPPoster.setPrototype();
			self.videoPoster_do = new FWDUVPPoster(self,  self.data.show, self.data.posterBackgroundColor_str);
			self.videoHolder_do.addChild(self.videoPoster_do);
		};
		
		//##########################################//
		/* setup info window */
		//##########################################//
		this.setupInfoWindow = function(){
			FWDUVPInfoWindow.setPrototype();
			self.infoWindow_do = new FWDUVPInfoWindow(self, self.data);
			self.infoWindow_do.addListener(FWDUVPInfoWindow.HIDE_COMPLETE, self.infoWindowHideCompleteHandler);
			self.main_do.addChild(self.infoWindow_do);
		};
		
		this.infoWindowHideCompleteHandler = function(){
			
			if(self.isVideoPlayingWhenOpenWindows_bl) self.resume();
			
			if(self.controller_do && !self.isMobile_bl){
				self.controller_do.infoButton_do.isDisabled_bl = false;
				self.controller_do.infoButton_do.setNormalState(true);
			}
		};
		
		//###########################################//
		/* Setup large play / pause button */
		//###########################################//
		this.setupLargePlayPauseButton = function(){
			if(self.data.useVectorIcons_bl){				
				FWDUVPSimpleButton.setPrototype();
				self.largePlayButton_do = new FWDUVPSimpleButton(
						undefined, undefined, undefined, true, undefined, undefined, undefined,
						"<div class='table-fwduvp-button'><span class='table-cell-fwduvp-button icon-play'></span></div>",
						undefined,
						"UVPLargePlayButtonNormalState",
						"UVPLargePlayButtonSelectedState"
				);
			}else{
				FWDUVPSimpleButton.setPrototype();
				if(this.skinPath_str.indexOf("hex_white") != -1){
					self.largePlayButton_do = new FWDUVPSimpleButton(self.data.largePlayN_img, self.data.largePlayS_str, undefined, true,
															 self.data.useHEXColorsForSkin_bl,
															 self.data.normalButtonsColor_str,
															 "#FFFFFF");
				}else{
					self.largePlayButton_do = new FWDUVPSimpleButton(self.data.largePlayN_img, self.data.largePlayS_str, undefined, true,
															 self.data.useHEXColorsForSkin_bl,
															 self.data.normalButtonsColor_str,
															 self.data.selectedButtonsColor_str);
				}
			}
			
			self.largePlayButton_do.addListener(FWDUVPSimpleButton.MOUSE_UP, self.largePlayButtonUpHandler);
			self.largePlayButton_do.setOverflow("visible");
			self.largePlayButton_do.hide(false);
			self.main_do.addChild(self.largePlayButton_do);
		};
		
		this.largePlayButtonUpHandler = function(){
			self.disableClick();
			self.largePlayButton_do.hide();
			self.play();
		};
		
		this.positionLargePlayButton =  function(){
			self.largePlayButton_do.setX(parseInt((self.tempVidStageWidth - self.largePlayButton_do.w)/2));
			self.largePlayButton_do.setY(parseInt((self.tempVidStageHeight - self.largePlayButton_do.h)/2));
		};
		
		//###########################################//
		/* Setup logo */
		//###########################################//
		this.setupLogo = function(){
			FWDUVPLogo.setPrototype();
			self.logo_do = new FWDUVPLogo(self, self.data.logoPath_str, self.data.logoPosition_str, self.data.logoMargins);
			self.main_do.addChild(self.logo_do);
		};
		
		//###########################################//
		/* Setup playlist */
		//###########################################//
		this.setupPlaylist = function(){
			FWDUVPPlaylist.setPrototype();
			self.playlist_do = new FWDUVPPlaylist(self, self.data);
			self.playlist_do.addListener(FWDUVPPlaylist.THUMB_MOUSE_UP, self.playlistThumbMouseUpHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.PLAY_PREV_VIDEO, self.playPrevVideoHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.PLAY_NEXT_VIDEO, self.playNextVideoHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.ENABLE_SHUFFLE, self.enableShuffleHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.DISABLE_SHUFFLE, self.disableShuffleHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.ENABLE_LOOP, self.enableLoopHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.DISABLE_LOOP, self.disableLoopHandler);
			self.playlist_do.addListener(FWDUVPPlaylist.CHANGE_PLAYLIST, self.changePlaylistHandler);
			self.main_do.addChildAt(self.playlist_do, 0);
			if(self.data.useVectorIcons_bl){
				setTimeout(function(){
					self.playlist_do.resizeAndPosition(true);
				}, 340);
			}
		};
		
		this.changePlaylistHandler = function(e){
			self.loadPlaylist(e.id);
		}
		
		this.playlistThumbMouseUpHandler = function(e){
			if(self.disableClick_bl) return;
			
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			if(self.useDeepLinking_bl && self.id != e.id){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + e.id);
				self.id = e.id;
				self.isThumbClick_bl = true;
			}else{
				self.id = e.id;
				self.changeHLS_bl = true;
				self.isThumbClick_bl = true;
				self.isAdd_bl = false;
				
				self.updateAds(0);
			}
		};
		
		this.playPrevVideoHandler = function(){
			self.isThumbClick_bl = true;
			if(self.data.shuffle_bl){
				self.playShuffle();
			}else{
				self.playPrev();
			}
		};
		
		this.playNextVideoHandler = function(){
		
			self.isThumbClick_bl = true;
			if(self.data.shuffle_bl){
				self.playShuffle();
			}else{
				self.playNext();
			}
		};
		
		this.enableShuffleHandler = function(e){
			self.data.shuffle_bl = true;
			self.data.loop_bl = false;
			self.playlist_do.setShuffleButtonState("selected");
			self.playlist_do.setLoopStateButton("unselected");
		};
		
		this.disableShuffleHandler = function(e){
			self.data.shuffle_bl = false;
			self.playlist_do.setShuffleButtonState("unselected");
		};
		
		this.enableLoopHandler = function(e){
			self.data.loop_bl = true;
			self.data.shuffle_bl = false;
			self.playlist_do.setLoopStateButton("selected");
			self.playlist_do.setShuffleButtonState("unselected");
		};
		
		this.disableLoopHandler = function(e){
			self.data.loop_bl = false;
			self.playlist_do.setLoopStateButton("unselected");
		};
		
		//###########################################//
		/* Setup popup ads */
		//###########################################//
		this.setupPopupAds = function(){
			FWDUVPPupupAds.setPrototype();
			self.popupAds_do =  new FWDUVPPupupAds(self, self.data);
			self.videoHolder_do.addChild(self.popupAds_do);
		};
		
		
		//###########################################//
		/* Setup subtitle */
		//###########################################//
		this.setupSubtitle = function(){
			FWDUVPSubtitle.setPrototype();
			self.subtitle_do =  new FWDUVPSubtitle(self, self.data);
			self.subtitle_do.addListener(FWDUVPSubtitle.LOAD_COMPLETE, self.subtitleLoadComplete);
		};
		
		this.subtitleLoadComplete = function(){
			self.subtitle_do.show();
			if(self.controller_do) self.controller_do.enableSubtitleButton();
		};
		
		this.loadSubtitle = function(path){
			if(path.indexOf("encrypt:") != -1) path = atob(path.substr(8));
			if(self.controller_do) self.controller_do.disableSubtitleButton();
			if(path){
				self.subtitle_do.loadSubtitle(path);
				self.videoHolder_do.addChildAt(self.subtitle_do, self.videoHolder_do.getChildIndex(self.dumyClick_do) - 1);
				
			}
		}
		
		//###########################################//
		/* setup controller */
		//###########################################//
		this.setupController = function(){
		
			FWDUVPController.setPrototype();
			self.controller_do = new FWDUVPController(self.data, self);
			
			self.controller_do.addListener(FWDUVPData.LOAD_ERROR, self.controllerErrorHandler);
			self.controller_do.addListener(FWDUVPController.REWIND, self.rewindHandler);
			self.controller_do.addListener(FWDUVPController.CHANGE_PLAYBACK_RATES, self.changePlaybackRateHandler);
			self.controller_do.addListener(FWDUVPController.CHANGE_SUBTITLE, self.changeSubtitileHandler);
			self.controller_do.addListener(FWDUVPController.SHOW_CATEGORIES, self.showCategoriesHandler);
			self.controller_do.addListener(FWDUVPController.SHOW_PLAYLIST, self.showPlaylistHandler);
			self.controller_do.addListener(FWDUVPController.HIDE_PLAYLIST, self.hidePlaylistHandler);
			self.controller_do.addListener(FWDUVPController.PLAY, self.controllerOnPlayHandler);
			self.controller_do.addListener(FWDUVPController.PAUSE, self.controllerOnPauseHandler);
			self.controller_do.addListener(FWDUVPController.START_TO_SCRUB, self.controllerStartToScrubbHandler);
			self.controller_do.addListener(FWDUVPController.SCRUB, self.controllerScrubbHandler);
			self.controller_do.addListener(FWDUVPController.STOP_TO_SCRUB, self.controllerStopToScrubbHandler);
			self.controller_do.addListener(FWDUVPController.CHANGE_VOLUME, self.controllerChangeVolumeHandler);
			self.controller_do.addListener(FWDUVPController.DOWNLOAD_VIDEO, self.controllerDownloadVideoHandler);
			self.controller_do.addListener(FWDUVPController.CHANGE_YOUTUBE_QUALITY, self.controllerChangeYoutubeQualityHandler);
			self.controller_do.addListener(FWDUVPController.FULL_SCREEN, self.controllerFullScreenHandler);
			self.controller_do.addListener(FWDUVPController.NORMAL_SCREEN, self.controllerNormalScreenHandler);
			self.controller_do.addListener(FWDUVPPlaylist.PLAY_PREV_VIDEO, self.playPrevVideoHandler);
			self.controller_do.addListener(FWDUVPPlaylist.PLAY_NEXT_VIDEO, self.playNextVideoHandler);
			self.controller_do.addListener(FWDUVPController.SHOW_EMBED_WINDOW, self.showEmbedWindowHandler);
			self.controller_do.addListener(FWDUVPController.SHOW_INFO_WINDOW, self.showInfoWindowHandler);
			self.controller_do.addListener(FWDUVPController.SHOW_SHARE_WINDOW, self.controllerShareHandler);
			self.controller_do.addListener(FWDUVPController.SHOW_SUBTITLE, self.showSubtitleHandler);
			self.controller_do.addListener(FWDUVPController.HIDE_SUBTITLE, self.hideSubtitleHandler);
			self.videoHolder_do.addChild(self.controller_do);
		};
		
		this.controllerErrorHandler = function(e){
			self.main_do.addChild(self.info_do);
			self.info_do.showText(e.text);
		}
		
		this.rewindHandler = function(){
			self.rewind(10);
		}

		this.rewind = function(offset){
			var curTime = self.getCurrentTime();
			if(curTime.length == 5) curTime = "00:" + curTime;
			if(curTime.length == 7) curTime = "0" + curTime;
			curTime = FWDUVPUtils.getSecondsFromString(curTime);
			curTime -= offset;
			curTime = FWDUVPUtils.formatTime(curTime);
			if(curTime.length == 5) curTime = "00:" + curTime;
			if(curTime.length == 7) curTime = "0" + curTime;
			self.scrubbAtTime(curTime);
		}
		
		this.changePlaybackRateHandler = function(e){
			self.setPlaybackRate(e.rate);
		}
		
		this.changeSubtitileHandler = function(e){
			self.data.playlist_ar[self.id].startAtSubtitle = e.id;
			self.controller_do.updateSubtitleButtons(self.data.playlist_ar[self.id].subtitleSource, self.data.playlist_ar[self.id].startAtSubtitle);
			if(!self.isAdd_bl) self.loadSubtitle(self.data.playlist_ar[self.id].subtitleSource[self.data.playlist_ar[self.id].subtitleSource.length - 1 - self.data.playlist_ar[self.id].startAtSubtitle]["source"]);
		}
		
		this.showSubtitleHandler = function(){
			self.subtitle_do.show();
			self.subtitle_do.isShowed_bl = true;
			
		};
		
		this.hideSubtitleHandler = function(){
			self.subtitle_do.isShowed_bl = false;
			self.subtitle_do.hide();
		};
		
		this.showCategoriesHandler = function(e){
			self.showCategories();
			if(self.controller_do) self.controller_do.setCategoriesButtonState("selected");
		};
		
		this.showPlaylistHandler = function(e){
			self.disableClick();
			self.showPlaylist();			
		};
		
		this.hidePlaylistHandler = function(e){
			self.disableClick();
			self.hidePlaylist();	
		};
		
		this.controllerOnPlayHandler = function(e){
			self.play();
		};
		
		this.controllerOnPauseHandler = function(e){
			self.pause();
		};
		
		this.controllerStartToScrubbHandler = function(e){
			self.startToScrub();
		};
		
		this.controllerScrubbHandler = function(e){
			self.scrub(e.percent);
		};
		
		this.controllerStopToScrubbHandler = function(e){
			self.stopToScrub();
		};
		
		this.controllerChangeVolumeHandler = function(e){
			self.setVolume(e.percent);
		};
		
		this.controllerDownloadVideoHandler = function(){
			self.downloadVideo();
		};
		
		this.controllerShareHandler = function(e){
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.ytb_do.isPlaying_bl;
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.vimeo_do.isPlaying_bl;
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.isVideoPlayingWhenOpenWindows_bl = self.videoScreen_do.isPlaying_bl;
			}
			self.pause();
			
			self.shareWindow_do.show();
			if(self.controller_do && !self.isMobile_bl){
				self.controller_do.shareButton_do.setSelectedState();
				self.controller_do.shareButton_do.isDisabled_bl = true;
			}
		};
		
		this.controllerChangeYoutubeQualityHandler = function(e){
			//self.ytb_do.setQuality(e.quality);
			if(self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.ytb_do.setQuality(e.quality);
			}else{
				self.data.playlist_ar[self.id].startAtVideo = self.data.playlist_ar[self.id].videoSource.length - 1 - e.id;
				self.setSource(self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["source"], false, self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["is360"]);
				self.isQualityChanging_bl = true;
				self.play();
			}
			
			//self.controller_do.updateQuality(self.data.playlist_ar[self.id].videoLabels_ar, self.data.playlist_ar[self.id].videoLabels_ar[self.data.playlist_ar[self.id].videoLabels_ar.length - 1 - self.data.playlist_ar[self.id].startAtVideo]);
		};
		
		this.controllerFullScreenHandler = function(){
			self.goFullScreen();
		};
		
		this.controllerNormalScreenHandler = function(){
			self.goNormalScreen();
		};
		
		this.showEmbedWindowHandler = function(){
		
		/*	if(location.protocol.indexOf("file:") != -1){
				self.main_do.addChild(self.info_do);
				self.info_do.showText("Embedding video files local is not allowed or possible! To function properly please test online");
				return;
			}*/
			
			
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.ytb_do.isPlaying_bl;
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.vimeo_do.isPlaying_bl;
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.isVideoPlayingWhenOpenWindows_bl = self.videoScreen_do.isPlaying_bl;
			}
			self.pause();
		
			self.embedWindow_do.show();
			
			if(self.controller_do && !self.isMobile_bl){
				self.controller_do.embedButton_do.setSelectedState();
				self.controller_do.embedButton_do.isDisabled_bl = true;
			}
		};
		
		this.showInfoWindowHandler = function(){
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.ytb_do.isPlaying_bl;
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.vimeo_do.isPlaying_bl;
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.isVideoPlayingWhenOpenWindows_bl = self.videoScreen_do.isPlaying_bl;
			}
			self.pause();
			
			self.infoWindow_do.show(self.data.playlist_ar[self.id].desc);
			
			if(self.controller_do && !self.isMobile_bl){
				self.controller_do.infoButton_do.setSelectedState();
				self.controller_do.infoButton_do.isDisabled_bl = true;
			}
		};
		
		//###########################################//
		/* setup FWDUVPAudioScreen */
		//###########################################//
		this.setupAudioScreen = function(){	
			if(self.audioScreen_do) return;
			FWDUVPAudioScreen.setPrototype();
			self.audioScreen_do = new FWDUVPAudioScreen(self, self.data.volume);
			//self.audioScreen_do.addListener(FWDUVPAudioScreen.START, self.audioScreenStartHandler);
			
			self.audioScreen_do.addListener(FWDUVPAudioScreen.ERROR, self.videoScreenErrorHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.SAFE_TO_SCRUBB, self.videoScreenSafeToScrubbHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.STOP, self.videoScreenStopHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.PLAY, self.videoScreenPlayHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.PAUSE, self.videoScreenPauseHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.UPDATE, self.videoScreenUpdateHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.UPDATE_TIME, self.videoScreenUpdateTimeHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.LOAD_PROGRESS, self.videoScreenLoadProgressHandler);
			self.audioScreen_do.addListener(FWDUVPVideoScreen.START_TO_BUFFER, self.videoScreenStartToBuferHandler);
			self.audioScreen_do.addListener(FWDUVPVideoScreen.STOP_TO_BUFFER, self.videoScreenStopToBuferHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.PLAY_COMPLETE, self.videoScreenPlayCompleteHandler);
			self.audioScreen_do.addListener(FWDUVPAudioScreen.UPDATE_SUBTITLE, self.videoScreenUpdateSubtitleHandler);
			
			self.videoHolder_do.addChild(self.audioScreen_do);
		};
		
		//###########################################//
		/* setup FWDUVPVideoScreen */
		//###########################################//
		this.setupVideoScreen = function(){
			FWDUVPVideoScreen.setPrototype();
			self.videoScreen_do = new FWDUVPVideoScreen(self, self.data.volume);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.ERROR, self.videoScreenErrorHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.SAFE_TO_SCRUBB, self.videoScreenSafeToScrubbHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.STOP, self.videoScreenStopHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.PLAY, self.videoScreenPlayHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.PAUSE, self.videoScreenPauseHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.UPDATE, self.videoScreenUpdateHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.UPDATE_TIME, self.videoScreenUpdateTimeHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.LOAD_PROGRESS, self.videoScreenLoadProgressHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.START_TO_BUFFER, self.videoScreenStartToBuferHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.STOP_TO_BUFFER, self.videoScreenStopToBuferHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.PLAY_COMPLETE, self.videoScreenPlayCompleteHandler);
			self.videoScreen_do.addListener(FWDUVPVideoScreen.UPDATE_SUBTITLE, self.videoScreenUpdateSubtitleHandler);
			self.videoHolder_do.addChild(self.videoScreen_do);
		};
		
		this.videoScreenErrorHandler = function(e){
			var error;
			self.isPlaying_bl = false;
			if(FWDUVPlayer.hasHTML5Video || self.videoType_str == FWDUVPlayer.YOUTUBE){
				error = e.text;
				if(window.console) console.log(e.text);
				if(self.main_do) self.main_do.addChild(self.info_do);
				if(self.info_do) self.info_do.showText(error);
				
				if(self.controller_do){
					self.controller_do.disableMainScrubber();
					self.controller_do.disablePlayButton();
					if(!self.data.showControllerWhenVideoIsStopped_bl) self.controller_do.hide(!self.isMobile_bl);
					self.largePlayButton_do.hide();
					self.hideClickScreen();
					if(self.hider) self.hider.stop();
				}
			}else{
				error = e;
				if(self.main_do) self.main_do.addChild(self.info_do);
				if(self.info_do) self.info_do.showText(error);
			}
			
			if(self.logo_do) self.logo_do.hide(false);
			if(self.preloader_do) self.preloader_do.hide(false);
			self.showCursor();
			self.dispatchEvent(FWDUVPlayer.ERROR, {error:error});
		};
		
		this.videoScreenSafeToScrubbHandler = function(){
			if(self.controller_do){
				if(self.isAdd_bl){
					self.controller_do.disableMainScrubber();
					if(self.data.timeToHoldAds != 0) self.adsStart_do.show(true);
					if(self.data.adsThumbnailPath_str && self.data.adsThumbnailPath_str != "none") self.adsStart_do.loadThumbnail(self.data.adsThumbnailPath_str);
					self.positionAds();
				}else{
					self.controller_do.enableMainScrubber();
				}
				
				self.controller_do.enablePlayButton();
				self.controller_do.show(true);
				
				if(!self.isAdd_bl && self.controller_do.ytbQualityButton_do){
					self.controller_do.ytbQualityButton_do.enable();
					self.controller_do.enablePlaybackRateButton();
				}
				if(!self.isAdd_bl && self.controller_do.playbackRateButton_do) self.controller_do.enablePlaybackRateButton();
				if(!self.isAdd_bl && self.controller_do){
					 if(self.controller_do.downloadButton_do) self.controller_do.downloadButton_do.enable();
					 if(self.controller_do.rewindButton_do) self.controller_do.rewindButton_do.enable();
				}
				if(self.fillEntireVideoScreen_bl) self.resizeHandler();
				if(self.hider) self.hider.start();
				
			}
			
			if(!self.isAdd_bl && self.data.playlist_ar[self.id].subtitleSource){
				self.loadSubtitle(self.data.playlist_ar[self.id].subtitleSource[self.data.playlist_ar[self.id].subtitleSource.length - 1 - self.data.playlist_ar[self.id].startAtSubtitle]["source"]);
				
			}
			
			if(!self.isAdd_bl){
				if(self.customContextMenu_do) self.customContextMenu_do.enable();
				if(self.controller_do && self.controller_do.thumbnailsPreview_do && self.hasThumbnailsPreview) self.controller_do.thumbnailsPreview_do.load(self.data.playlist_ar[self.id]['thumbnailsPreview']);
			}
			
			if(self.controller_do){
				if( !self.isQualityChanging_bl) self.controller_do.disableSubtitleButton();
				self.controller_do.enableAtbButton();
			} 
			
			if(self.isMobile_bl){
				self.adsSkip_do.hide(false);
			}
			
			
			self.callVastEvent("start");
			self.executeVastEvent(self.Impression);
			
			if(self.videoType_str != FWDUVPlayer.VIMEO) self.showClickScreen();
			setTimeout(function(){
				if(self.totalDuration && self.controller_do) self.controller_do.positionAdsLines(self.totalDuration);
			}, 1500);
			self.dispatchEvent(FWDUVPlayer.SAFE_TO_SCRUB);
			if(self.getStartTimeStamp("t") != "00:00:00") self.scrubbAtTime(self.getStartTimeStamp("t"));
			
			if(document.cookie && self.data.useResumeOnPlay_bl){
				if(FWDUVPUtils.getCookie("fwduvp_video_path") && FWDUVPUtils.getCookie("fwduvp_time") 
				   && FWDUVPUtils.getCookie("fwduvp_video_path") == self.videoSourcePath_str && !self.isAdd_bl){
					var curTime = FWDUVPUtils.getCookie("fwduvp_time");
					if(!self.rmsPlayed_bl){
						self.scrubbAtTime(FWDUVPUtils.getCookie("fwduvp_time"));
						//document.cookie = "fwduvp_video_path=; expires=Thu, 01-Jan-70 00:00:01 GMT; path=/";
					}
				}
			}
			self.rmsPlayed_bl = true;
		};
		
		this.videoScreenUpdateSubtitleHandler = function(e){
			if(self.subtitle_do && !self.isAdd_bl) self.subtitle_do.updateSubtitle(e.curTime);
		}
	
		this.videoScreenStopHandler = function(e){
			if(self.main_do) if(self.main_do.contains(self.info_do)) self.main_do.removeChild(self.info_do);
			self.videoPoster_do.allowToShow_bl = true;
			self.isPlaying_bl = false;
			
			if(self.controller_do){
				self.controller_do.disableMainScrubber();
				self.controller_do.showPlayButton();
				if(!self.data.showControllerWhenVideoIsStopped_bl){
					self.controller_do.hide(!self.isMobile_bl);
				}else{
					self.controller_do.show(!self.isMobile_bl);
				}
				if(self.hider) self.hider.stop();
			}
			
			if(self.useYoutube_bl && self.ytb_do){
				if(self.isMobile_bl){
					self.ytb_do.destroyYoutube();
				}else{
					self.ytb_do.stopVideo();
				}
			}
			
			if(self.logo_do) self.logo_do.hide(true);
			
			self.hideClickScreen();
			
			if(self.isMobile_bl && self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.videoPoster_do.hide();
				self.largePlayButton_do.hide();
			}
			
			if(self.isMobile_bl){
				self.adsSkip_do.hide(false);
				self.adsStart_do.hide(false);
			}
			
			self.showCursor();
			self.dispatchEvent(FWDUVPlayer.STOP);
		};
		
		this.videoScreenPlayHandler = function(){
			
			if(self.is360) self.dumyClick_do.getStyle().cursor = 'url(' + self.data.handPath_str + '), default';
			FWDUVPlayer.keyboardCurInstance = self;
			
			if(self.videoType_str == FWDUVPlayer.YOUTUBE
			   && self.ytb_do && self.ytb_do.isStopped_bl) return;
			   
			self.callVastEvent("resume");
			
			if(self.isMobile_bl){
				if(FWDUVPlayer.videoStartBehaviour == FWDUVPlayer.STOP_ALL_VIDEOS){
					FWDUVPlayer.stopAllVideos(self);
				}
			}else{
				if(FWDUVPlayer.videoStartBehaviour == FWDUVPlayer.PAUSE_ALL_VIDEOS){
					FWDUVPlayer.pauseAllVideos(self);
				}
			}
			
			self.isPlaying_bl = true;
			self.isThumbClick_bl = false;
			self.loadAddFirstTime_bl = false;
			
			if(self.logo_do) self.logo_do.show(true);
			  
			if(self.controller_do){
				self.controller_do.showPauseButton();
				self.controller_do.show(true);
			}
			
			self.playAtTime_bl = false;
			self.hasHlsPlayedOnce_bl = true;
			if(self.largePlayButton_do) self.largePlayButton_do.hide();
			if(self.hider) self.hider.start();
			self.showCursor();
			
			if(self.popw_do) self.popw_do.hide();
			
			if(self.isQualityChanging_bl){
				self.scrubbAtTime(self.curDurration);
				self.curDurration = 0;
				self.isQualityChanging_bl = false;
			}
			
			if(self.wasAdd_bl){
				if(FWDUVPUtils.isIOS){
					setTimeout(function(){
						self.scrubbAtTime(self.scrubAfterAddDuration);
					},500);
				}else{
					if(self.videoType_str == FWDUVPlayer.VIMEO){
						setTimeout(function(){
							self.scrubbAtTime(self.scrubAfterAddDuration);
						},500);
					}else{
						self.scrubbAtTime(self.scrubAfterAddDuration)
					}
				
				}
				self.wasAdd_bl = false;
			}
			
			if(!self.hasStartedToPlay_bl && self.data.playlist_ar[self.id].startAtTime) self.scrubbAtTime(self.data.playlist_ar[self.id].startAtTime);
			self.hasStartedToPlay_bl = true;
			if(self.opener_do) self.opener_do.showPauseButton();
			
			self.dispatchEvent(FWDUVPlayer.PLAY);
		};
		
		this.videoScreenPauseHandler = function(){

			if(self.videoType_str == FWDUVPlayer.YOUTUBE
			   && self.ytb_do && self.ytb_do.isStopped_bl) return;
			   
			if(self.videoType_str == FWDUVPlayer.VIMEO
			   && self.vimeo_do && self.vimeo_do.isStopped_bl) return;
			   
			self.callVastEvent("pause");
			if(self.preloader_do) self.preloader_do.hide();

			self.isPlaying_bl = false;
			if(self.controller_do){
				self.controller_do.showPlayButton();
				self.controller_do.show(true);
			}
			
			var isShareWIndowShowed_bl = self.shareWindow_do && self.shareWindow_do.isShowed_bl;
			var isEmbedWIndowShowed_bl = self.embedWindow_do && self.embedWindow_do.isShowed_bl;

			if(!isShareWIndowShowed_bl && !isEmbedWIndowShowed_bl){
				if(self.showPopW_bl) self.popw_do.show(self.popwSource);	
			}
				
			if(self.largePlayButton_do && !self.data.showAnnotationsPositionTool_bl) self.largePlayButton_do.show();
			
			if(self.hider){
				self.hider.reset();
				self.hider.stop();
			}
			
			if(self.videoType_str != FWDUVPlayer.VIMEO) self.showClickScreen();
			
			self.showCursor();
			if(self.opener_do) self.opener_do.showPlayButton();
			//if(self.is360) self.dumyClick_do.getStyle().cursor = 'url(' + self.data.grabPath_str + '), default';
			self.dispatchEvent(FWDUVPlayer.PAUSE);
		};
		
		this.videoScreenUpdateHandler = function(e){
			var percent;	
			if(FWDUVPlayer.hasHTML5Video || self.videoType_str == FWDUVPlayer.YOUTUBE){
				
				percent = e.percent;
				if(self.controller_do) self.controller_do.updateMainScrubber(percent);
			}else{
				percent = e;
				console.log(e)
				if(self.controller_do) self.controller_do.updateMainScrubber(percent);
			}
			self.dispatchEvent(FWDUVPlayer.UPDATE, {percent:percent});
		};
		
		this.videoScreenUpdateTimeHandler = function(e, e2, e3){
		
			if(self.prevSeconds != e.seconds) self.totalTimePlayed += 1;
			self.totalTimeInSeconds = e.totalTimeInSeconds;
			self.curTimeInSecond = e.seconds;
			self.totalTime = e.totalTime;
			self.curTime = e.curTime;
			self.prevSeconds = e.seconds
			self.totalPercentPlayed = self.totalTimePlayed / e.totalTimeInSeconds;
			if(!isFinite(self.totalPercentPlayed)) self.totalPercentPlayed = 0;
			if(FWDUVPUtils.getSecondsFromString(self.getStartTimeStamp("e"))){
				if(self.curTimeInSecond >= parseInt(FWDUVPUtils.getSecondsFromString(self.getStartTimeStamp("e")))) self.stop();
			}

			if(self.controller_do 
			   && !self.controller_do.isMainScrubberScrubbing_bl
			   && self.controller_do.atb
			   && self.controller_do.atb.isShowed_bl
			   && !self.controller_do.atb.scrub){
				
				var a = self.totalTimeInSeconds * self.controller_do.atb.pa;
				var b = self.totalTimeInSeconds * self.controller_do.atb.pb;
			
				if(self.prevCurTimeInSeconds != self.curTimeInSecond){
					self.prevCurTimeInSeconds = self.curTimeInSecond;
					if(self.curTimeInSecond < a){
						self.scrub(self.controller_do.atb.pa);
					}else if(self.curTimeInSecond > b){
						self.scrub(self.controller_do.atb.pa);
					}
				}
				
			}
			
			if(self.isAdd_bl){
				if(self.totalPercentPlayed >= .25 && self.callFirstQuartile){
					self.callVastEvent("firstQuartile");
					self.callFirstQuartile = false;
				}else if(self.totalPercentPlayed >= .50 && self.callMidpoint){
					self.callVastEvent("midpoint");
					self.callMidpoint = false;
				}else if(self.totalPercentPlayed >= .75 && self.callThirdQuartile){
					self.callVastEvent("thirdQuartile");
					self.callThirdQuartile = false;
				}
			}

			var time;
			var seconds;
			if(FWDUVPlayer.hasHTML5Video || self.videoType_str == FWDUVPlayer.YOUTUBE || self.videoType_str == FWDUVPlayer.VIMEO){
				self.curTime = e.curTime;
				self.totalTime = e.totalTime;
				time = self.curTime + "/" + self.totalTime;
				seconds = e.seconds;
				if(self.controller_do) self.controller_do.updateTime(time);
			}else{
				self.curTime = e;
				self.totalTime = e2;
				time = self.curTime + "/" + self.totalTime;
				if(e == undefined || e2 ==  undefined) time = "00:00/00:00";
				seconds = e3;
				if(self.controller_do) self.controller_do.updateTime(time);
			}
			
			self.currentSecconds = e.seconds;
			
			if(self.popupAds_do && !self.isAdd_bl) self.popupAds_do.update(parseInt(e.seconds));
			if(self.annotations_do && !self.isAdd_bl) self.annotations_do.update(parseInt(e.seconds));
			
			if(self.cuePointsSource_ar && !self.isAdd_bl){
				for(var i=0; i<self.cuePointsSource_ar.length; i++){
					var cuePoint = self.cuePointsSource_ar[i];
					if(cuePoint.timeStart == e.seconds){
						if(self.data.executeCuepointsOnlyOnce_bl){
							if(!cuePoint.isPlayed_bl) eval(cuePoint.javascriptCall);
						}else{
							eval(cuePoint.javascriptCall);
						}
						cuePoint.isPlayed_bl = true;
					}
				}
			}
			
			if(!self.isAdd_bl){
				if(self.totalTime.length>5){
					self.totalDuration = FWDUVPUtils.getSecondsFromString(self.totalTime);
				}else{
					self.totalDuration = FWDUVPUtils.getSecondsFromString("00:" + self.totalTime);
				}
			}
	
			if(self.isAdd_bl){
			
				if(self.data.timeToHoldAds > seconds){
					self.adsStart_do.updateText(self.data.skipToVideoText_str + Math.abs(self.data.timeToHoldAds - seconds));
					if(self.isMobile_bl) self.adsSkip_do.hide(false);
					if(self.videoType_str == FWDUVPlayer.IMAGE){
						self.adsStart_do.show(true);
					}
				}else if(self.isPlaying_bl){
					self.adsStart_do.hide(true);
					if(self.data.timeToHoldAds != 0) self.adsSkip_do.show(true);
				}
			}else{
				self.adsStart_do.hide(true);
				self.adsSkip_do.hide(true);
			}
		
			if(seconds != 0){
				self.curDurration = seconds;
				self.updateAds(seconds);
			}
			
			if(self.isPlaying_bl && FWDUVPUtils.getSecondsFromString( self.data.playlist_ar[self.id].stopAtTime) <= e.seconds){
				if(self.data.playAfterVideoStop_bl){
					if(self.data.stopAfterLastVideoHasPlayed_bl && self.data.playlist_ar.length - 1 == self.id){
						self.stop();
					}else{
						self.playNext();
					}
				}else if(!self.data.stopAfterLastVideoHasPlayed_bl && self.data.playlist_ar.length - 1 == self.id){
					self.playNext();
				}else{
					self.stop();
				}
				
			}

			self.dispatchEvent(FWDUVPlayer.UPDATE_TIME, {currentTime:self.curTime, totalTime:self.totalTime});
		};
		
		this.videoScreenLoadProgressHandler = function(e){
			if(FWDUVPlayer.hasHTML5Video || self.videoType_str == FWDUVPlayer.YOUTUBE){
				if(self.controller_do) self.controller_do.updatePreloaderBar(e.percent);
			}else{
				if(self.controller_do) self.controller_do.updatePreloaderBar(e);
			}
		};
		
		this.videoScreenStartToBuferHandler = function(){
			if(self.preloader_do){
				self.preloader_do.show(false);
				//self.preloader_do.startPreloader();
			}
		};
		
		this.videoScreenStopToBuferHandler = function(){
			if(self.preloader_do) self.preloader_do.hide(true);
		};
		
		this.videoScreenPlayCompleteHandler = function(e, buttonUsedToSkipAds){
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			self.callVastEvent("complete");
			
			if(!self.isAdd_bl && self.data.playlist_ar[self.id].redirectURL){
				if(self.data.playlist_ar[self.id].redirectTarget == "_self"){
					location.replace(self.data.playlist_ar[self.id].redirectURL);
				}else{
					window.open(self.data.playlist_ar[self.id].redirectURL, self.data.playlist_ar[self.id].redirectTarget);
				}
			}
			
			var tempIsAdd_bl = self.isAdd_bl;
			if(self.isAdd_bl){
				self.isThumbClick_bl = true;
				if(self.data.openNewPageAtTheEndOfTheAds_bl && self.data.adsPageToOpenURL_str != "none" && !buttonUsedToSkipAds){
					if(self.data.adsPageToOpenTarget_str == "_self"){
						location.href = self.data.adsPageToOpenURL_str;
					}else{
						window.open(self.data.adsPageToOpenURL_str, self.data.adsPageToOpenTarget_str);
					}
				}
				
				self.isAdd_bl = false;	
				//self.setSource(self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["source"], true);
			
				self.updateAds(0);

				self.wasAdd_bl = true;
					
				if(buttonUsedToSkipAds && self.videoType_str == FWDUVPlayer.VIDEO){	
					self.play();
				}else{
					if(!self.isMobile_bl) self.play();
				}
			}
			
			if(!tempIsAdd_bl){
				if((self.data.stopVideoWhenPlayComplete_bl || self.data.playlist_ar.length == 1)
				||  (self.data.stopAfterLastVideoHasPlayed_bl && self.data.playlist_ar.length - 1 == self.id)
				){
					self.stop();
				}else if(self.data.loop_bl){
					if(self.videoType_str == "hls_flash"){
						setTimeout(function(){
							self.scrub(0);
							self.resume();
						}, 50);
					}else{						
						self.scrub(0);
						self.play();
					}
				}else if(self.data.shuffle_bl){
					self.playShuffle();
					if(self.isMobile_bl) self.stop();
				}else{
					self.playNext();
					if(self.isMobile_bl) self.stop();
				}
			}
			if(self.hider) self.hider.reset();
			self.dispatchEvent(FWDUVPlayer.PLAY_COMPLETE);
		};
		
		//##########################################//
		/* Setup annotations */
		//##########################################//
		this.setupAnnotations = function(){
			FWDUVPAnnotations.setPrototype();
			self.annotations_do = new FWDUVPAnnotations(self, self.data);
			self.videoHolder_do.addChild(self.annotations_do);
		};
		
		//##########################################//
		/* Setup skip adds buttons */
		//##########################################//
		this.setupAdsStart = function(){
			FWDUVPAdsStart.setPrototype();
			self.adsStart_do = new FWDUVPAdsStart(
					self.data.adsButtonsPosition_str, 
					self.data.adsBorderNormalColor_str, 
					"", 
					self.data.adsBackgroundPath_str,
					self.data.adsTextNormalColor);
			
			FWDUVPAdsButton.setPrototype();
			self.adsSkip_do = new FWDUVPAdsButton(
					self.data.skipIconPath_img,
					self.data.skipIconSPath_str,
					self.data.skipToVideoButtonText_str,
					self.data.adsButtonsPosition_str, 
					self.data.adsBorderNormalColor_str, 
					self.data.adsBorderSelectedColor_str, 
					self.data.adsBackgroundPath_str,
					self.data.adsTextNormalColor,
					self.data.adsTextSelectedColor,
					self.data.useHEXColorsForSkin_bl,
					self.data.normalButtonsColor_str,
					self.data.selectedButtonsColor_str);
			self.adsSkip_do.addListener(FWDUVPAdsButton.MOUSE_UP, self.skipAdsMouseUpHandler);
			
			
			self.videoHolder_do.addChild(self.adsSkip_do);
			self.videoHolder_do.addChild(self.adsStart_do);
		};
		
		this.skipAdsMouseUpHandler = function(){
			self.isThumbClick_bl = true;
			self.callVastEvent("skip");
			self.videoScreenPlayCompleteHandler(null, true);
		};
		
		this.positionAds = function(animate){
			
			var finalX;
			var finalY;
			//if(self.adsStart_do.isShowed_bl){
				if(self.data.adsButtonsPosition_str == "left"){
					finalX = 0;
				}else{
					finalX = self.tempVidStageWidth;
				}
				
				if(self.controller_do){
					if(self.controller_do.isShowed_bl){
						finalY = self.tempVidStageHeight - self.adsStart_do.h - self.data.controllerHeight - 30;
					}else{
						finalY = self.tempVidStageHeight - self.adsStart_do.h - self.data.controllerHeight;
					}
				}else{
					finalY = self.tempVidStageHeight - self.adsStart_do.h
				}
				
				FWDAnimation.killTweensOf(this.adsStart_do);
				if(animate){
					FWDAnimation.to(this.adsStart_do, .8, {y:finalY, ease:Expo.easeInOut});
				}else{
					this.adsStart_do.setY(finalY);
				}
				
				self.adsStart_do.setX(finalX);

			//}
			
			//if(self.adsSkip_do.isShowed_bl){
				if(self.data.adsButtonsPosition_str == "left"){
					finalX = 0;
				}else{
					finalX = self.tempVidStageWidth;
				}
				
				if(self.controller_do){
					if(self.controller_do.isShowed_bl){
						finalY = self.tempVidStageHeight - self.adsSkip_do.h - self.data.controllerHeight - 30;
					}else{
						finalY = self.tempVidStageHeight - self.adsSkip_do.h - self.data.controllerHeight;
					}
				}else{
					finalY = self.tempVidStageHeight - self.adsSkip_do.h
				}
				
				FWDAnimation.killTweensOf(this.adsSkip_do);
				if(animate){
					FWDAnimation.to(this.adsSkip_do, .8, {y:finalY, ease:Expo.easeInOut});
				}else{
					this.adsSkip_do.setY(finalY);
				}
				
				self.adsSkip_do.setX(finalX);
			//}
		};
		
		//##########################################//
		/* Setup embed window */
		//##########################################//
		this.setupShareWindow = function(){
			FWDUVPShareWindow.setPrototype();
			self.shareWindow_do = new FWDUVPShareWindow(self.data, self);
			self.shareWindow_do.addListener(FWDUVPShareWindow.HIDE_COMPLETE, self.shareWindowHideCompleteHandler);
		};
		
		this.shareWindowHideCompleteHandler = function(){
			
			if(self.isVideoPlayingWhenOpenWindows_bl) self.resume();
			
			if(self.controller_do && !self.isMobile_bl){
				self.controller_do.shareButton_do.isDisabled_bl = false;
				self.controller_do.shareButton_do.setNormalState(true);
			}
		};
		
		//##########################################//
		/* Setup embed window */
		//##########################################//
		this.setupPasswordWindow = function(){
			FWDUVPPassword.setPrototype();
			self.passWindow_do = new FWDUVPPassword(self.data, self);
			self.passWindow_do.addListener(FWDUVPPassword.CORRECT, self.passordCorrect);
		};
		
		this.passordCorrect = function(){
			self.passWindow_do.hide();
			self.hasPassedPassowrd_bl = true;
			self.play();
		}
		
		//##########################################//
		/* Setup embed window */
		//##########################################//
		this.setupEmbedWindow = function(){
			//if(self.isMobile_bl || location.protocol.indexOf("file:") != -1) return;
			FWDUVPEmbedWindow.setPrototype();
			self.embedWindow_do = new FWDUVPEmbedWindow(self.data, self);
			self.embedWindow_do.addListener(FWDUVPEmbedWindow.ERROR, self.embedWindowErrorHandler);
			self.embedWindow_do.addListener(FWDUVPEmbedWindow.HIDE_COMPLETE, self.embedWindowHideCompleteHandler);
		};
		
		this.embedWindowErrorHandler = function(e){
			self.main_do.addChild(self.info_do);
			self.info_do.showText(e.error);
		};
		
		this.embedWindowHideCompleteHandler = function(){
			
			if(self.isVideoPlayingWhenOpenWindows_bl) self.resume();
			
			if(self.controller_do && !self.isMobile_bl){
				self.controller_do.embedButton_do.isDisabled_bl = false;
				self.controller_do.embedButton_do.setNormalState(true);
			}
		};
		
		this.copyLinkButtonOnMouseOver = function(){
			self.embedWindow_do.copyLinkButton_do.setSelectedState();
		};
		
		this.copyLinkButtonOnMouseOut = function(){
			self.embedWindow_do.copyLinkButton_do.setNormalState();
		};
		
		this.getLinkCopyPath = function(){
			return self.embedWindow_do.linkToVideo_str;
		};
		
		this.embedkButtonOnMouseOver = function(){
			self.embedWindow_do.copyEmbedButton_do.setSelectedState();
		};
		
		this.embedButtonOnMouseOut = function(){
			self.embedWindow_do.copyEmbedButton_do.setNormalState();
		};
		
		this.getEmbedCopyPath = function(){
			return self.embedWindow_do.finalEmbedCode_str;
		};
		
		
		//######################################//
		/* Add keyboard support */
		//######################################//
		this.setInputs = function(){
			var numInputs = document.querySelectorAll('input');
			for (var i = 0; i < numInputs .length; i++) {
				if(self.hasPointerEvent_bl){
					numInputs[i].addEventListener("pointerdown", self.inputFocusInHandler);
				}else if(numInputs[i].addEventListener){
					numInputs[i].addEventListener("mousedown", self.inputFocusInHandler);
					numInputs[i].addEventListener("touchstart", self.inputFocusInHandler);
				}
			}
		}
		
		this.inputFocusInHandler = function(e){
			//if(FWDUVPlayer.isSearchedFocused_bl) return;
			self.curInput = e.target;
			setTimeout(function(){
			
				if(self.hasPointerEvent_bl){
					window.addEventListener("pointerdown", self.inputFocusOutHandler);
				}else if(window.addEventListener){
					window.addEventListener("mousedown", self.inputFocusOutHandler);
					window.addEventListener("touchstart", self.inputFocusOutHandler);
				}
				FWDUVPlayer.isSearchedFocused_bl = true;
			}, 50);
		}
		
		this.inputFocusOutHandler = function(e){
			
			var vc = FWDUVPUtils.getViewportMouseCoordinates(e);	
			if(!FWDUVPUtils.hitTest(self.curInput, vc.screenX, vc.screenY)){
				if(self.hasPointerEvent_bl){
					window.removeEventListener("pointerdown", self.inputFocusOutHandler);
				}else if(window.removeEventListener){
					window.removeEventListener("mousedown", self.inputFocusOutHandler);
					window.removeEventListener("touchstart", self.inputFocusOutHandler);
				}
				//if(e.target && e.target.type != "text") 
				FWDUVPlayer.isSearchedFocused_bl = false;
				return;
			}
		};
		
		this.addKeyboardSupport = function(){
			self.setInputs();
			document.addEventListener("keydown",  this.onKeyDownHandler);	
			document.addEventListener("keyup",  this.onKeyUpHandler);
		};
		
		this.onKeyDownHandler = function(e){

			if(self.isSpaceDown_bl || !self.hasStartedToPlay_bl || FWDUVPlayer.isSearchedFocused_bl) return;
			self.isSpaceDown_bl = true;
			if(e.preventDefault) e.preventDefault();
			if(self != FWDUVPlayer.keyboardCurInstance && (FWDUVPlayer.videoStartBehaviour == "pause" || FWDUVPlayer.videoStartBehaviour == "none")) return
			//pause
			if (e.keyCode == 32){
				
				self.stickOnCurrentInstanceKey_bl = true;
				if(self.videoType_str == FWDUVPlayer.IMAGE || self.videoType_str == FWDUVPlayer.IFRAME){
					if(self.isImageAdsPlaying_bl){
						self.stopUpdateImageInterval();
					}else{
						self.startUpdateImageInterval();
					}
				}else if(self.videoType_str == FWDUVPlayer.YOUTUBE){
					if(!self.ytb_do.isSafeToBeControlled_bl) return;
					self.ytb_do.togglePlayPause();
				}else if(self.videoType_str == FWDUVPlayer.VIMEO){
					if(!self.vimeo_do.isSafeToBeControlled_bl) return;
					self.vimeo_do.togglePlayPause();
				}else if(self.videoType_str == FWDUVPlayer.MP3){
					if(!self.audioScreen_do.isSafeToBeControlled_bl) return;
					self.audioScreen_do.togglePlayPause();
				}else if(FWDUVPlayer.hasHTML5Video){
					if(!self.videoScreen_do.isSafeToBeControlled_bl) return;
					if(self.videoScreen_do) self.videoScreen_do.togglePlayPause();
				}else if(self.isFlashScreenReady_bl){
					self.flashObject.togglePlayPause();
				}
				if(e.preventDefault) e.preventDefault();
				return false;
			}else if (e.keyCode == 70){
				if(self.isFullScreen_bl){
					self.goNormalScreen();
				}else{
					self.goFullScreen();
				}
			}else if (e.keyCode == 77){
				if(self.volume != 0) self.lastVolume = self.volume;
				if(self.volume != 0){
					self.volume = 0;
				}else{
					self.volume = self.lastVolume;
				}
				self.setVolume(self.volume);
			}else if (e.keyCode == 38){
				self.volume += .1;
				if(self.volume > 1) self.volume = 1;
				self.setVolume(self.volume);
			}else if (e.keyCode == 40){
				self.volume -= .1;
				if(self.volume < 0) self.volume = 0;
				self.setVolume(self.volume);
			}else if (e.keyCode == 77){
				
				if(self.volume < 0) self.volume = 0;
				self.setVolume(self.volume);
			}else if (e.keyCode == 39 && !self.isAdd_bl){
				var curTime = self.getCurrentTime();
				if(curTime.length == 5) curTime = "00:" + curTime;
				if(curTime.length == 7) curTime = "0" + curTime;
				curTime = FWDUVPUtils.getSecondsFromString(curTime);
				curTime += 5;
				curTime = FWDUVPUtils.formatTime(curTime);
				if(curTime.length == 5) curTime = "00:" + curTime;
				if(curTime.length == 7) curTime = "0" + curTime;
				
				self.scrubbAtTime(curTime);
			}else if (e.keyCode == 37 && !self.isAdd_bl){
				var curTime = self.getCurrentTime();
				if(curTime.length == 5) curTime = "00:" + curTime;
				if(curTime.length == 7) curTime = "0" + curTime;
				curTime = FWDUVPUtils.getSecondsFromString(curTime);
				curTime -= 5;
				curTime = FWDUVPUtils.formatTime(curTime);
				if(curTime.length == 5) curTime = "00:" + curTime;
				if(curTime.length == 7) curTime = "0" + curTime;
				self.scrubbAtTime(curTime);
			}
		};
		
		this.onKeyUpHandler = function(e){
			self.isSpaceDown_bl = false;
		};
		
		this.setupAopw = function(){
			FWDUVPOPWindow.setPrototype();
			self.popw_do = new FWDUVPOPWindow(self.data, self);
		}
		
		//####################################//
		/* Setup hider */
		//####################################//
		this.setupHider = function(){
			FWDUVPHider.setPrototype();
			self.hider = new FWDUVPHider(self.main_do, self.controller_do, self.data.controllerHideDelay);
			self.hider.addListener(FWDUVPHider.SHOW, self.hiderShowHandler);
			self.hider.addListener(FWDUVPHider.HIDE, self.hiderHideHandler);
			self.hider.addListener(FWDUVPHider.HIDE_COMPLETE, self.hiderHideCompleteHandler);
		};
		
		this.hiderShowHandler = function(){
			if(self.controller_do) self.controller_do.show(true);
			if(self.logo_do && self.data.hideLogoWithController_bl && self.isPlaying_bl) self.logo_do.show(true);
			self.showCursor();
			
			if(self.isAdd_bl){
				self.positionAds(true);
				self.adsStart_do.showWithOpacity();
				self.adsSkip_do.showWithOpacity();	
			}
			
			if(self.subtitle_do) self.subtitle_do.position(true);
			if(self.popupAds_do) self.popupAds_do.position(true);
			
		};
		
		this.hiderHideHandler = function(){
			
			if(self.videoType_str == FWDUVPlayer.VIMEO){
				self.hider.reset();
				return;
			}
		
			if(self.controller_do.volumeScrubber_do && self.controller_do.isVolumeScrubberShowed_bl){
				self.hider.reset();
				return;
			}

			if(self.controller_do.atb && self.controller_do.atb.isShowed_bl){
				if(FWDUVPUtils.hitTest(self.controller_do.atb.mainHolder_do.screen, self.hider.globalX, self.hider.globalY)){
					self.hider.reset();
					return;
				}
				
			}
		
			if(self.data.showYoutubeQualityButton_bl && FWDUVPUtils.hitTest(self.controller_do.ytbButtonsHolder_do.screen, self.hider.globalX, self.hider.globalY)){
				self.hider.reset();
				return;
			}
			
			if(self.data.showPlaybackRateButton_bl && self.controller_do && FWDUVPUtils.hitTest(self.controller_do.playbackRatesButtonsHolder_do.screen, self.hider.globalX, self.hider.globalY)){
				self.hider.reset();
				return;
			}
			
			if(self.controller_do && self.data.showSubtitleButton_bl && FWDUVPUtils.hitTest(self.controller_do.subtitlesButtonsHolder_do.screen, self.hider.globalX, self.hider.globalY)){
				self.hider.reset();
				return;
			}
			
			if(FWDUVPUtils.hitTest(self.controller_do.screen, self.hider.globalX, self.hider.globalY)){
				self.hider.reset();
				return;
			}
			
			if(FWDUVPUtils.hitTest(self.controller_do.mainScrubber_do.screen, self.hider.globalX, self.hider.globalY)){
				self.hider.reset();
				return;
			}
			
			
			self.controller_do.hide(true);
			if(self.logo_do && self.data.hideLogoWithController_bl) self.logo_do.hide(true);
			if(self.isFullScreen_bl) self.hideCursor();
			
			if(self.isAdd_bl){
				self.positionAds(true);
				self.adsStart_do.hideWithOpacity();
				self.adsSkip_do.hideWithOpacity();	
			}
			
			self.subtitle_do.position(true);
			if(self.popupAds_do) self.popupAds_do.position(true);
		};
		
		this.hiderHideCompleteHandler = function(){
			self.controller_do.positionScrollBarOnTopOfTheController();
		};
		
		//####################################//
		// API
		//###################################//
		this.play = function(){
		
			if(!self.isAPIReady_bl) return;
			if(self.isMobile_bl && self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do && !self.ytb_do.isSafeToBeControlled_bl) return;
			
			if(self.videoType_str == FWDUVPlayer.HLS_JS){
				if(location.protocol.indexOf("file:") >= 0){
					self.main_do.addChild(self.info_do);
					self.info_do.showText("HLS m3u8 videos can't be played local on this browser, please test it online!.");
					return;
				}
			}
			
			if(!self.isAdd_bl && self.data.playlist_ar[self.id]["isPrivate"] && !self.hasPassedPassowrd_bl && self.passWindow_do){
				if(self.largePlayButton_do) self.largePlayButton_do.show();
				self.passWindow_do.show();
				return
			}
			self.hasPassedPassowrd_bl = true;
			
			if(self.isMobile_bl){
				  FWDUVPlayer.stopAllVideos(self);
			}else{
				if(FWDUVPlayer.videoStartBehaviour == FWDUVPlayer.PAUSE_ALL_VIDEOS){
					//FWDUVPlayer.pauseAllVideos(self);
				}else if(FWDUVPlayer.videoStartBehaviour == FWDUVPlayer.STOP_ALL_VIDEOS){
					FWDUVPlayer.stopAllVideos(self);
				}
			}
		
			//FWDUVPlayer.stopAllVideos(self);
			
			if(self.videoType_str == FWDUVPlayer.IMAGE){
				self.startUpdateImageInterval();
			}else if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.ytb_do.play();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.play();
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				//if(self.vimeo_do.isStopped_bl && (!self.vimeo_do.isVideoLoaded_bl && !self.useDeepLinking_bl)){
				//	self.playVimeoWithDelay();
				//}else{
					self.vimeo_do.play();
				//}
			}else if(FWDUVPlayer.hasHTML5Video){
				
				if(self.videoType_str == FWDUVPlayer.HLS_JS && !self.isHLSManifestReady_bl){
					self.videoScreen_do.initVideo();
					self.setupHLS();
					self.hlsJS.loadSource(self.videoSourcePath_str);
					self.hlsJS.attachMedia(self.videoScreen_do.video_el);
					
					self.hlsJS.on(Hls.Events.MANIFEST_PARSED,function(e){
						self.isHLSManifestReady_bl = true;
						if(self.videoType_str == FWDUVPlayer.HLS_JS) self.play();
					});
				}else{
					if(self.videoScreen_do) self.videoScreen_do.play();
				}
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.playVideo();
				self.scrub(0);
			}
			
			FWDUVPlayer.keyboardCurInstance = self;
			self.videoPoster_do.allowToShow_bl = false;
			
			self.largePlayButton_do.hide();
			self.videoPoster_do.hide();
		};
		
		this.pause = function(){
			if(!self.isAPIReady_bl) return;
			
			if(self.videoType_str == FWDUVPlayer.IMAGE){
				self.stopUpdateImageInterval();
			}else if(self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.ytb_do.pause();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.pause();
			}else if(self.videoType_str == FWDUVPlayer.VIMEO){
				self.vimeo_do.pause();
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.pause();
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.pauseVideo();
			}
		};
		
		this.resume = function(){
			if(!self.isAPIReady_bl) return;
			
			if(self.videoType_str == FWDUVPlayer.IMAGE){
				self.startUpdateImageInterval();
			}else if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.ytb_do.resume();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.resume();
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.vimeo_do.resume();
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.resume();
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.resume();
			}
		};
		
		this.sendPlayEvent =  function(){
			
		}
		
		this.sendGAPlayedEvent = function(){
		
			if(!isNaN(self.totalPercentPlayed) && window["ga"]){
				if(Math.round(self.totalPercentPlayed * 100)){
					var gaLabel = 'videoName:' + self.videoNameGa +  ', percentPlayed:' + Math.round(self.totalPercentPlayed * 100)  + ', stoppedAtTime:' + self.getCurrentTime() + ', fullScreen:' +  self.isFullScreen_bl + '';
					
					ga('send', {
					  hitType: 'event',
					  eventCategory: self.videoCat,
					  eventAction: 'played',
					  eventLabel: gaLabel,
					  nonInteraction: true
					});
					
					self.totalTimePlayed = 0;
					self.totalPercentPlayed = 0;
				}
			}
		}
		
		
		this.stop = function(source){
			if(!self.isAPIReady_bl) return;
			
			self.sendGAPlayedEvent();
		
			self.hasPassedPassowrd_bl = false;
			self.isHLSManifestReady_bl = false;
			clearInterval(self.tryHLS_int);
			clearInterval(self.checkIfYoutubePlayerIsReadyId_int);
			clearInterval(self.keepCheckingYoutubeAPI_int);
			self.destroyHLS();
			self.isPlaying_bl = false;
			if(self.customContextMenu_do) self.customContextMenu_do.disable();
			
			//console.log("########### stop ########## " + self.videoType_str)
			
			if(self.videoType_str == FWDUVPlayer.IMAGE){
				self.stopUpdateImageInterval();
			}else if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.ytb_do.stop();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.stop();
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.vimeo_do.stop();
			}else if(FWDUVPlayer.hasHTML5Video){
				self.videoScreen_do.stop();
			}
			//if(self.videoType_str == FWDUVPlayer.MP3) self.videoScreenUpdateTimeHandler({curTime:"00:00", totalTime:"00:00"});
			
			clearTimeout(self.playVimeoWhenLoadedId_to); 
			if(self.popw_do) self.popw_do.hide();
			
			if(self.isMobile_bl){

				if(self.data.showControllerWhenVideoIsStopped_bl && self.controller_do) self.controller_do.show(true);
				
				if(!source && self.videoType_str != FWDUVPlayer.YOUTUBE){
				
					self.videoPoster_do.show();
					if(self.videoType_str != FWDUVPlayer.VIMEO) self.largePlayButton_do.show();
				}else if(self.useYoutube_bl){
					if(self.ytb_do && !self.ytb_do.ytb){ 
						self.ytb_do.setupVideo();
					}
				}
			}else{
				if(!self.isThumbClick_bl && !self.isAdd_bl){
				
					if(self.controller_do && self.data.showControllerWhenVideoIsStopped_bl) self.controller_do.show(true);
					if(self.videoPoster_do) self.videoPoster_do.show();
					if(self.largePlayButton_do) self.largePlayButton_do.show();
				}
			}
			
			if(self.controller_do){
				if(self.controller_do.atb) self.controller_do.atb.hide(true);
				if(self.controller_do.subtitleButton_do){
					self.controller_do.disableSubtitleButton();
					if(self.subtitle_do){
						if(self.subtitle_do.showSubtitleByDefault_bl){
							self.controller_do.subtitleButton_do.setButtonState(0);
						}else{
							self.controller_do.subtitleButton_do.setButtonState(1);
						}
					}
				}
				if(self.controller_do.thumbnailsPreview_do) self.controller_do.thumbnailsPreview_do.remove();

				if(self.controller_do.atbButton_do){
					self.controller_do.atbButton_do.doNotallowToSetNormal = false;
					self.controller_do.atbButton_do.isSelected = false;
					self.controller_do.atbButton_do.setNormalState();
				}

				self.controller_do.disableAtbButton();
				if(self.controller_do.ttm) self.controller_do.ttm.hide();
				if(self.controller_do.ytbQualityButton_do) self.controller_do.ytbQualityButton_do.disable();
				if(self.controller_do.playbackRateButton_do) self.controller_do.playbackRateButton_do.disable();
				if(self.controller_do && self.controller_do.rewindButton_do) self.controller_do.rewindButton_do.disable();
			}
			
			if(self.popupAds_do){
				self.popupAds_do.hideAllPopupButtons(false);
			}
			
			self.hasHlsPlayedOnce_bl = false;
			self.isSafeToScrub_bl = false;
			self.hlsState = undefined;
			self.changeHLS_bl = false;
			
			if(self.controller_do) self.controller_do.disablePlaybackRateButton();
			if(self.subtitle_do) self.subtitle_do.hide();
			if(self.annotations_do) self.annotations_do.update(-1);
			if(self.hider) self.hider.reset();
			self.showCursor();
			if(self.adsStart_do) self.adsStart_do.hide(true);
			if(self.adsSkip_do) self.adsSkip_do.hide(true);
			if(self.controller_do) self.controller_do.hideAdsLines();
			self.stopVisualization();
			self.totalDuration = 0;
			self.hasStartedToPlay_bl = false;
			//self.prevDuration = 0;
			//self.prevSource = -1;
		};
		
		this.startToScrub = function(){
			if(!self.isAPIReady_bl) return;
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do && self.ytb_do.isSafeToBeControlled_bl){
				self.ytb_do.startToScrub();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.startToScrub();
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.startToScrub();
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.startToScrub();
			}
		};
		
		this.stopToScrub = function(){
			if(!self.isAPIReady_bl) return;
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do && self.ytb_do.isSafeToBeControlled_bl){
				self.ytb_do.stopToScrub();
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.stopToScrub();
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.stopToScrub();
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.stopToScrub();
			}
		};
		
		this.scrubbAtTime = function(duration){
			if(!self.isAPIReady_bl || !duration) return;
			if(String(duration).indexOf(":") != -1) duration = FWDUVPUtils.getSecondsFromString(duration);
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do && self.ytb_do.isSafeToBeControlled_bl){
				self.ytb_do.scrubbAtTime(duration);
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.vimeo_do.scrubbAtTime(duration);
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.scrubbAtTime(duration);
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.scrubbAtTime(duration);
			}
		};
		
		this.scrub = function(percent){
			if(!self.isAPIReady_bl) return;
			if(isNaN(percent)) return;
			if(percent < 0){
				percent = 0;
			}else if(percent > 1){
				percent = 1;
			}
			
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do && self.ytb_do.isSafeToBeControlled_bl){
				self.ytb_do.scrub(percent);
			}else if(self.videoType_str == FWDUVPlayer.MP3){
				if(self.audioScreen_do) self.audioScreen_do.scrub(percent);
			}else if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do && self.vimeo_do.isSafeToBeControlled_bl){
				self.vimeo_do.scrub(percent);
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.scrub(percent);
			}else if(self.isFlashScreenReady_bl){
				self.flashObject.scrub(percent);
			}
		};
		
		this.setVolume = function(volume){
			if(!self.isAPIReady_bl) return;
			self.volume = volume;
			if(self.controller_do) self.controller_do.updateVolume(volume, true);
			
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.ytb_do.setVolume(volume);
			}
			
			if(self.videoType_str == FWDUVPlayer.VIMEO && self.vimeo_do){
				self.vimeo_do.setVolume(volume);
			}
			
			if(self.audioScreen_do){
				self.audioScreen_do.setVolume(volume);
			}
			
			if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.videoScreen_do.setVolume(volume);
			}
			
			if(self.isFlashScreenReady_bl){
				self.flashObject.setVolume(volume);
			}
			self.dispatchEvent(FWDUVPlayer.VOLUME_SET, {volume:volume});
		};
			
		this.showCategories = function(){
		
			if(!self.isAPIReady_bl) return;
			if(self.videoType_str == FWDUVPlayer.YOUTUBE && self.ytb_do){
				self.isVideoPlayingWhenOpenWindows_bl = self.ytb_do.isPlaying_bl;
			}else if(FWDUVPlayer.hasHTML5Video){
				if(self.videoScreen_do) self.isVideoPlayingWhenOpenWindows_bl = self.videoScreen_do.isPlaying_bl;
			}
			
			if(self.categories_do){
				self.categories_do.show(self.catId);
				//if(self.customContextMenu_do) self.customContextMenu_do.updateParent(self.categories_do);
				if(self.controller_do) self.controller_do.setCategoriesButtonState("selected");
				self.pause();
			}
		};
		
		this.hideCategories = function(){
			if(!self.isAPIReady_bl) return;
			if(self.categories_do){
				self.categories_do.hide();
				if(self.controller_do) self.controller_do.setCategoriesButtonState("unselected");
			}
		};
		
		this.showPlaylist = function(){
			if(!self.isAPIReady_bl || !self.showPlaylistButtonAndPlaylist_bl) return;
			self.isPlaylistShowed_bl = false;
			
			if(self.controller_do) self.controller_do.showHidePlaylistButton();
			self.playlist_do.hide(!self.isMobile_bl);
			self.resizeHandler(!self.isMobile_bl);
			self.setStageContainerFinalHeightAndPosition(self.animate_bl);
			if(FWDUVPUtils.isSafari && FWDUVPUtils.isWin){
				self.playlist_do.hide(false);
				self.resizeHandler(false);
			}else{
				if(!self.isMobile_bl){
					FWDAnimation.to(self, .8, {tempStageWidth:self.stageWidth,
												 tempStageHeight:self.stageHeight,
												 tempVidStageWidth:self.vidStageWidth,
												 tempVidStageHeight:self.vidStageHeight,
												 ease:Expo.easeInOut,
												 onUpdate:self.resizeFinal});
				}
			}
		};
		
		this.hidePlaylist = function(){
			if(!self.isAPIReady_bl || !self.showPlaylistButtonAndPlaylist_bl) return;
			
			self.isPlaylistShowed_bl = true;
			if(self.controller_do) self.controller_do.showHidePlaylistButton();
			if(self.controller_do) self.controller_do.showShowPlaylistButton();
			self.playlist_do.show(!self.isMobile_bl);
			self.resizeHandler(!self.isMobile_bl);
			self.setStageContainerFinalHeightAndPosition(self.animate_bl);
			
			if(FWDUVPUtils.isSafari && FWDUVPUtils.isWin){
				self.playlist_do.show(false);
				self.resizeHandler(false);
			}else{
				if(!self.isMobile_bl){
					FWDAnimation.to(self, .8, {tempStageWidth:self.stageWidth,
												 tempStageHeight:self.stageHeight,
												 tempVidStageWidth:self.vidStageWidth,
												 tempVidStageHeight:self.vidStageHeight,
												 ease:Expo.easeInOut,
												 onUpdate:self.resizeFinal});
				}
			}
			
		};
		
		this.setPosterSource = function(path){
			if(!self.isAPIReady_bl || !path) return;
			var path_ar = path.split(",");
				
			if(self.isMobile_bl && path_ar[1] != undefined){
				path = path_ar[1];
			}else{
				path = path_ar[0];
			}
			
			if(!self.videoPoster_do) return;
			self.posterPath_str = path;
		
			if((self.videoSourcePath_str.indexOf(".") == -1 && self.videoType_str == FWDUVPlayer.YOUTUBE && self.isMobile_bl)
			   || (self.videoSourcePath_str.indexOf("vimeo.com") == -1 && self.videoType_str == FWDUVPlayer.VIMEO && self.isMobile_bl)){
				self.videoPoster_do.setPoster("youtubemobile");
			}else{
				self.videoPoster_do.setPoster(self.posterPath_str);
				if(self.prUVPosterSource_str != path) self.dispatchEvent(FWDUVPlayer.UPDATE_POSTER_SOURCE);
			}
			self.prUVPosterSource_str = path;
		};
		
		//#####################################################//
		/* Update ads */
		//#####################################################//
		this.updateAds = function(duration, setSourceOverwrite){
			//if(!duration) duration = 0;

			if(!self.data.playlist_ar[self.id]) return;
		
			if(self.data.playlist_ar[self.id].vastURL && !self.data.playlist_ar[self.id].ads_ar){
				self.data.loadVast(self.data.playlist_ar[self.id].vastURL);
				return;
			}
		
			self.curAddData = self.data.playlist_ar[self.id].ads_ar;
			
			if(!this.isAdd_bl && self.curAddData){
					
				self.TrackingEvents = undefined;
				self.Impression = undefined;
				self.ClickTracking = undefined;
			
				self.callFirstQuartile = true;
				self.callMidpoint = true;
				self.callThirdQuartile = true;
				
				self.curSource = self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["source"];
				
				for(var i=0; i<self.data.playlist_ar[self.id].ads_ar.length; i++){
					if(duration >= self.curAddData[i].timeStart && duration <= (self.curAddData[i].timeStart + 1)){
						self.addSource_str = self.curAddData[i].source;
					}
				}
			
				for(var i=0; i<self.curAddData.length; i++){
					if(duration >= self.curAddData[i].timeStart && duration <= (self.curAddData[i].timeStart + 1) 
						&& !self.curAddData[i].played_bl && self.addSource_str != self.prvAdSource){
						self.addId = i;
						if(self.curAddData[i].timeStart == 0) setSourceOverwrite = false;
						self.isAdd_bl = true;
					
						self.addSource_str = self.curAddData[i].source;
						self.curAddData[self.addId].played_bl = true;
						if(self.curAddData) self.curAddData[self.addId].played_bl = true;
						setTimeout(function(){
							if(self.curAddData) self.curAddData[self.addId].played_bl = true;
						},50);
						self.data.adsThumbnailPath_str = self.curAddData[i].thumbnailSource;
						self.data.timeToHoldAds = self.curAddData[i].timeToHoldAds;
						self.data.adsPageToOpenURL_str = self.curAddData[i].link;
						self.data.adsPageToOpenTarget_str = self.curAddData[i].target;
						self.TrackingEvents = self.curAddData[i].TrackingEvents;
						
						self.Impression = self.curAddData[i].Impression
						self.ClickTracking = self.curAddData[i].ClickTracking
						self.scrubAfterAddDuration = self.curAddData[i].timeStart;
						self.curImageTotalTime = self.curAddData[i].addDuration;
						self.setSource(self.addSource_str);
						
						
						//if(self.curAddData[i].timeStart != 0) self.loadAddFirstTime_bl = false;
						//if(self.videoType_str == FWDUVPlayer.VIDEO && !self.isMobile_bl && loadAddFirstTime_bl) self.play();
						
						if(this.controller_do && this.controller_do.line_ar) this.controller_do.line_ar[i].setVisible(false);
						self.prvAdSource = self.addSource_str;
						return;
					}
				}
			}else{
				if(!this.isAdd_bl){
					self.curSource = self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["source"];
				}else{
					self.curSource = "FWDUVPDummy" + new Date().getTime();
				}
			}
		
			if(this.controller_do){
				self.controller_do.setupAdsLines(self.curAddData, self.id, self.catId);
				if(self.totalDuration) self.controller_do.positionAdsLines(self.totalDuration);
			}
			
			
			self.isLive = self.data.playlist_ar[self.id]["isLive"];
			
			if(!this.isAdd_bl && self.prevSource != self.curSource && self.curSource.indexOf("FWDUVPDummy") == -1 || setSourceOverwrite){
				if(setSourceOverwrite){
					this.isAdd_bl = false;
					self.curSource = self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["source"]
				}
				self.setSource(self.curSource, false, self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo]["is360"]);
			}
			if(this.controller_do) this.controller_do.positionAdsLines(self.curDuration);
			self.prevDuration = duration;
			
			self.prevSource = self.curSource;
			
		};
		
		//#####################################################//
		/* Setup image screen */
		//#####################################################//
		this.updateImageScreen = function(source){
			if(!this.imageSceeenHolder_do){
				this.imageSceeenHolder_do = new FWDUVPDisplayObject("div");
				this.imageSceeenHolder_do.setX(0);
				this.imageSceeenHolder_do.setY(0);
				this.imageSceeenHolder_do.setBkColor("#000000");
			}
			
			self.videoHolder_do.addChildAt(self.imageSceeenHolder_do,  self.videoHolder_do.getChildIndex(self.dumyClick_do) - 1);
			self.showClickScreen();
			if(self.imageSceeenHolder_do.contains(self.imageScreen_do)) self.imageSceeenHolder_do.removeChild(this.imageScreen_do);
			this.imageScreen_do = null;
			
			self.imageScreen_do = new FWDUVPDisplayObject("img");
			
			self.imageAdd_img = new Image()
			self.imageAdd_img.src = source;
		
			if(self.preloader_do){
				self.preloader_do.show(false);
				//self.preloader_do.startPreloader();
			}
			if(self.largePlayButton_do) self.largePlayButton_do.hide();
			
			self.imageAdd_img.onload = function(){
				self.imageScreen_do.setScreen(self.imageAdd_img);
				self.imageScreen_do.setAlpha(0);
				FWDAnimation.to(self.imageScreen_do, 1, {alpha:1});
				self.imageAddOriginalWidth = self.imageAdd_img.width;
				self.imageAddOriginalHeight = self.imageAdd_img.height;
				if(self.preloader_do) self.preloader_do.hide();
				self.imageSceeenHolder_do.addChild(self.imageScreen_do);
				self.positionAdsImage();
				self.startToUpdateAdsButton();
			}
			
			self.imageAdd_img.onerror = function(){
				self.main_do.addChild(self.info_do);
				self.info_do.showText("Advertisment image with path " +  source + " can't be found");
				if(self.preloader_do) self.preloader_do.hide();
				return;
			}
		}
		
		this.positionAdsImage = function(){
			if(!self.imageScreen_do || self.videoType_str != FWDUVPlayer.IMAGE) return;
			var scaleX = self.tempVidStageWidth/self.imageAddOriginalWidth;
			var scaleY = self.tempVidStageHeight/self.imageAddOriginalHeight;
			
			totalScale = 0;
			if(scaleX >= scaleY){
				totalScale = scaleX;
			}else if(scaleX <= scaleY){
				totalScale = scaleY;
			}
			
			finalW = parseInt(self.imageAddOriginalWidth * totalScale);
			finalH = parseInt(self.imageAddOriginalHeight * totalScale);
			finalX = parseInt((self.tempVidStageWidth - finalW)/2);
			finalY = parseInt((self.tempVidStageHeight - finalH)/2);
			
			
			self.imageScreen_do.setWidth(finalW); 
			self.imageScreen_do.setHeight(finalH); 
			self.imageScreen_do.setX(finalX); 
			self.imageScreen_do.setY(finalY); 
			self.imageSceeenHolder_do.setWidth(self.tempVidStageWidth);
			self.imageSceeenHolder_do.setHeight(self.tempVidStageHeight);
		}
		
		this.startToUpdateAdsButton = function(){
			self.curImageTime = 0;
			self.updateAdsButton();
			self.stopUpdateImageInterval();
			self.startUpdateImageInterval();
			self.setPlayAndPauseButtonState();	
		}
		
		this.stopUpdateImageInterval = function(){
			self.isImageAdsPlaying_bl = false;
			clearInterval(self.startUpdateAdsId_int);
			self.setPlayAndPauseButtonState();
			//if(self.largePlayButton_do) self.largePlayButton_do.show();
			self.isPlaying_bl = false;
			self.hider.stop();	
		}
		
		this.startUpdateImageInterval = function(){
			self.isImageAdsPlaying_bl = true;
			self.startUpdateAdsId_int = setInterval(self.updateAdsButton, 1000);
			self.setPlayAndPauseButtonState();
			//if(self.largePlayButton_do) self.largePlayButton_do.hide();
			self.isPlaying_bl = true;
			self.hider.start();
		}
		
		this.updateAdsButton = function(){
			self.videoScreenUpdateTimeHandler({curTime:FWDUVPUtils.formatTime(self.curImageTime), totalTime:FWDUVPUtils.formatTime(self.curImageTotalTime), seconds:self.curImageTime});
			self.videoScreenUpdateHandler({percent:self.curImageTime/self.curImageTotalTime});
			if(self.curImageTime == self.curImageTotalTime) self.videoScreenPlayCompleteHandler();
			self.curImageTime += 1;
		}
		
		this.setPlayAndPauseButtonState = function(){
			if(this.isImageAdsPlaying_bl){
				if(self.controller_do) self.controller_do.showPauseButton();
			}else{
				if(self.controller_do) self.controller_do.showPlayButton();
			}
		}
		
		
		this.setSource = function(source, overwrite, is360){
			if(source) self.source = source;
			if(self.data.playVideoOnlyWhenLoggedIn_bl && !self.data.isLoggedIn_bl){
				self.main_do.addChild(self.info_do);
				self.info_do.showText(self.data.loggedInMessage_str);
				self.info_do.allowToRemove_bl = false;
				if(self.largePlayButton_do) self.largePlayButton_do.show();
				return;
			}
			
			//Loading thumbnails preview.
			if(self.data.playlist_ar[self.id]['thumbnailsPreview']){
				if(location.protocol.indexOf("file:") != -1){
					//self.info_do.allowToRemove_bl = false;
					setTimeout(function(){
						self.main_do.addChild(self.info_do);
						self.info_do.showText("This browser doesn't allow thumbnails preview videos local, please test online.");
						self.resizeHandler();
					}, 50);
				}

				if(self.data.playlist_ar[self.id]['thumbnailsPreview'].length > 2
				   && location.protocol.indexOf("file:") == -1
				   && !self.thumbnailsPreviewLoaded_bl){
					var script = document.createElement('script');
					script.src =  self.mainFolderPath_str + 'java/FWDUVPThumbnailsPreview.js'
					document.head.appendChild(script);
					
					script.onerror = function(e){
						self.main_do.addChild(self.info_do);
						self.info_do.showText('The thumbnails preview javascript file named <font color="#FF0000">FWDUVPThumbnailsPreview.js</font> is not found. Please make sure that the content folder contains the java folder that contains the <font color="#FF0000">FWDUVPThumbnailsPreview.js</font> file.');
					}
					
					script.onload = function () {
						self.thumbnailsPreviewLoaded_bl = true;
						self.setSource(self.source);
					}
					return;
				}
			}
			
			self.hasThumbnailsPreview = false;
			if(self.data.playlist_ar[self.id]['thumbnailsPreview'] && self.data.playlist_ar[self.id]['thumbnailsPreview'].length > 2){
				self.hasThumbnailsPreview = true;
				if(self.controller_do) self.controller_do.setupThumbnailsPreview()
			}
			
			if(!self.isAPIReady_bl || self.id == -1) return;
			
			if(source.indexOf("encrypt:") != -1) source = atob(source.substr(8));
			
			//if(source.indexOf(".") == -1) obj.videoSource = "https://www.youtube.com/watch?v=" + obj.videoSource;
			
			//if(source == self.videoSourcePath_str) return;+
			
			if(self.id < 0){
				self.id = 0;
			}else if(self.id > self.totalVideos - 1){
				self.id = self.totalVideos - 1;
			}
			
		
			if(self.data.playlist_ar[self.id] == undefined) return;
			
			self.stop(source);
			
		
			if(self.controller_do) self.controller_do.setIsLive(self.isLive);
			
			if(self.playlist_do && self.playlist_do.curId != self.id){
				//self.addId = Math.random() * 999999999;
				self.prvAdSource = Math.random() * 999999999;
				if(!self.data.playAdsOnlyOnce_bl){
					for(var i=0; i<self.data.playlist_ar.length; i++){
						if(self.data.playlist_ar[i].ads_ar){
							for(var j=0; j<self.data.playlist_ar[i].ads_ar.length; j++){
								self.data.playlist_ar[i].ads_ar[j].played_bl = false;
							}
						}	
					}
				}
			}
			
			if(source.indexOf("vimeo.com") != -1 
			   && source.indexOf(".m3u8") == -1
			   && source.indexOf(".mp4") == -1){
				self.videoType_str = FWDUVPlayer.VIMEO;
			}else if(source.indexOf("youtube.") != -1){
				self.videoType_str = FWDUVPlayer.YOUTUBE;
			}else if(source.toLowerCase().indexOf(".mp3") != -1){
				self.videoType_str = FWDUVPlayer.MP3;
				if(self.controller_do) self.controller_do.setX(0);
			}else if(source.indexOf(".jpg") != -1 
					|| source.indexOf(".jpeg") != -1 
					|| source.indexOf(".png") != -1
			){
				self.videoType_str = FWDUVPlayer.IMAGE;
				if(self.controller_do) self.controller_do.setX(0);
			}else{
				if(self.controller_do) self.controller_do.setX(0);
				if(!self.isMobile_bl && !FWDUVPlayer.hasHTMLHLS && source.indexOf(".m3u8") != -1){
					self.videoType_str = FWDUVPlayer.HLS_JS;
				}else{
					self.videoType_str = FWDUVPlayer.VIDEO;
				}
			}
			
			//console.log("***** set source ******** " + source + " - " + self.videoType_str);
			
			if(!self.isMobile_bl && !FWDUVPlayer.hasHTMLHLS && source.indexOf(".m3u8") != -1 && !self.isHLSJsLoaded_bl && !FWDUVPlayer.isHLSJsLoaded_bl){
				
				if(location.protocol.indexOf("file:") != -1){
					//self.info_do.allowToRemove_bl = false;
					self.main_do.addChild(self.info_do);
					self.info_do.showText("This browser doesn't allow playing HLS / live streaming videos local, please test online.");
					self.resizeHandler();
					return;
				}
				
				var script = document.createElement('script');
				script.src = self.data.hlsPath_str;
				document.head.appendChild(script); //or something of the likes
				script.onerror = function(){
					self.main_do.addChild(self.info_do);
					self.info_do.showText("Error loading HLS library <font color='#FF0000'>" + self.data.hlsPath_str + "</font>.");
					if(self.preloader_do) self.preloader_do.hide();
					return;
				}
				
				script.onload = function () {
					self.isHLSJsLoaded_bl = true;
					FWDUVPlayer.isHLSJsLoaded_bl = true;
					self.setupHLS();
					self.setSource(source);
				}
				
				if(!self.autoPlay_bl && !self.isThumbClick_bl){
					self.setPosterSource(self.posterPath_str);
					if(self.videoPoster_do) self.videoPoster_do.show();
					if(self.largePlayButton_do) self.largePlayButton_do.show();
				}
				return;
			}
			
			if(source.indexOf("youtube.") != -1 && !self.ytb_do){
				setTimeout(function(){
					
					if(self.showPreloader_bl){
						self.main_do.addChild(self.preloader_do);	
						if(self.preloader_do){
							self.preloader_do.show(false);
							//self.preloader_do.startPreloader();
						}
						if(self.largePlayButton_do) self.largePlayButton_do.hide();
						
						if(location.protocol.indexOf("file:") != -1 && FWDUVPUtils.isIE) self.main_do.addChild(self.info_do);
					}
				}, 50);
				
				if(location.protocol.indexOf("file:") != -1 && FWDUVPUtils.isIE){
					//self.info_do.allowToRemove_bl = false;
					self.main_do.addChild(self.info_do);
					self.info_do.showText("This browser doesn't allow the Youtube API to run local, please test it online or in another browser like Firefox or Chrome.");
			
					self.resizeHandler();
					return;
				}	
				
				self.setupYoutubeAPI();
				return;
			}
		
			if(source.indexOf("vimeo.") != -1 && !self.vimeo_do && self.videoType_str == FWDUVPlayer.VIMEO){
					
				if(location.protocol.indexOf("file:") != -1){
					//self.info_do.allowToRemove_bl = false;
					self.main_do.addChild(self.info_do);
					self.info_do.showText("This browser doesn't allow playing Vimeo videos local, please test online.");
					self.resizeHandler();
					return;
				}
				
				
				if(self.showPreloader_bl){
					self.main_do.addChild(self.preloader_do);	
					if(self.preloader_do){
						self.preloader_do.show(false);
						//self.preloader_do.startPreloader();
					}
				}
				if(self.largePlayButton_do) self.largePlayButton_do.hide();
			
				self.setupVimeoAPI();
				return;
			}
			
			self.is360 = is360;
			
			if(self.videoType_str != FWDUVPlayer.VIDEO)  self.is360 = false;
			if(self.is360 && !self.isThreeJsOrbigLoaded_bl){
					
				if(FWDUVPUtils.isLocal){
					self.main_do.addChild(self.info_do);
					self.info_do.showText("This browser doesn't allow playing 360 videos local, please test online.");
					if(self.preloader_do) self.preloader_do.hide();
					return;
				}
				
				if(!FWDUVPUtils.hasWEBGL){
					self.main_do.addChild(self.info_do);
					self.info_do.showText("Playing 360 videos in this browser is not possible because it doesn't support WEBGL.");
					if(self.preloader_do) self.preloader_do.hide();
					return;
				}
				
				if(!self.isThreeJsLoaded_bl && !FWDUVPlayer.hasThreeJsLoaded_bl){
					var script = document.createElement('script');
					script.src = self.data.threeJsPath_str;
					script.onerror = function(){
						self.main_do.addChild(self.info_do);
						self.info_do.showText("Error loading 360 degree library <font color='#FF0000'>" + self.data.threeJsPath_str + "</font>.");
						if(self.preloader_do) self.preloader_do.hide();
						return;
					}
					script.onload = function () {
						self.isThreeJsOrbigLoaded_bl = true;
						
							var script2 = document.createElement('script');
							script2.src = self.data.threeJsControlsPath_str;
							script2.onerror = function(){
								self.main_do.addChild(self.info_do);
								self.info_do.showText("Error loading three.js from <font color='#FF0000'>" + self.data.threeJsControlsPath_str + "</font>.");
								if(self.preloader_do) self.preloader_do.hide();
								return;
							}
							script2.onload = function () {
								FWDUVPlayer.hasThreeJsLoaded_bl = true;
								self.isThreeJsOrbitLoaded_bl = true;
								if(self.isThreeJsOrbigLoaded_bl && self.isThreeJsOrbitLoaded_bl) self.setSource(source, true, true);
								clearTimeout(self.load360ScriptsId_to);
								if(self.preloader_do) self.preloader_do.hide();
							};
							document.head.appendChild(script2); //or something of the likes	
							};

					document.head.appendChild(script); //or something of the likes
					
					
					this.load360ScriptsId_to = setTimeout(function(){
						if(self.showPreloader_bl){
							if(self.preloader_do){
								self.preloader_do.show(false);
								//self.preloader_do.startPreloader();
							}
						}
					},1000);
					return;
				}
			}
			
			self.videoSourcePath_str = source;
			self.finalVideoPath_str = source;
		
			
			if(self.is360){
				self.dumyClick_do.getStyle().cursor = 'url(' + self.data.handPath_str + '), default';
			}else{
				self.dumyClick_do.getStyle().cursor = "auto";
			}
			
			if(self.data.playlist_ar[self.id] && self.data.playlist_ar[self.id].scrubAtTimeAtFirstPlay) self.playAtTime_bl = true;
			if(self.controller_do && self.controller_do.rewindButton_do) self.controller_do.rewindButton_do.disable();
		
			self.popwSource = self.data.playlist_ar[self.id].dataAdvertisementOnPauseSource;
			if(self.data.playlist_ar[self.id] && self.data.playlist_ar[self.id].dataAdvertisementOnPauseSource){
				self.showPopW_bl = true;
			}else{
				self.showPopW_bl = false;
			}
			
			self.cuePointsSource_ar = self.data.playlist_ar[self.id].cuepoints_ar;
			
			if(!source) source = self.data.playlist_ar[self.id].videoSource[self.data.playlist_ar[self.id].startAtVideo].source;

			if(source.indexOf("youtube.") != -1){
				var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
				source = source.match(regExp)[2];
			}
				
			//if(source == self.prevVideoSource_str && !self.isAdd_bl && !overwrite) return;
			
			if(self.controller_do) self.controller_do.enablePlayButton();
			self.prevVideoSource_str = source;
			
			if(!source){
				self.main_do.addChild(self.info_do);
				self.info_do.showText("Video source is not defined!");
				return;
			}
		
			if(self.playlist_do){
				self.playlist_do.curId = self.id;
				self.playlist_do.checkThumbsState();
			}
			
			if(self.controller_do && self.data.playlist_ar[self.id].subtitleSource && self.data.playlist_ar[self.id].subtitleSource.length > 1){
				self.controller_do.updateSubtitleButtons(self.data.playlist_ar[self.id].subtitleSource, self.data.playlist_ar[self.id].startAtSubtitle);
			}
			
			if(self.controller_do) self.controller_do.updateHexColorForScrubber(self.isAdd_bl);
			
			self.posterPath_str = self.data.playlist_ar[self.id].posterSource;
			self.annotations_ar = self.data.playlist_ar[self.id].annotations_ar;
			self.annotations_do.setupAnnotations(self.annotations_ar);
			
			/*
			if(self.isAdd_bl && source.indexOf(".") ==  -1){
				setTimeout(function(){
					self.main_do.addChild(self.info_do);
					self.info_do.showText("Advertisment youtube videos are not supported, please make sure you are using a mp4 video file.");
				}, 200);
				return;
			}
			*/
			
			if(self.popupAds_do){
				if(self.data.playlist_ar[self.id].popupAds_ar){
					self.popupAds_do.resetPopups(self.data.playlist_ar[self.id].popupAds_ar);
					self.popupAds_do.id = self.curId;
				}else{
					self.popupAds_do.hideAllPopupButtons(true);
				}
			}
			
			//self.data.startAtPlaybackIndex = 3;
			self.startAtPlaybackIndex = self.data.startAtPlaybackIndex;
			
			if(self.data.playlist_ar[self.id]["dataPlaybackRate"] == "0.25"){
				self.startAtPlaybackIndex = 5;
			}else if(self.data.playlist_ar[self.id]["dataPlaybackRate"] == "0.5"){
				self.startAtPlaybackIndex = 4;
			}else if(self.data.playlist_ar[self.id]["dataPlaybackRate"] == "1"){
				self.startAtPlaybackIndex = 3;
			}else if(self.data.playlist_ar[self.id]["dataPlaybackRate"] == "1.25"){
				self.startAtPlaybackIndex = 2;
			}else if(self.data.playlist_ar[self.id]["dataPlaybackRate"] == "1.5"){
				self.startAtPlaybackIndex = 1;
			}else if(self.data.playlist_ar[self.id]["dataPlaybackRate"] == "2"){
				self.startAtPlaybackIndex = 0;
			}
			
			self.prevVideoSourcePath_str = self.videoSourcePath_str;
			self.resizeHandler(false, true);
		
			//Image
			if(self.videoType_str == FWDUVPlayer.IMAGE){
				self.updateImageScreen(self.videoSourcePath_str);
				if(self.videoPoster_do) self.videoPoster_do.setX(-5000);
				return;
			}else{
				
				if(self.videoHolder_do.contains(self.imageSceeenHolder_do)) self.videoHolder_do.removeChild(self.imageSceeenHolder_do);
		   		if(self.videoPoster_do) self.videoPoster_do.setX(0);
			}
			
			if(self.getVideoSource()) self.dispatchEvent(FWDUVPlayer.UPDATE_VIDEO_SOURCE);
			
			
			//Vimeo
			if(self.videoType_str == FWDUVPlayer.VIMEO){
			
				if(self.ytb_do) self.ytb_do.setX(-5000);
				if(self.videoScreen_do) self.videoScreen_do.setX(-5000);
				if(self.vimeo_do.x != 0) self.vimeo_do.setX(0);
			
				if(self.isAdd_bl){
					
					self.showClickScreen();
				}else{
					self.hideClickScreen();
				}
				
				if(self.audioScreen_do) self.audioScreen_do.setX(-5000);
				self.audioScreen_do.setVisible(false);
				
							
				if(self.flash_do){
					self.flash_do.setWidth(1);
					self.flash_do.setHeight(1);
				}
				if(self.videoScreen_do) self.videoScreen_do.setVisible(false);
				if(self.controller_do) self.controller_do.removePlaybackRateButton();
				
				//if(!self.isMobile_bl) self.vimeo_do.showDisable();
				self.vimeo_do.setSource(source);
				if(self.controller_do){
					self.controller_do.hideQualityButtons(false);
					self.controller_do.removeYtbQualityButton();
				}
				
				if(self.isMobile_bl){
					self.videoPoster_do.hide();
					if(self.largePlayButton_do) self.largePlayButton_do.hide();
				}else{
					if(self.data.autoPlay_bl || self.isThumbClick_bl){
						setTimeout(self.play, 500);
					}else{
						self.setPosterSource(self.posterPath_str);
						if(self.videoPoster_do) self.videoPoster_do.show();
						if(self.largePlayButton_do) self.largePlayButton_do.show();
					}
				}
				
				if(self.getVideoSource()) self.dispatchEvent(FWDUVPlayer.UPDATE_VIDEO_SOURCE);
				this.resizeHandler();
				return;
			}
			
			//Youtube
			if(self.videoType_str == FWDUVPlayer.YOUTUBE){
				
				if(self.vimeo_do) self.vimeo_do.setX(-5000);
				self.videoScreen_do.setX(-5000);
				self.videoScreen_do.setVisible(false);
				
				if(self.audioScreen_do) self.audioScreen_do.setX(-5000);
				self.audioScreen_do.setVisible(false);
				
				self.setPosterSource(self.posterPath_str);
			
				if(self.ytb_do) self.ytb_do.setX(0);
				
				if(self.flash_do){
					self.flash_do.setWidth(1);
					self.flash_do.setHeight(1);
				}
				
				self.isTempYoutubeAdd_bl = false;
				self.ytb_do.setSource(source);
				if(self.isMobile_bl){
					setTimeout(function(){
						self.videoPoster_do.hide();
						self.largePlayButton_do.hide();
					}, 100);
				}else{
					if(self.data.autoPlay_bl || self.isThumbClick_bl){
						if(self.data.autoPlay_bl && !self.isMobile_bl) self.play();
					}else{
						if(self.videoPoster_do) self.videoPoster_do.show();
						if(self.largePlayButton_do) self.largePlayButton_do.show();
					}
				}
				
				if(self.controller_do){
					self.controller_do.addYtbQualityButton();
					//if(self.data.showPlaybackRateButton_bl) self.controller_do.addPlaybackRateButton(self.data.defaultPlaybackRate_str);
					if(self.controller_do){
						if((self.isMobile_bl && FWDUVPUtils.isAndroid)
							|| (self.isMobile_bl && self.videoType_str == FWDUVPlayer.YOUTUBE)
							|| self.videoType_str == FWDUVPlayer.VIMEO
							|| self.videoType_str == FWDUVPlayer.HLS_JS
							|| self.videoType_str == FWDUVPlayer.IMAGE
						){
							self.controller_do.removePlaybackRateButton();
						}else{
							self.controller_do.addPlaybackRateButton(self.startAtPlaybackIndex);
						}
					}
				}
				if(self.isAdd_bl){
					self.setPlaybackRate(1);
				}else{
					self.setPlaybackRate(self.data.defaultPlaybackRate_ar[self.data.startAtPlaybackIndex]);
				}
				if(self.controller_do && self.data.showPlaybackRateButton_bl){
					self.controller_do.updatePlaybackRateButtons(self.startAtPlaybackIndex);
				}
				
				self.resizeHandler(false, true);
				if(self.getVideoSource()) self.dispatchEvent(FWDUVPlayer.UPDATE_VIDEO_SOURCE);
				return;
			}
			
			if(source.indexOf("google.com") == -1){
				var path_ar = source.split(",");
				
				if(self.isMobile_bl && path_ar[1] != undefined){
					source = path_ar[1];
				}else{
					source = path_ar[0];
				}
			}
			
		
			self.finalVideoPath_str = source;
			
			if(self.videoType_str == FWDUVPlayer.MP3){
				
				if(self.vimeo_do) self.vimeo_do.setX(-5000);
				if(self.ytb_do) self.ytb_do.setX(-5000);
				
				if(self.audioScreen_do) self.audioScreen_do.setX(-5000);
				self.audioScreen_do.setVisible(false);
				self.videoScreen_do.setVisible(true);
				
				
				if(self.controller_do && self.data.playlist_ar[self.id].videoSource.length > 1){
					self.controller_do.updatePreloaderBar(0);
					if(self.controller_do){
						self.controller_do.addYtbQualityButton();
						//if(self.data.showPlaybackRateButton_bl) self.controller_do.addPlaybackRateButton(self.data.defaultPlaybackRate_str);
					}
					self.controller_do.updateQuality(self.data.playlist_ar[self.id].videoLabels_ar, self.data.playlist_ar[self.id].videoLabels_ar[self.data.playlist_ar[self.id].videoLabels_ar.length - 1 - self.data.playlist_ar[self.id].startAtVideo]);
				}else if(self.controller_do){
					self.controller_do.removeYtbQualityButton();
				}
				
				if(self.controller_do){
					if((self.isMobile_bl && FWDUVPUtils.isAndroid)
						|| (self.isMobile_bl && self.videoType_str == FWDUVPlayer.YOUTUBE)
						|| self.videoType_str == FWDUVPlayer.VIMEO
						|| self.videoType_str == FWDUVPlayer.HLS_JS
						|| self.videoType_str == FWDUVPlayer.IMAGE
						|| self.videoType_str == FWDUVPlayer.MP3
					){
						self.controller_do.removePlaybackRateButton();
					}else{
						self.controller_do.addPlaybackRateButton(self.startAtPlaybackIndex);
					}
				}
				
				
				self.audioScreen_do.setX(0);
				self.audioScreen_do.setVisible(true);
				self.audioScreen_do.setSource(source);
			
				if(self.data.autoPlay_bl || self.isThumbClick_bl || (!self.isMobile_bl && self.isAdd_bl && !self.loadAddFirstTime_bl)){
					self.play();
					self.videoPoster_do.hide();
					self.largePlayButton_do.hide();
				}else{
					self.setPosterSource(self.posterPath_str);
					if(self.videoPoster_do) self.videoPoster_do.show();
					if(self.largePlayButton_do) self.largePlayButton_do.show();
				}
			}
			
			
			if(FWDUVPlayer.hasHTML5Video && self.videoType_str == FWDUVPlayer.VIDEO || self.videoType_str == FWDUVPlayer.HLS_JS){
				
				if(self.vimeo_do) self.vimeo_do.setX(-5000);
				if(self.ytb_do) self.ytb_do.setX(-5000);
				
				if(self.audioScreen_do) self.audioScreen_do.setX(-5000);
				self.audioScreen_do.setVisible(false);
				
				self.videoScreen_do.setVisible(true);
				
				if(self.controller_do && self.data.playlist_ar[self.id].videoSource.length > 1){
					self.controller_do.updatePreloaderBar(0);
					if(self.controller_do){
						self.controller_do.addYtbQualityButton();
						//if(self.data.showPlaybackRateButton_bl) self.controller_do.addPlaybackRateButton(self.data.defaultPlaybackRate_str);
					}
					self.controller_do.updateQuality(self.data.playlist_ar[self.id].videoLabels_ar, self.data.playlist_ar[self.id].videoLabels_ar[self.data.playlist_ar[self.id].videoLabels_ar.length - 1 - self.data.playlist_ar[self.id].startAtVideo]);
				}else if(self.controller_do){
					self.controller_do.removeYtbQualityButton();
				}
				
				if(self.controller_do){
					if((self.isMobile_bl && FWDUVPUtils.isAndroid)
						|| (self.isMobile_bl && self.videoType_str == FWDUVPlayer.YOUTUBE)
						|| self.videoType_str == FWDUVPlayer.VIMEO
						|| self.videoType_str == FWDUVPlayer.HLS_JS
						|| self.videoType_str == FWDUVPlayer.IMAGE
					){
						self.controller_do.removePlaybackRateButton();
					}else{
						self.controller_do.addPlaybackRateButton(self.startAtPlaybackIndex);
					}
				}
				
				if(self.flash_do){
					self.flash_do.setWidth(1);
					self.flash_do.setHeight(1);
				}
				
				if(self.videoType_str == FWDUVPlayer.HLS_JS){
					
					self.videoScreen_do.setSource(source);
					self.videoScreen_do.initVideo();
					self.setupHLS();
					self.hlsJS.loadSource(self.videoSourcePath_str);
					self.hlsJS.attachMedia(self.videoScreen_do.video_el);
					
					self.hlsJS.on(Hls.Events.MANIFEST_PARSED,function(e){
						if(self.videoType_str == FWDUVPlayer.HLS_JS){
							self.isHLSManifestReady_bl = true;
							if(self.data.autoPlay_bl || self.isThumbClick_bl || (!self.isMobile_bl && self.isAdd_bl && !self.loadAddFirstTime_bl)){
								self.play();
							}
							
							if(self.isAdd_bl){
								self.setPlaybackRate(1);
							}else{
								self.setPlaybackRate(self.data.defaultPlaybackRate_ar[self.startAtPlaybackIndex]);
							}
							if(self.controller_do && self.data.showPlaybackRateButton_bl){
								self.controller_do.updatePlaybackRateButtons(self.startAtPlaybackIndex);
							}
						}
					});
				}else{
					self.videoScreen_do.setSource(source);
				
					if(self.data.autoPlay_bl || self.isThumbClick_bl || (!self.isMobile_bl && self.isAdd_bl && !self.loadAddFirstTime_bl)){
						self.play();
					}else{
						self.setPosterSource(self.posterPath_str);
						self.videoPoster_do.show();
						self.largePlayButton_do.show();
					}
					
					if(self.isAdd_bl){
						self.setPlaybackRate(1);
					}else{
						self.setPlaybackRate(self.data.defaultPlaybackRate_ar[self.startAtPlaybackIndex]);
					}
					if(self.controller_do && self.data.showPlaybackRateButton_bl){
						self.controller_do.updatePlaybackRateButtons(self.startAtPlaybackIndex);
					}
					
				}
				
			}
			this.resizeHandler();
		};
		
		
		this.destroyHLS = function(){
			if(self.hlsJS){
				self.hlsJS.destroy();
				self.hlsJS = null;
			}
		}
		
		this.setupHLS = function(){
			if(self.hlsJS) return;
			self.isHLSJsLoaded_bl = true;
			self.hlsJS = new Hls();
			
			 self.hlsJS.on(Hls.Events.ERROR, function(event,data) {
				
				self.HLSError_str;
				switch(data.details) {
					case Hls.ErrorDetails.MANIFEST_LOAD_ERROR:
						try {
						 self.HLSError_str ="cannot load <a href=\"" + data.context.url + "\">" + url + "</a><br>HTTP response code:" + data.response.code + " <br>" + data.response.text;
							if(data.response.code === 0) {
							 self.HLSError_str += "this might be a CORS issue, consider installing <a href=\"https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi\">Allow-Control-Allow-Origin</a> Chrome Extension";
							}
						} catch(err) {
						  self.HLSError_str = "cannot load " + self.videoSourcePath_str;
						}
						break;
					case Hls.ErrorDetails.MANIFEST_LOAD_TIMEOUT:
						self.HLSError_str = "timeout while loading manifest";
						break;
						case Hls.ErrorDetails.MANIFEST_PARSING_ERROR:
						self.HLSError_str = "error while parsing manifest:" + data.reason;
					break;
						case Hls.ErrorDetails.LEVEL_LOAD_ERROR:
						self.HLSError_str = "error while loading level playlist";
					break;
						case Hls.ErrorDetails.LEVEL_LOAD_TIMEOUT:
						self.HLSError_str = "timeout while loading level playlist";
					break;
						case Hls.ErrorDetails.LEVEL_SWITCH_ERROR:
						self.HLSError_str = "error while trying to switch to level " + data.level;
					break;
						case Hls.ErrorDetails.FRAG_LOAD_ERROR:
						self.HLSError_str = "error while loading fragment " + data.frag.url;
					break;
						case Hls.ErrorDetails.FRAG_LOAD_TIMEOUT:
						self.HLSError_str = "timeout while loading fragment " + data.frag.url;
					break;
						case Hls.ErrorDetails.FRAG_LOOP_LOADING_ERROR:
						self.HLSError_str = "Frag Loop Loading Error";
					break;
						case Hls.ErrorDetails.FRAG_DECRYPT_ERROR:
						self.HLSError_str = "Decrypting Error:" + data.reason;
					break;
						case Hls.ErrorDetails.FRAG_PARSING_ERROR:
						self.HLSError_str = "Parsing Error:" + data.reason;
					break;
						case Hls.ErrorDetails.KEY_LOAD_ERROR:
						self.HLSError_str ="error while loading key " + data.frag.decryptdata.uri;
					break;
						case Hls.ErrorDetails.KEY_LOAD_TIMEOUT:
						self.HLSError_str = "timeout while loading key " + data.frag.decryptdata.uri;
					break;
						case Hls.ErrorDetails.BUFFER_APPEND_ERROR:
						self.HLSError_str = "Buffer Append Error";
					break;
						case Hls.ErrorDetails.BUFFER_ADD_CODEC_ERROR:
						self.HLSError_str = "Buffer Add Codec Error for " + data.mimeType + ":" + data.err.message;
					break;
						case Hls.ErrorDetails.BUFFER_APPENDING_ERROR:
						self.HLSError_str = "Buffer Appending Error";
					break;
						default:
					break;
				}
				
				
				if(self.HLSError_str){
					if(console) console.log(self.HLSError_str);
					//self.info_do.allowToRemove_bl = false;
					self.main_do.addChild(self.info_do);
					self.info_do.showText(self.HLSError_str);
					self.resizeHandler();
				}
			});
		}
		
		var recoverDecodingErrorDate,recoverSwapAudioCodecDate;
		function handleMediaError() {
			  if(autoRecoverError) {
				var now = performance.now();
				if(!recoverDecodingErrorDate || (now - recoverDecodingErrorDate) > 3000) {
				  recoverDecodingErrorDate = performance.now();
				  self.HLSError_str = "try to recover media Error ..."
				  self.hlsJS.recoverMediaError();
				} else {
				  if(!recoverSwapAudioCodecDate || (now - recoverSwapAudioCodecDate) > 3000) {
					recoverSwapAudioCodecDate = performance.now();
					self.HLSError_str = "try to swap Audio Codec and recover media Error ...";
					self.hlsJS.swapAudioCodec();
					self.hlsJS.recoverMediaError();
				  } else {
					self.HLSError_str = "cannot recover, last media error recovery failed ...";
				  }
				}
			  }
			  
			  if(self.HLSError_str){
				if(console) console.log(self.HLSError_str);
				//self.info_do.allowToRemove_bl = false;
				self.main_do.addChild(self.info_do);
				self.info_do.showText(self.HLSError_str);
				self.resizeHandler();
			}
		}
		
	
		//#############################################//
		/* go fullscreen / normal screen */
		//#############################################//
		this.goFullScreen = function(){
			if(!self.isAPIReady_bl) return;
			self.wasMin = self.isMin;
			self.isFullScreen_bl = true;
			self.removeMinOnScroll();
			
			if(document.addEventListener){
				document.addEventListener("fullscreenchange", self.onFullScreenChange);
				document.addEventListener("mozfullscreenchange", self.onFullScreenChange);
				document.addEventListener("webkitfullscreenchange", self.onFullScreenChange);
				document.addEventListener("MSFullscreenChange", self.onFullScreenChange);
			}
			
			if(FWDUVPUtils.isSafari && FWDUVPUtils.isWin){
				
			}else{
				if(document.documentElement.requestFullScreen) {
					self.main_do.screen.requestFullScreen();
				}else if(document.documentElement.mozRequestFullScreen){ 
					self.main_do.screen.mozRequestFullScreen();
				}else if(document.documentElement.webkitRequestFullScreen){
					self.main_do.screen.webkitRequestFullScreen();
				}else if(document.documentElement.msRequestFullscreen){
					self.main_do.screen.msRequestFullscreen();
				}
			}
			
			self.callVastEvent("playerExpand");
			self.stopVisualization();
			self.disableClick();
			
			if(!self.isEmbedded_bl){
				self.main_do.getStyle().position = "fixed";
				document.documentElement.style.overflow = "hidden";
				self.main_do.getStyle().zIndex = 9999999999998;
			}
			
			if(self.controller_do){
				self.controller_do.showNormalScreenButton();
				self.controller_do.setNormalStateToFullScreenButton();
			}
			if(self.customContextMenu_do) self.customContextMenu_do.updateFullScreenButton(1);
			var scrollOffsets = FWDUVPUtils.getScrollOffsets();
			self.lastX = scrollOffsets.x;
			self.lastY = scrollOffsets.y;
			window.scrollTo(0,0);

		
			if(self.isMobile_bl) window.addEventListener("touchmove", self.disableFullScreenOnMobileHandler);
			self.dispatchEvent(FWDUVPlayer.GO_FULLSCREEN);
			self.resizeHandler();
		};
		
		this.disableFullScreenOnMobileHandler = function(e){
			if(e.preventDefault) e.preventDefault();
		};
		
		this.goNormalScreen = function(){		
			if(!self.isAPIReady_bl) return;
			
			if (document.cancelFullScreen) {  
				document.cancelFullScreen();  
			}else if (document.mozCancelFullScreen) {  
				document.mozCancelFullScreen();  
			}else if (document.webkitCancelFullScreen) {  
				document.webkitCancelFullScreen();  
			}else if (document.msExitFullscreen) {  
				document.msExitFullscreen();  
			}
			
			self.disableClick();
			self.addMainDoToTheOriginalParent();
			self.isFullScreen_bl = false;
		};
		
		this.addMainDoToTheOriginalParent = function(){
			if(!self.isFullScreen_bl) return;
			
			if(document.removeEventListener){
				document.removeEventListener("fullscreenchange", self.onFullScreenChange);
				document.removeEventListener("mozfullscreenchange", self.onFullScreenChange);
				document.removeEventListener("webkitfullscreenchange", self.onFullScreenChange);
				document.removeEventListener("MSFullscreenChange", self.onFullScreenChange);
			}
			
			self.callVastEvent("playerCollapse");
				
			if(self.controller_do){
				self.controller_do.setNormalStateToFullScreenButton();
			}
		
			if(!self.isEmbedded_bl){
				if(self.displayType == FWDUVPlayer.RESPONSIVE
				   || self.displayType == FWDUVPlayer.AFTER_PARENT
				   || self.displayType == FWDUVPlayer.LIGHTBOX
				    || self.displayType == FWDUVPlayer.STICKY
				 ){
					if(FWDUVPUtils.isIEAndLessThen9){
						document.documentElement.style.overflow = "auto";
					}else{
						document.documentElement.style.overflow = "visible";
					}
					
					if(self.isMin){
						self.main_do.getStyle().position = 'fixed';
						self.main_do.getStyle().zIndex = 9999999999999;
					}else{
						self.main_do.getStyle().position = "relative";
						self.main_do.getStyle().zIndex = 0;
					}
				
				}else{
					self.main_do.getStyle().position = "absolute";
					self.main_do.getStyle().zIndex = 9999999999998;
				}
			}
			
			if(self.displayType != FWDUVPlayer.FULL_SCREEN) self.controller_do.enablePlaylistButton();
			
			if(self.customContextMenu_do) self.customContextMenu_do.updateFullScreenButton(0);
			self.controller_do.showFullScreenButton();
			window.scrollTo(self.lastX, self.lastY);

			self.showCursor();
			self.resizeHandler();

			setTimeout(function(){
				self.addMinOnScroll();
				self.resizeHandler();
			}, 500);
			
			window.scrollTo(self.lastX, self.lastY);
			if(!FWDUVPUtils.isIE){
				setTimeout(function(){
					window.scrollTo(self.lastX, self.lastY);
				}, 150);
			}
			
			if(self.isMobile_bl) window.removeEventListener("touchmove", self.disableFullScreenOnMobileHandler);
			self.dispatchEvent(FWDUVPlayer.GO_NORMALSCREEN);
		};
		
		this.onFullScreenChange = function(e){
			if(!(document.fullScreen || document.msFullscreenElement  || document.mozFullScreen || document.webkitIsFullScreen || document.msieFullScreen)){
				self.controller_do.showNormalScreenButton();
				self.addMainDoToTheOriginalParent();
				self.isFullScreen_bl = false;
			}
		};
		
		this.loadPlaylist = function(id){
			if(!self.isAPIReady_bl || !self.isPlaylistLoaded_bl) return;
			if(self.data.prevId == id) return;
			
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			self.catId = id;
			self.id = 0;
			
			if(self.catId < 0){
				self.catId = 0;
			}else if(self.catId > self.data.totalPlaylists - 1){
				self.catId = self.data.totalPlaylists - 1;
			};
			
			if(self.useDeepLinking_bl){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
			}else{
				self.loadInternalPlaylist();
			}
		};
		
		this.playNext = function(){	
			if(!self.isAPIReady_bl || !self.isPlaylistLoaded_bl) return;
			
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			self.id ++;
			if(self.id < 0){
				self.id = self.totalVideos - 1;
			}else if(self.id > self.totalVideos - 1){
				self.id = 0;
			}
		
			if(self.useDeepLinking_bl){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
			}else{
			
				self.isThumbClick_bl = true;
				self.updateAds(0, true);
			}
		};
		
		this.playPrev = function(){
			if(!self.isAPIReady_bl || !self.isPlaylistLoaded_bl) return;
			
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			self.id --;	
			if(self.id < 0){
				self.id = self.totalVideos - 1;
			}else if(self.id > self.totalVideos - 1){
				self.id = 0;
			}
			
			if(self.useDeepLinking_bl){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
			}else{
				self.isThumbClick_bl = true;
				self.updateAds(0, true);
			}
		};
		
		this.playShuffle = function(){
			if(!self.isAPIReady_bl || !self.isPlaylistLoaded_bl) return;
			
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			var tempId = parseInt(Math.random() * self.totalVideos);
			while(tempId == self.id) tempId = parseInt(Math.random() * self.totalVideos);
			
			self.id = tempId;	
			if(self.id < 0){
				self.id = self.totalVideos - 1;
			}else if(self.id > self.totalVideos - 1){
				self.id = 0;
			}
			
			if(self.useDeepLinking_bl){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
			}else{
				self.isThumbClick_bl = true;
				self.updateAds(0, true);
			}
		};
		
		this.playVideo = function(videoId){	
			if(!self.isAPIReady_bl || !self.isPlaylistLoaded_bl) return;
			
			if(self.data.playlist_ar){
				self.videoNameGa = self.data.playlist_ar[self.id]["gaStr"]
				self.videoCat = self.data.cats_ar[self.catId]["playlistName"];
			}
			
			self.id = videoId;
			
			if(self.id < 0){
				self.id = self.totalVideos - 1;
			}else if(self.id > self.totalVideos - 1){
				self.id = 0;
			}
			
			if(self.useDeepLinking_bl){
				FWDAddress.setValue("?playlistId=" + self.catId + "&videoId=" + self.id);
			}else{
				self.updateAds(0, true);
				if(self.isMobile_bl && self.videoType_str == FWDUVPlayer.VIDEO) self.play();
				if(!self.isMobile_bl){
					self.play();
				}
			}
		};
		
		this.setVideoSource =  function(source, is360, isLive){
			self.isAdd_bl = false;
			var s360_str = "no";
			if(is360) s360_str = "yes";
			self.isLive = isLive;
			self.setSource(source, false, is360);
		};
	
		
		this.downloadVideo = function(pId){
			
			if(pId ==  undefined) pId = self.id;
			
			var sourceName;
			
			var source = self.data.playlist_ar[pId].videoSource[self.data.playlist_ar[self.id].startAtVideo]["source"];
			if(String(source.indexOf("encrypt:")) != -1){
				source = atob(source.substr(8));
			}
			if(source.indexOf("/") != -1){
				sourceName = source.substr(source.lastIndexOf("/") + 1);
			}else{
				sourceName = source;
			}
			
			var gaLabel = 'videoName:' + self.data.playlist_ar[self.id]["gaStr"];
			if(window["ga"]){
				ga('send', {
				  hitType: 'event',
				  eventCategory: self.data.cats_ar[self.catId]["playlistName"],
				  eventAction: 'downloaded',
				  eventLabel: gaLabel,
				  nonInteraction: true
				});
			}
		
		
			self.data.downloadVideo(source, sourceName);
		};
		
		this.share = function(){
			if(!self.isAPIReady_bl) return;
			self.controllerShareHandler();
		};	
		
		this.getVideoSource = function(){
			if(!self.isAPIReady_bl) return;
			return self.finalVideoPath_str;
		};
		
		this.getPosterSource = function(){
			if(!self.isAPIReady_bl) return;
			return self.posterPath_str;
		};
		
		this.getPlaylistId = function(){
			return self.catId;
		};
		
		this.getVideoId = function(){
			return self.id;
		};
		
		this.getCurrentTime = function(){
			var tm;
			if(!self.curTime){
				tm = "00:00";
			}else{
				tm = self.curTime;
			}
			return tm;
		};
		
		this.setPlaybackRate = function(rate){
			if(!self.isAPIReady_bl) return;
			if(self.videoType_str == FWDUVPlayer.VIDEO && self.videoScreen_do){
				self.videoScreen_do.setPlaybackRate(rate);
			}else if(self.videoType_str == FWDUVPlayer.YOUTUBE){
				self.ytb_do.setPlaybackRate(rate);
			}
		}
		
		this.getTotalTime = function(){
			var tm;
			if(!self.totalTime){
				tm = "00:00";
			}else{
				tm = self.totalTime;
			}
			return tm;
		};
		
		this.showLightbox = function(){
			if(self.lightBox_do) self.lightBox_do.show();
		}
		
		this.fillEntireVideoScreen = function(param){
			this.fillEntireVideoScreen_bl = param;
			this.resizeHandler();
		};
		
		this.updateHEXColors = function(normalColor, selectedColor){
			if(!self.isAPIReady_bl) return;
			self.controller_do.updateHEXColors(normalColor, selectedColor);
			if(self.largePlayButton_do) self.largePlayButton_do.updateHEXColors(normalColor, "#FFFFFF");
			if(self.shareWindow_do) self.shareWindow_do.updateHEXColors(normalColor, selectedColor);
			
			if(self.embedWindow_do){
				if(self.skinPath_str.indexOf("hex_white") != -1){
					self.embedWindow_do.updateHEXColors(normalColor, "#FFFFFF");
				}else{
					self.embedWindow_do.updateHEXColors(normalColor, selectedColor);
				}	
			}
			
			if(self.annotations_do) self.annotations_do.updateHEXColors(normalColor, selectedColor);
			if(self.adsSkip_do) self.adsSkip_do.updateHEXColors(normalColor, selectedColor);
			if(self.categories_do) self.categories_do.updateHEXColors(normalColor, selectedColor);
			if(self.playlist_do) self.playlist_do.updateHEXColors(normalColor, selectedColor);
			self.data.setYoutubePlaylistHEXColor(normalColor);	
			if(self.popupAds_do) self.popupAds_do.updateHEXColors(normalColor, "#FFFFFF");
			if(self.adsStart_do) self.adsStart_do.updateHEXColors(normalColor, selectedColor);
			
		};
		
		//###########################################//
		/* Hide / show cursor */
		//###########################################//
		this.hideCursor = function(){
			document.documentElement.style.cursor = "none";
			document.getElementsByTagName("body")[0].style.cursor = "none";
			if(!self.isAdd_bl) self.dumyClick_do.getStyle().cursor = "none";
		};
		
		this.showCursor = function(){
			document.documentElement.style.cursor = "auto";
			document.getElementsByTagName("body")[0].style.cursor = "auto";
			if(self.isAdd_bl){
				self.dumyClick_do.setButtonMode(true);
			}else{
				if(self.is360){
					self.dumyClick_do.getStyle().cursor = 'url(' + self.data.handPath_str + '), default';
				}else{
					self.dumyClick_do.getStyle().cursor = "auto";
				}
			}
		};
		
		this.showPlayer = function(){
			if(!self.isAPIReady_bl) return;
			self.isShowed_bl = true;
			self.opener_do.showCloseButton();
			self.setStageContainerFinalHeightAndPosition(self.animate_bl);
			if(self.isMin){
				self.isMinShowed = true;
				self.positionOnMin(true);
			}
		};
		
		this.hidePlayer = function(){
			if(!self.isAPIReady_bl) return;
			self.isShowed_bl = false;
			self.opener_do.showOpenButton();
			self.setStageContainerFinalHeightAndPosition(self.animate_bl);
			if(self.isMin){
				self.isMinShowed = false;
				self.positionOnMin(true);
			}
		};
		
		this.getStartTimeStamp = function(str){
			var ts  = window.location.href;
			ts = ts.substr(ts.indexOf(str + "=") + 2);
			if(ts.indexOf("&") != -1){
				ts = ts.substr(0, ts.indexOf("&"));
			}

			if(ts.indexOf("s&") != -1){
				ts = ts.substr(0, ts.indexOf("s&") + 1);
			}
			
			var pattern = /\d+h/g;
			var hours = ts.match(pattern);
			try{ hours = ts.match(pattern)[0] }catch(e){}
			if(hours){
				hours = hours.substr(0, hours.length -1);
				if(hours.length == 1 && parseInt(hours) < 10){
					hours = "0" + hours;
				}
				if(parseInt(hours) > 59) hours = 59;
			}
			hours = hours ? hours : "00";
			
			var pattern = /\d+m/g;
			var minutes = ts.match(pattern);
			try{ minutes = ts.match(pattern)[0] }catch(e){}
			if(minutes){
				minutes = minutes.substr(0, minutes.length -1);
				if(minutes.length == 1 && parseInt(minutes) < 10){
					minutes = "0" + minutes;
				}
				if(parseInt(minutes) > 59) minutes = 59;
			}
			minutes = minutes ? minutes : "00";
			
			var pattern = /\d+s/g;
			var seconds = ts.match(pattern);
			try{ seconds = ts.match(pattern)[0] }catch(e){}
			if(seconds){
				seconds = seconds.substr(0, seconds.length -1);
				if(seconds.length == 1 && parseInt(seconds) < 10){
					seconds = "0" + seconds;
				}
				if(parseInt(seconds) > 59) seconds = 59;
			}
			seconds = seconds ? seconds : "00";
		
			return hours + ":" + minutes + ":" + seconds;
		}
	
		

		//###########################################//
		/* event dispatcher */
		//###########################################//
		this.addListener = function (type, listener){
	    	if(type == undefined) throw Error("type is required.");
	    	if(typeof type === "object") throw Error("type must be of type String.");
	    	if(typeof listener != "function") throw Error("listener must be of type Function.");
	    	
	        var event = {};
	        event.type = type;
	        event.listener = listener;
	        event.target = this;
	        this.listeners.events_ar.push(event);
	    };
	    
	    this.dispatchEvent = function(type, props){
	    	if(this.listeners == null) return;
	    	if(type == undefined) throw Error("type is required.");
	    	if(typeof type === "object") throw Error("type must be of type String.");
	    	
	        for (var i=0, len=this.listeners.events_ar.length; i < len; i++){
	        	if(this.listeners.events_ar[i].target === this && this.listeners.events_ar[i].type === type){		
	    	        if(props){
	    	        	for(var prop in props){
	    	        		this.listeners.events_ar[i][prop] = props[prop];
	    	        	}
	    	        }
	        		this.listeners.events_ar[i].listener.call(this, this.listeners.events_ar[i]);
	        	}
	        }
	    };
	    
	   this.removeListener = function(type, listener){
	    	
	    	if(type == undefined) throw Error("type is required.");
	    	if(typeof type === "object") throw Error("type must be of type String.");
	    	if(typeof listener != "function") throw Error("listener must be of type Function." + type);
	    	
	        for (var i=0, len=this.listeners.events_ar.length; i < len; i++){
	        	if(this.listeners.events_ar[i].target === this 
	        			&& this.listeners.events_ar[i].type === type
	        			&& this.listeners.events_ar[i].listener ===  listener
	        	){
	        		this.listeners.events_ar.splice(i,1);
	        		break;
	        	}
	        }  
	    };
		
		//#############################################//
		/* Tracking vast events */
		//#############################################//
		this.callVastEvent = function(eventName){
			
			if(!self.TrackingEvents) return;
			var URI;
			
			for(var i=0; i<self.TrackingEvents.length; i++){
				if(eventName == self.TrackingEvents[i]["event"]){
					//console.log(eventName);
					URI = self.TrackingEvents[i]["URI"];
				}
			}
		
			if(!URI) return;
			self.executeVastEvent(URI);
		}
		
		this.executeVastEvent = function(URI){
			if(!URI) return;
			//console.log(URI);
			var trackingXHR = new XMLHttpRequest();
			trackingXHR.onreadystatechange = function(e){
				//if(trackingXHR.readyState == 4){
					//console.log(trackingXHR.status + "  " + trackingXHR.response)
				//}
			};
			
			trackingXHR.onerror = function(e){
				try{
					if(window.console) console.log(e);
					if(window.console) console.log(e.message);
				}catch(e){};
			};
			
			trackingXHR.open("get", URI, true);
			trackingXHR.send();
		}
	    
	  //#############################################//
		/* clean main events */
		//#############################################//
		self.cleanMainEvents = function(){
			if(window.removeEventListener){
				window.removeEventListener("resize", self.onResizeHandler);
			}else if(window.detachEvent){
				window.detachEvent("onresize", self.onResizeHandler);
			}
		
			clearTimeout(self.resizeHandlerId_to);
			clearTimeout(self.resizeHandler2Id_to);
			clearTimeout(self.hidePreloaderId_to);
			clearTimeout(self.orientationChangeId_to);
		};
	
		
		var args = FWDUVPUtils.getUrlArgs(window.location.search);

		var embedTest = args.RVPInstanceName;
		var instanceName = args.RVPInstanceName;
		
		if(embedTest){
			self.isEmbedded_bl = props.instanceName == instanceName;
		}
		
		if(self.isEmbedded_bl){
			var ws = FWDUVPUtils.getViewportSize();
			
			self.embeddedPlaylistId = parseInt(args.RVPPlaylistId);
			self.embeddedVideoId = parseInt(args.RVPVideoId);
			
			var dumy_do = new FWDUVPDisplayObject("div");
			dumy_do.setBkColor(props.backgroundColor);
			dumy_do.setWidth(ws.w);
			dumy_do.setHeight(ws.h);
			
			document.documentElement.style.overflow = "hidden";
			document.getElementsByTagName("body")[0].style.overflow = "hidden";
			
			if(FWDUVPUtils.isIEAndLessThen9){
				document.getElementsByTagName("body")[0].appendChild(dumy_do.screen);
			}else{
				document.documentElement.appendChild(dumy_do.screen);
			}
		}
	
		
		self.init();
	};
	
	

	
	
	/* set prototype */
	FWDUVPlayer.setPrototype =  function(){
		FWDUVPlayer.prototype = new FWDUVPEventDispatcher();
	};
		
	
	FWDUVPlayer.stopAllVideos = function(pVideo){
		var tt = FWDUVPlayer.instaces_ar.length;
		var video;
		for(var i=0; i<tt; i++){
			video = FWDUVPlayer.instaces_ar[i];
			if(video != pVideo){
				video.stop();
			}
		};
	};
	
	FWDUVPlayer.pauseAllVideos = function(pVideo){
		var tt = FWDUVPlayer.instaces_ar.length;
		var video;
		for(var i=0; i<tt; i++){
			video = FWDUVPlayer.instaces_ar[i];
			if(video != pVideo){
				video.pause();
			}
		};
	};
	
	FWDUVPlayer.hasHTML5VideoTestIsDone = false;
	if(!FWDUVPlayer.hasHTML5VideoTestIsDone){
		FWDUVPlayer.hasHTML5Video = (function(){
			var videoTest_el = document.createElement("video");
			var flag = false;
			if(videoTest_el.canPlayType){
				flag = Boolean(videoTest_el.canPlayType('video/mp4') == "probably" || videoTest_el.canPlayType('video/mp4') == "maybe");
				FWDUVPlayer.canPlayMp4 = Boolean(videoTest_el.canPlayType('video/mp4') == "probably" || videoTest_el.canPlayType('video/mp4') == "maybe");
				FWDUVPlayer.canPlayOgg = Boolean(videoTest_el.canPlayType('video/ogg') == "probably" || videoTest_el.canPlayType('video/ogg') == "maybe");
				FWDUVPlayer.canPlayWebm = Boolean(videoTest_el.canPlayType('video/webm') == "probably" || videoTest_el.canPlayType('video/webm') == "maybe");
			}
			
			if(self.isMobile_bl) return true;
			//return false;
			FWDUVPlayer.hasHTML5VideoTestIsDone = true;
			return flag;
		}());
	}
	
	FWDUVPlayer.hasCanvas = (function(){
		return Boolean(document.createElement("canvas"));
	})();
	
	FWDUVPlayer.instaces_ar = [];
	
	FWDUVPlayer.hasHTMLHLS = (function(){
		var videoTest_el = document.createElement("video");
		var flag = false;
		if(videoTest_el.canPlayType){
			flag = Boolean(videoTest_el.canPlayType('application/vnd.apple.mpegurl') === "probably" || videoTest_el.canPlayType('application/vnd.apple.mpegurl') === "maybe");
		}
		return flag;
	}());
	
	
	FWDUVPlayer.areMainInstancesInitialized_bl = false;
	FWDUVPlayer.curInstance = null;
	FWDUVPlayer.keyboardCurInstance = null;
	FWDUVPlayer.isYoutubeAPICreated_bl = false;
	
	FWDUVPlayer.HLS_JS = "HLS";
	FWDUVPlayer.PAUSE_ALL_VIDEOS = "pause";
	FWDUVPlayer.STOP_ALL_VIDEOS = "stop";
	FWDUVPlayer.DO_NOTHING = "none";
	FWDUVPlayer.YOUTUBE = "youtube";
	FWDUVPlayer.VIMEO = "vimeo";
	FWDUVPlayer.VIDEO = "video";
	FWDUVPlayer.atLeastOnePlayerHasDeeplinking_bl = false;
	FWDUVPlayer.MP3 = "mp3";
	
	FWDUVPlayer.CENTER = "center";
	FWDUVPlayer.RIGHT = "right";
	FWDUVPlayer.LEFT = "left";
	FWDUVPlayer.POSITION_BOTTOM = "bottom";
	FWDUVPlayer.POSITION_TOP = "top";
	FWDUVPlayer.HIDE_LIGHTBOX_COMPLETE = "lightboxHideComplete";
	FWDUVPlayer.START_TO_LOAD_PLAYLIST = "startToLoadPlaylist";
	FWDUVPlayer.LOAD_PLAYLIST_COMPLETE = "loadPlaylistComplete";
	FWDUVPlayer.READY = "ready";
	FWDUVPlayer.STOP = "stop";
	FWDUVPlayer.PLAY = "play";
	FWDUVPlayer.PAUSE = "pause";
	FWDUVPlayer.UPDATE = "update";
	FWDUVPlayer.UPDATE_TIME = "updateTime";
	FWDUVPlayer.UPDATE_VIDEO_SOURCE = "updateVideoSource";
	FWDUVPlayer.UPDATE_POSTER_SOURCE = "udpatePosterSource";
	FWDUVPlayer.ERROR = "error";
	FWDUVPlayer.PLAY_COMPLETE = "playComplete";
	FWDUVPlayer.VOLUME_SET = "volumeSet";
	FWDUVPlayer.GO_FULLSCREEN = "goFullScreen";
	FWDUVPlayer.GO_NORMALSCREEN = "goNormalScreen";
	FWDUVPlayer.IMAGE = "image";
	FWDUVPlayer.HLS_JS = "hls_flash"
	FWDUVPlayer.SAFE_TO_SCRUB = "safeToScrub";
	
	FWDUVPlayer.LIGHTBOX = "lightbox";
	FWDUVPlayer.STICKY = "sticky";
	FWDUVPlayer.RESPONSIVE = "responsive";
	FWDUVPlayer.FULL_SCREEN = "fullscreen";
	FWDUVPlayer.AFTER_PARENT = "afterparent";
	
	
	window.FWDUVPlayer = FWDUVPlayer;
	
}(window));