/* FWDUVPComboBoxSelector */
(function (){
var FWDUVPComboBoxSelector = function(
			arrowW,
			arrowH,
			arrowN_str,
			arrowS_str,
			label1, 
			backgroundNormalColor,
			backgroundSelectedColor,
			textNormalColor,
			textSelectedColor,
			totalHeight,
			useHEXColorsForSkin_bl,
			normalButtonsColor_str,
			selectedButtonsColor_str
		){
		
		var self = this;
		var prototype = FWDUVPComboBoxSelector.prototype;
		
		this.arrow_do = null;
		this.arrowN_sdo = null;
		this.arrowS_sdo = null;
		
		this.arrowN_str = arrowN_str;
		this.arrowS_str = arrowS_str;
		
		this.label1_str = label1;
		this.backgroundNormalColor_str = backgroundNormalColor;
		this.backgroundSelectedColor_str = backgroundSelectedColor;
		this.textNormalColor_str = textNormalColor;
		this.textSelectedColor_str = textSelectedColor;
		
		self.useHEXColorsForSkin_bl = useHEXColorsForSkin_bl;
		self.normalButtonsColor_str = normalButtonsColor_str;
		self.selectedButtonsColor_str = selectedButtonsColor_str;
		
		this.totalWidth = 400;
		
		this.totalHeight = totalHeight;
		this.arrowWidth = arrowW;
		this.arrowHeight = arrowH;
		
		this.bk_sdo = null;
		this.text_sdo = null;
		this.dumy_sdo = null;
		
		this.hasPointerEvent_bl = FWDUVPUtils.hasPointerEvent;
		this.isMobile_bl = FWDUVPUtils.isMobile;
		this.isDisabled_bl = false;
		
		
		//##########################################//
		/* initialize self */
		//##########################################//
		self.init = function(){
			self.setBackfaceVisibility();
			self.setButtonMode(true);
			self.setupMainContainers();
			self.setWidth(self.totalWidth);
			self.setHeight(self.totalHeight);
		};
	
		//##########################################//
		/* setup main containers */
		//##########################################//
		self.setupMainContainers = function(){
			
			self.bk_sdo = new FWDUVPDisplayObject("div");
			self.bk_sdo.getStyle().backgroundColor = self.backgroundNormalColor_str;
			self.addChild(self.bk_sdo);
			
			self.text_sdo = new FWDUVPDisplayObject("div");
			self.text_sdo.getStyle().whiteSpace = "nowrap";
			self.text_sdo.setBackfaceVisibility();
			self.text_sdo.setOverflow("visible");
			self.text_sdo.setDisplay("inline-block");
			self.text_sdo.getStyle().fontFamily = "Arial";
			self.text_sdo.getStyle().fontSize= "13px";
			self.text_sdo.getStyle().fontWeight = "100";
			self.text_sdo.getStyle().padding = "6px";
			self.text_sdo.getStyle().color = self.normalColor_str;
			self.text_sdo.getStyle().fontSmoothing = "antialiased";
			self.text_sdo.getStyle().webkitFontSmoothing = "antialiased";
			self.text_sdo.getStyle().textRendering = "optimizeLegibility";
			
			if (FWDUVPUtils.isIEAndLessThen9)
			{
				self.text_sdo.screen.innerText = self.label1_str;
			}
			else
			{
				self.text_sdo.setInnerHTML(self.label1_str);
			}
			
			self.addChild(self.text_sdo);
			
			self.arrow_do = new FWDUVPDisplayObject("div");
			self.arrow_do.screen.className = "arrow";
			self.arrow_do.setOverflow("visible");
			
			if(self.useHEXColorsForSkin_bl){
				self.arrowN_img = new Image();
				self.arrowN_img.src = self.arrowN_str;
				self.arrowS_img = new Image();
				self.arrowS_img.src = self.arrowS_str;
				self.arrowN_sdo = new FWDUVPDisplayObject("div");
				self.arrowS_sdo = new FWDUVPDisplayObject("div");
				
				self.arrowS_img.onload = function(){
					self.arrowN_sdo.setWidth(self.arrowN_img.width);
					self.arrowN_sdo.setHeight(self.arrowN_img.height);
					self.scrubberLines_n_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.arrowN_img, self.normalButtonsColor_str, true);
					self.scrubbelinesNImage_img = self.scrubberLines_n_canvas.image;
					self.arrowN_sdo.getStyle().background = "url('" + self.scrubbelinesNImage_img.src + "') repeat-y";
					
					self.arrowS_sdo.setWidth(self.arrowS_img.width);
					self.arrowS_sdo.setHeight(self.arrowS_img.height);
					self.scrubberLines_s_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.arrowS_img, self.selectedButtonsColor_str, true);
					self.scrubbelinesSImage_img = self.scrubberLines_s_canvas.image;
					self.arrowS_sdo.getStyle().background = "url('" + self.scrubbelinesSImage_img.src + "') repeat-y";
				}
			}else{
				self.arrowN_sdo = new FWDUVPDisplayObject("div");
				self.arrowN_sdo.screen.style.backgroundImage = "url(" + self.arrowN_str + ")";
				self.arrowS_sdo = new FWDUVPDisplayObject("div");
				self.arrowS_sdo.screen.style.backgroundImage = "url(" + self.arrowS_str + ")";
			}
			
			self.arrowS_sdo.setAlpha(0);
			self.arrow_do.addChild(self.arrowN_sdo);
			self.arrow_do.addChild(self.arrowS_sdo);
			self.addChild(self.arrow_do);
			
			self.arrowN_sdo.setWidth(self.arrowWidth);
			self.arrowN_sdo.setHeight(self.arrowHeight);
			self.arrowS_sdo.setWidth(self.arrowWidth);
			self.arrowS_sdo.setHeight(self.arrowHeight);
			
			self.dumy_sdo = new FWDUVPDisplayObject("div");
			if(FWDUVPUtils.isIE){
				self.dumy_sdo.setBkColor("#FF0000");
				self.dumy_sdo.setAlpha(0);
			};
			self.addChild(self.dumy_sdo);
			
			if(self.hasPointerEvent_bl){
				self.screen.addEventListener("pointerup", self.onClick);
				self.screen.addEventListener("pointerover", self.onMouseOver);
				self.screen.addEventListener("pointerout", self.onMouseOut);
			}else if(self.screen.addEventListener){	
				self.screen.addEventListener("mouseover", self.onMouseOver);
				self.screen.addEventListener("mouseout", self.onMouseOut);
				self.screen.addEventListener("mouseup", self.onClick);
				self.screen.addEventListener("touchend", self.onClick);
			}
			
			
		};
		
		self.onMouseOver = function(e){
			if(self.isDisabled_bl) return;
			if(!e.pointerType || e.pointerType == e.MSPOINTER_TYPE_MOUSE){
				FWDAnimation.killTweensOf(self.text_sdo);
				self.setSelectedState(true, 0);
				self.dispatchEvent(FWDUVPComboBoxSelector.MOUSE_OVER);
			}
		};
			
		self.onMouseOut = function(e){
			if(self.isDisabled_bl) return;
			if(!e.pointerType || e.pointerType == e.MSPOINTER_TYPE_MOUSE){
				FWDAnimation.killTweensOf(self.text_sdo);
				self.setNormalState(true, true);
				self.dispatchEvent(FWDUVPComboBoxSelector.MOUSE_OUT);
			}
		};
		
		self.onClick = function(e){
			
			if(self.isDeveleper_bl){
				window.open("http://www.webdesign-flash.ro", "_blank");
				return;
			}
			
			//if(self.isDisabled_bl) return;
			if(e.preventDefault) e.preventDefault();
			self.dispatchEvent(FWDUVPComboBoxSelector.CLICK, {e:e});
		};
		
		self.onMouseDown = function(e){
			//if(self.isDisabled_bl) return;
			if(e.preventDefault) e.preventDefault();
			self.dispatchEvent(FWDUVPComboBoxSelector.MOUSE_DOWN, {e:e});
		};
		
		//###########################################//
		/* set selected / normal state */
		//###########################################//
		this.setSelectedState = function(animate, dl){
			
			if(animate){
				
				FWDAnimation.to(self.bk_sdo, .6, {alpha:1, ease:Expo.easeOut});	
				FWDAnimation.to(self.text_sdo.screen, .6, {css:{color:self.textSelectedColor_str}, ease:Expo.easeOut});
				FWDAnimation.to(self.arrowS_sdo, .6, {alpha:1, ease:Expo.easeOut});
			}else{
				self.bk_sdo.setAlpha(1);
				self.text_sdo.getStyle().color = self.textSelectedColor_str;
				self.arrowS_sdo.alpha = 1;
			}
		};
		
		this.setNormalState = function(animate, removeDelay){
			//if(FWDAnimation.isTweening(self.arrowS_sdo)) return;
			var dll = .6;
			if(removeDelay) dll = 0;
			dll = 0;
			if(animate){
				FWDAnimation.to(self.bk_sdo, .6, {alpha:0, delay:dll, ease:Expo.easeOut});	
				FWDAnimation.to(self.text_sdo.screen, .6, {css:{color:self.textNormalColor_str}, delay:dll, ease:Expo.easeOut});
				FWDAnimation.to(self.arrowS_sdo, .6, {alpha:0, delay:dll, ease:Expo.easeOut});
			}else{
				self.bk_sdo.setAlpha(0);
				self.text_sdo.getStyle().color = self.textNormalColor_str;
				self.arrowS_sdo.alpha = 0;
			}
		};
		
	

		//##########################################//
		/* center text */
		//##########################################//
		self.centerText = function(){
			self.dumy_sdo.setWidth(self.totalWidth);
			self.dumy_sdo.setHeight(self.totalHeight);
			self.bk_sdo.setWidth(self.totalWidth);
			self.bk_sdo.setHeight(self.totalHeight);
			
			self.text_sdo.setX(2);
			
			self.text_sdo.setY(Math.round((self.totalHeight - self.text_sdo.getHeight())/2));
			
			self.arrow_do.setX(self.totalWidth - self.arrowWidth - 8);
			self.arrow_do.setY(Math.round((self.totalHeight - self.arrowHeight)/2));
			
			
		};
		
		//###############################//
		/* get max text width */
		//###############################//
		self.getMaxTextWidth = function(){
			return self.text_sdo.getWidth();
		};
		
		//##############################//
		/* disable / enable */
		//#############################//
		this.disable = function(){
			self.isDisabled_bl = true;
			self.setSelectedState(true);
			if(FWDUVPUtils.hasTransform2d){
				FWDAnimation.to(self.arrowN_sdo.screen, .8, {css:{rotation:180}, ease:Quart.easeOut});
				FWDAnimation.to(self.arrowS_sdo.screen, .8, {css:{rotation:180}, ease:Quart.easeOut});
			}
			self.setButtonMode(false);
		};
		
		this.enable = function(){
			
			self.isDisabled_bl = false;
			self.setNormalState(true);
			if(FWDUVPUtils.hasTransform2d){
				FWDAnimation.to(self.arrowN_sdo.screen, .8, {css:{rotation:0}, ease:Quart.easeOut});
				FWDAnimation.to(self.arrowS_sdo.screen, .8, {css:{rotation:0}, ease:Quart.easeOut});
			}
			self.setButtonMode(true);
		};
		
		this.setText = function(text)
		{
			if (FWDUVPUtils.isIEAndLessThen9)
			{
				self.text_sdo.screen.innerText = text;
			}
			else
			{
				self.text_sdo.setInnerHTML(text);
			}
		};
		
		//##############################//
		/* destroy */
		//##############################//
		self.destroy = function(){
			
			if(self.isMobile_bl){
				self.screen.removeEventListener("touchstart", self.onMouseDown);
			}else if(self.screen.removeEventListener){
				self.screen.removeEventListener("mouseover", self.onMouseOver);
				self.screen.removeEventListener("mouseout", self.onMouseOut);
				self.screen.removeEventListener("mousedown", self.onMouseDown);
				self.screen.removeEventListener("click", self.onClick);
			}else if(self.screen.detachEvent){
				self.screen.detachEvent("onmouseover", self.onMouseOver);
				self.screen.detachEvent("onmouseout", self.onMouseOut);
				self.screen.detachEvent("onmousedown", self.onMouseDown);
				self.screen.detachEvent("onclick", self.onClick);
			}
			
			
			FWDAnimation.killTweensOf(self.text_sdo);
			FWDAnimation.killTweensOf(self.colorObj);
			self.text_sdo.destroy();
			
			self.dumy_sdo.destroy();
			
			self.text_sdo = null;
			self.dumy_sdo = null;
			
			self.label1_str = null;
			self.normalColor_str = null;
			self.textSelectedColor_str = null;
			self.disabledColor_str = null;
			
			label1 = null;
			normalColor = null;
			selectedColor = null;
			disabledColor = null;
			
			self.setInnerHTML("");
			prototype.destroy();
			self = null;
			prototype = null;
			FWDUVPComboBoxSelector.prototype = null;
		};
	
		self.init();
	};
	
	/* set prototype */
	FWDUVPComboBoxSelector.setPrototype = function(){
		FWDUVPComboBoxSelector.prototype = new FWDUVPDisplayObject ("div");
	};
	
	FWDUVPComboBoxSelector.FIRST_BUTTON_CLICK = "onFirstClick";
	FWDUVPComboBoxSelector.SECOND_BUTTON_CLICK = "secondButtonOnClick";
	FWDUVPComboBoxSelector.MOUSE_OVER = "onMouseOver";
	FWDUVPComboBoxSelector.MOUSE_OUT = "onMouseOut";
	FWDUVPComboBoxSelector.MOUSE_DOWN = "onMouseDown";
	FWDUVPComboBoxSelector.CLICK = "onClick";
	
	FWDUVPComboBoxSelector.prototype = null;
	window.FWDUVPComboBoxSelector = FWDUVPComboBoxSelector;
}(window));