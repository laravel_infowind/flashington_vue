/* Data */
(function(window){
	
	var FWDUVPData = function(props, playListElement, parent){
		
		var self = this;
		var prototype = FWDUVPData.prototype;
	

	
		this.props_obj = props;
		this.skinPaths_ar = [];
		this.images_ar = [];
		this.cats_ar = [];
		this.catsRef_ar = [];
		
	
		this.controllerHeight = 0;
		this.countLoadedSkinImages = 0;
		this.volume = 1;
		this.controllerHideDelay = 0;
		this.startSpaceBetweenButtons = 0;
		this.spaceBetweenButtons = 0;
		this.scrubbersOffsetWidth = 0;
		this.volumeScrubberOffsetTopWidth = 0;
		this.timeOffsetLeftWidth = 0;
		this.timeOffsetTop = 0;
		this.logoMargins = 0;
		this.startAtPlaylist = 0;
		this.startAtVideo = 0;
		this.playlistBottomHeight = 0;
		this.maxPlaylistItems = 0;
		this.totalPlaylists = 0;
		this.thumbnailMaxWidth = 0; 
		this.buttonsMargins = 0;
		this.nextAndPrevSetButtonsMargins = 0;
		this.thumbnailMaxHeight = 0;
		this.horizontalSpaceBetweenThumbnails = 0;
		this.verticalSpaceBetweenThumbnails = 0;
		this.buttonsToolTipHideDelay = 0;
		this.thumbnailWidth = 0;
		this.thumbnailHeight = 0;
		this.timeOffsetTop = 0;
		this.embedWindowCloseButtonMargins = 0;
		
		this.loadImageId_to;
		this.dispatchLoadSkinCompleteWithDelayId_to;
		this.dispatchPlaylistLoadCompleteWidthDelayId_to;
		this.JSONPRequestTimeoutId_to;
		

		this.isMobile_bl = FWDUVPUtils.isMobile;
		this.hasPointerEvent_bl = FWDUVPUtils.hasPointerEvent;
	
		//###################################//
		/*init*/
		//###################################//
		self.init = function(){
			self.parseProperties();
		};
		
		//#############################################//
		// parse properties.
		//#############################################//
		self.parseProperties = function(){
			
			self.useHEXColorsForSkin_bl = self.props_obj.useHEXColorsForSkin; 
			self.useHEXColorsForSkin_bl = self.useHEXColorsForSkin_bl == "yes" ? true : false;
			if(location.protocol.indexOf("file:") != -1) self.useHEXColorsForSkin_bl = false;
			
			self.categoriesId_str = self.props_obj.playlistsId;
			if(!self.categoriesId_str){
				setTimeout(function(){
					if(self == null) return;
					errorMessage_str = "The <font color='#ff0000'>playlistsId</font> property is not defined in the constructor function!";
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:errorMessage_str});
				}, 50);
				return;
			}
				
			
			self.mainFolderPath_str = self.props_obj.mainFolderPath;
			if(!self.mainFolderPath_str){
				setTimeout(function(){
					if(self == null) return;
					errorMessage_str = "The <font color='#ff0000'>mainFolderPath</font> property is not defined in the constructor function!";
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:errorMessage_str});
				}, 50);
				return;
			}
			
			if((self.mainFolderPath_str.lastIndexOf("/") + 1) != self.mainFolderPath_str.length){
				self.mainFolderPath_str += "/";
			}
			
			self.skinPath_str = self.props_obj.skinPath;
			if(!self.skinPath_str){
				setTimeout(function(){
					if(self == null) return;
					errorMessage_str = "The <font color='#ff0000'>skinPath</font> property is not defined in the constructor function!";
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:errorMessage_str});
				}, 50);
				return;
			}
			
			if((self.skinPath_str.lastIndexOf("/") + 1) != self.skinPath_str.length){
				self.skinPath_str += "/";
			}
		
			self.skinPath_str = self.mainFolderPath_str + self.skinPath_str;
			self.flashPath_str = self.mainFolderPath_str + "flashlsChromeless.swf";
			self.flashCopyToCBPath_str = self.mainFolderPath_str + "cb.swf";
			self.proxyPath_str =  self.mainFolderPath_str + "proxy.php";
			self.proxyFolderPath_str = self.mainFolderPath_str  + "proxyFolder.php";
			self.mailPath_str = self.mainFolderPath_str  + "sendMail.php";
			self.sendToAFriendPath_str = self.mainFolderPath_str  + "sendMailToAFriend.php";
			self.videoDownloaderPath_str = self.mainFolderPath_str  + "downloader.php";
			
			self.handPath_str = self.skinPath_str + "hand.cur";
			self.grabPath_str = self.skinPath_str + "grab.cur";
			
			self.categories_el = document.getElementById(self.categoriesId_str);
			
			if(!self.categories_el){
				setTimeout(function(){
					if(self == null) return;
					errorMessage_str = "The playlist with the id <font color='#ff0000'>" + self.categoriesId_str + "</font> is not found!";
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:errorMessage_str});
				}, 50);
				return;
			}
			
			var catsChildren_ar = FWDUVPUtils.getChildren(self.categories_el);
			self.totalCats = catsChildren_ar.length;	
			
			if(self.totalCats == 0){
				setTimeout(function(){
					if(self == null) return;
					errorMessage_str = "At least one playlist is required!";
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:errorMessage_str});
				}, 50);
				return;
			}
			
			
			for(var i=0; i<self.totalCats; i++){
				var obj = {};
				
				var cat_el = null;
				child = catsChildren_ar[i];
				
				if(!FWDUVPUtils.hasAttribute(child, "data-source")){
					setTimeout(function(){
						if(self == null) return;
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Attribute <font color='#ff0000'>data-source</font> is required in the plalists html element at position <font color='#ff0000'>" + (i + 1)});
					}, 50);
					return;
				}
				
				if(!FWDUVPUtils.hasAttribute(child, "data-thumbnail-path")){
					setTimeout(function(){
						if(self == null) return;
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Attribute <font color='#ff0000'>data-thumbnail-path</font> is required in the playlists html element at position <font color='#ff0000'>" + (i + 1)});
					}, 50);
					return;
				}
				
				obj.source = FWDUVPUtils.getAttributeValue(child, "data-source");
				
				if(obj.source.indexOf("=") == -1 && obj.source.indexOf(".xml") == -1){
					cat_el = document.getElementById(obj.source);
				}else{
					cat_el = obj.source;
				}
				
				self.catsRef_ar.push(cat_el);
				obj.thumbnailPath = FWDUVPUtils.getAttributeValue(child, "data-thumbnail-path");
			
				obj.htmlContent = child.innerHTML;
				obj.htmlText_str = child.innerText;
				if(FWDUVPUtils.hasAttribute(child, "data-playlist-name")){
					obj.playlistName =  FWDUVPUtils.getAttributeValue(child, "data-playlist-name");
				}else{
					obj.playlistName = "not defined!";
				};
				
				self.cats_ar[i] = obj;
			}
			
			for(var i=0; i<self.totalCats; i++){
				var obj = {};
				var cat_el = null;
				child = catsChildren_ar[i];	
				cat_el = document.getElementById(FWDUVPUtils.getAttributeValue(child, "data-source"));
				
				try{
					//cat_el.parentNode.removeChild(cat_el);
				}catch(e){};
			}
			
			//try{self.categories_el.parentNode.removeChild(self.categories_el);}catch(e){};
			
			self.startAtPlaylist = self.props_obj.startAtPlaylist || 0;
			if(isNaN(self.startAtPlaylist)) self.startAtPlaylist = 0;
			//if(self.startAtPlaylist != 0) self.startAtPlaylist -= 1;
			if(self.startAtPlaylist < 0){
				self.startAtPlaylist = 0;
			}else if(self.startAtPlaylist > self.totalCats - 1){
				self.startAtPlaylist = self.totalCats - 1;
			}
			
			self.playlistBottomHeight = self.props_obj.playlistBottomHeight || 0;
			self.playlistBottomHeight = Math.min(800, self.playlistBottomHeight);
			
			self.subtitlesOffLabel_str = self.props_obj.subtitlesOffLabel || "Subtitle off";
		
			self.videoSourcePath_str = self.props_obj.videoSourcePath || undefined;
			self.timeColor_str = self.props_obj.timeColor || "#FF0000";
			
			self.youtubeQualityButtonNormalColor_str = self.props_obj.youtubeQualityButtonNormalColor || "#FF0000";
			self.youtubeQualityButtonSelectedColor_str = self.props_obj.youtubeQualityButtonSelectedColor || "#FF0000";
			self.posterBackgroundColor_str = self.props_obj.posterBackgroundColor || "transparent";
		
			
			self.showPlaylistButtonAndPlaylist_bl = self.props_obj.showPlaylistButtonAndPlaylist;
			self.showPlaylistButtonAndPlaylist_bl = self.showPlaylistButtonAndPlaylist_bl == "no" ? false : true;

			self.useResumeOnPlay_bl = self.props_obj.useResumeOnPlay;
			self.useResumeOnPlay_bl = self.useResumeOnPlay_bl == "yes" ? true : false;
			
			self.stopAfterLastVideoHasPlayed_bl = self.props_obj.stopAfterLastVideoHasPlayed;
			self.stopAfterLastVideoHasPlayed_bl = self.stopAfterLastVideoHasPlayed_bl == "yes" ? true : false;
			
	
			self.usePlaylistsSelectBox_bl = self.props_obj.usePlaylistsSelectBox;
			self.usePlaylistsSelectBox_bl = self.usePlaylistsSelectBox_bl == "yes" ? true : false;
			
			self.executeCuepointsOnlyOnce_bl = self.props_obj.executeCuepointsOnlyOnce; 
			self.executeCuepointsOnlyOnce_bl = self.executeCuepointsOnlyOnce_bl == "yes" ? true : false;
			
			self.showPlaylistByDefault_bl = self.props_obj.showPlaylistByDefault;
			self.showPlaylistByDefault_bl = self.showPlaylistByDefault_bl == "no" ? false : true;
			
			self.showThumbnail_bl = self.props_obj.showThumbnail;
			self.showThumbnail_bl = self.showThumbnail_bl == "no" ? false : true;
			
			self.playAfterVideoStop_bl = self.props_obj.playAfterVideoStop;
			self.playAfterVideoStop_bl = self.playAfterVideoStop_bl == "no" ? false : true;
			
			self.openerAlignment_str = self.props_obj.openerAlignment;
			self.openerEqulizerOffsetTop = self.props_obj.openerEqulizerOffsetTop || 0;
			self.openerEqulizerOffsetLeft = self.props_obj.openerEqulizerOffsetLeft || 0;
			
			self.showOpener_bl = self.props_obj.showOpener; 
			self.showOpener_bl = self.showOpener_bl == "yes" ? true : false;

			self.stickyOnScrollShowOpener_bl = self.props_obj.stickyOnScrollShowOpener; 
			self.stickyOnScrollShowOpener_bl = self.stickyOnScrollShowOpener_bl == "yes" ? true : false;
			
			self.showOpenerPlayPauseButton_bl = self.props_obj.showOpenerPlayPauseButton;
			self.showOpenerPlayPauseButton_bl = self.showOpenerPlayPauseButton_bl == "yes" ? true : false;
			
			self.animate_bl = self.props_obj.animatePlayer; 
			self.animate_bl = self.animate_bl == "yes" ? true : false;
			
			
			
			
			self.showAnnotationsPositionTool_bl =  self.props_obj.showAnnotationsPositionTool;
			self.showAnnotationsPositionTool_bl = self.showAnnotationsPositionTool_bl == "yes" ? true : false;
			if(self.showAnnotationsPositionTool_bl) self.showPlaylistByDefault_bl = false;
			
			self.showPlaylistName_bl = self.props_obj.showPlaylistName;
			self.showPlaylistName_bl = self.showPlaylistName_bl == "no" ? false : true;
			
			self.showSearchInput_bl = self.props_obj.showSearchInput;
			self.showSearchInput_bl = self.showSearchInput_bl == "no" ? false : true;
			
			self.showSubtitleByDefault_bl = self.props_obj.showSubtitleByDefault;
			self.showSubtitleByDefault_bl = self.showSubtitleByDefault_bl == "no" ? false : true;
				 
				 
			self.showSubtitleButton_bl = self.props_obj.showSubtitleButton;
			self.showSubtitleButton_bl = self.showSubtitleButton_bl == "no" ? false : true;
			
			
			self.forceDisableDownloadButtonForFolder_bl = self.props_obj.forceDisableDownloadButtonForFolder; 
			self.forceDisableDownloadButtonForFolder_bl = self.forceDisableDownloadButtonForFolder_bl == "yes" ? true : false;
			
			self.normalButtonsColor_str = self.props_obj.normalHEXButtonsColor || "#FFFFFF";
			self.selectedButtonsColor_str = self.props_obj.selectedHEXButtonsColor || "#999999";
				
			self.playlistPosition_str = self.props_obj.playlistPosition || "bottom";
			test = self.playlistPosition_str == "bottom" || self.playlistPosition_str == "right";		   
			if(!test) self.playlistPosition_str = "right";		
			
			self.folderVideoLabel_str = self.props_obj.folderVideoLabel || "Video ";
			
			self.logoPosition_str = self.props_obj.logoPosition || "topleft";
			self.logoPosition_str = String(self.logoPosition_str).toLowerCase();
			test = self.logoPosition_str == "topleft" 
					   || self.logoPosition_str == "topright"
					   || self.logoPosition_str == "bottomleft"
					   || self.logoPosition_str == "bottomright"; 
			if(!test) self.logoPosition_str = "topleft";
			
			self.thumbnailSelectedType_str = self.props_obj.thumbnailSelectedType || "opacity";
			if(self.thumbnailSelectedType_str != "blackAndWhite"  
				&& self.thumbnailSelectedType_str != "threshold" 
				&& self.thumbnailSelectedType_str != "opacity"){
				self.thumbnailSelectedType_str = "opacity";
			}
			if(self.isMobile_bl || FWDUVPUtils.isIEAndLessThen9)  self.thumbnailSelectedType_str = "opacity";
			if(document.location.protocol == "file:") self.thumbnailSelectedType_str = "opacity";
			
			self.adsButtonsPosition_str = self.props_obj.adsButtonsPosition || "left";
			self.adsButtonsPosition_str = String(self.adsButtonsPosition_str).toLowerCase();
			test = self.adsButtonsPosition_str == "left" 
					   || self.adsButtonsPosition_str == "right";
					 	   
			if(!test) self.adsButtonsPosition_str = "left";
			
			self.skipToVideoButtonText_str = self.props_obj.skipToVideoButtonText || "not defined";
			self.skipToVideoText_str = self.props_obj.skipToVideoText;
			
			self.adsTextNormalColor = self.props_obj.adsTextNormalColor || "#FF0000";
			self.adsTextSelectedColor = self.props_obj.adsTextSelectedColor || "#FF0000";
			self.adsBorderNormalColor_str = self.props_obj.adsBorderNormalColor || "#FF0000";
			self.adsBorderSelectedColor_str = self.props_obj.adsBorderSelectedColor || "#FF0000";
				
			self.volume = self.props_obj.volume;
			if(self.volume == undefined) self.volume = 1;
			
			
			if(isNaN(self.volume)) volume = 1;
			
			if(self.volume > 1 || self.isMobile_bl){
				self.volume = 1;
			}else if(self.volume <0){
				self.volume = 0;
			}
			
			self.buttonsToolTipFontColor_str = self.props_obj.buttonsToolTipFontColor || "#FF0000";
			self.toolTipsButtonFontColor_str = self.props_obj.toolTipsButtonFontColor || "#FF0000";
			self.shareAndEmbedTextColor_str = self.props_obj.shareAndEmbedTextColor || "#FF0000";
			self.inputBackgroundColor_str = self.props_obj.inputBackgroundColor || "#FF0000";
			self.inputColor_str = self.props_obj.inputColor || "#FF0000";
			self.searchInputBackgroundColor_str = self.props_obj.searchInputBackgroundColor || "#FF0000";
			self.borderColor_str = self.props_obj.borderColor || "#FF0000";
			self.searchInputColor_str = self.props_obj.searchInputColor || "#FF0000";
			self.youtubeAndFolderVideoTitleColor_str = self.props_obj.youtubeAndFolderVideoTitleColor || "#FF0000";
			self.folderAudioSecondTitleColor_str = self.props_obj.folderAudioSecondTitleColor || "#666666";
			self.youtubeDescriptionColor_str = self.props_obj.youtubeDescriptionColor || "#FF0000"; 
			self.youtubeOwnerColor_str = self.props_obj.youtubeOwnerColor || "#FF0000";
			self.secondaryLabelsColor_str = self.props_obj.secondaryLabelsColor || "#FF0000"; 
			self.mainLabelsColor_str = self.props_obj.mainLabelsColor || "#FF0000"; 
			self.playlistBackgroundColor_str = self.props_obj.playlistBackgroundColor || "#FF0000"; 
			self.thumbnailNormalBackgroundColor_str = self.props_obj.thumbnailNormalBackgroundColor || "#FF0000"; 
			self.playlistNameColor_str = self.props_obj.playlistNameColor || "#FF0000"; 
			self.thumbnailHoverBackgroundColor_str = self.props_obj.thumbnailHoverBackgroundColor || "#FF0000"; 
			self.thumbnailDisabledBackgroundColor_str = self.props_obj.thumbnailDisabledBackgroundColor || "#FF0000"; 
			self.mainSelectorBackgroundSelectedColor = self.props_obj.mainSelectorBackgroundSelectedColor || "#FFFFFF";
			self.mainSelectorTextNormalColor = self.props_obj.mainSelectorTextNormalColor || "#FFFFFF";
			self.mainSelectorTextSelectedColor = self.props_obj.mainSelectorTextSelectedColor || "#000000";
			self.mainButtonBackgroundNormalColor = self.props_obj.mainButtonBackgroundNormalColor || "#212021";
			self.mainButtonBackgroundSelectedColor = self.props_obj.mainButtonBackgroundSelectedColor || "#FFFFFF"; 
			self.mainButtonTextNormalColor = self.props_obj.mainButtonTextNormalColor || "#FFFFFF";
			self.mainButtonTextSelectedColor = self.props_obj.mainButtonTextSelectedColor || "#000000";
			self.logoLink_str = self.props_obj.logoLink || "none"; 
			self.startAtVideo = parseInt(self.props_obj.startAtVideo) || 0;
			self.audioVisualizerLinesColor_str = self.props_obj.audioVisualizerLinesColor || "#0099FF";
			self.audioVisualizerCircleColor_str = self.props_obj.audioVisualizerCircleColor || "#FFFFFF";
			self.privateVideoPassword_str = self.props_obj.privateVideoPassword;

			self.contextMenuBackgroundColor_str = self.props_obj.contextMenuBackgroundColor || "#000000";
			self.contextMenuBorderColor_str = self.props_obj.contextMenuBorderColor || "#FF0000";
			self.contextMenuSpacerColor_str = self.props_obj.contextMenuSpacerColor || "#FF0000";
			self.contextMenuItemNormalColor_str = self.props_obj.contextMenuItemNormalColor || "#FF0000";
			self.contextMenuItemSelectedColor_str = self.props_obj.contextMenuItemSelectedColor || "#FF0000";
			self.contextMenuItemDisabledColor_str = self.props_obj.contextMenuItemDisabledColor || "#FF0000";

			self.showScriptDeveloper_bl = self.props_obj.showScriptDeveloper;
			self.showScriptDeveloper_bl = self.showScriptDeveloper_bl == "yes" ? true : false;

			self.showContextmenu_bl = self.props_obj.showContextmenu;
			self.showContextmenu_bl = self.showContextmenu_bl == "no" ? false : true;
		
			self.nextAndPrevSetButtonsMargins = self.props_obj.nextAndPrevSetButtonsMargins || 0;
			self.buttonsMargins = self.props_obj.buttonsMargins || 0; 
			self.thumbnailMaxWidth = self.props_obj.thumbnailMaxWidth || 330; 
			self.thumbnailMaxHeight = self.props_obj.thumbnailMaxHeight || 330;
			self.horizontalSpaceBetweenThumbnails = self.props_obj.horizontalSpaceBetweenThumbnails;
			self.verticalSpaceBetweenThumbnails = self.props_obj.verticalSpaceBetweenThumbnails;
			self.totalPlaylists = self.cats_ar.length;
			self.controllerHeight = self.props_obj.controllerHeight || 50;
			self.startSpaceBetweenButtons = self.props_obj.startSpaceBetweenButtons || 0;
			self.controllerHideDelay = self.props_obj.controllerHideDelay || 2;
			self.controllerHideDelay *= 1000;
			self.spaceBetweenButtons = self.props_obj.spaceBetweenButtons || 0;
			self.scrubbersOffsetWidth = self.props_obj.scrubbersOffsetWidth || 0;
			self.mainScrubberOffestTop = self.props_obj.mainScrubberOffestTop || 0;
			self.volumeScrubberOffsetTopWidth = self.props_obj.volumeScrubberOffsetTopWidth || 0;
			self.timeOffsetLeftWidth = self.props_obj.timeOffsetLeftWidth || 0;
			self.timeOffsetRightWidth = self.props_obj.timeOffsetRightWidth || 0;
			self.timeOffsetTop = self.props_obj.timeOffsetTop || 0;
			self.embedWindowCloseButtonMargins = self.props_obj.embedAndInfoWindowCloseButtonMargins || 0;
			self.logoMargins = self.props_obj.logoMargins || 0;
			self.maxPlaylistItems = self.props_obj.maxPlaylistItems || 50;
			self.volumeScrubberHeight = self.props_obj.volumeScrubberHeight || 10;
			self.volumeScrubberOfsetHeight = self.props_obj.volumeScrubberOfsetHeight || 0;
			if(self.volumeScrubberHeight > 200) self.volumeScrubberHeight = 200;
			self.buttonsToolTipHideDelay = self.props_obj.buttonsToolTipHideDelay || 1.5;
			self.thumbnailWidth = self.props_obj.thumbnailWidth || 80;
			self.thumbnailWidth = Math.min(150, self.thumbnailWidth);
			self.thumbnailHeight = self.props_obj.thumbnailHeight || 80;
			self.spaceBetweenThumbnails = self.props_obj.spaceBetweenThumbnails || 0;
			self.thumbnailHeight = Math.min(150, self.thumbnailHeight);
			self.timeOffsetTop = self.props_obj.timeOffsetTop || 0;
			self.scrollbarOffestWidth = self.props_obj.scrollbarOffestWidth || 0;
			self.scollbarSpeedSensitivity = self.props_obj.scollbarSpeedSensitivity || .5;
			self.facebookAppId_str = self.props_obj.facebookAppId;
			
			self.aopwBorderSize = self.props_obj.aopwBorderSize || 0; 
			self.aopwTitle = self.props_obj.aopwTitle || "Advertisement";
			self.aopwTitleColor_str = self.props_obj.aopwTitleColor || "#FFFFFF"; 
			self.aopwWidth = self.props_obj.aopwWidth || 200; 
			self.aopwHeight = self.props_obj.aopwHeight || 200; 
			self.allowToChangeVolume_bl = true;
			if(self.isMobile_bl) self.allowToChangeVolume_bl = false;
			
			self.fillEntireVideoScreen_bl = self.props_obj.fillEntireVideoScreen; 
			self.fillEntireVideoScreen_bl = self.fillEntireVideoScreen_bl == "yes" ? true : false;
			
			self.showContextMenu_bl = self.props_obj.showContextMenu; 
			self.showContextMenu_bl = self.showContextMenu_bl == "no" ? false : true;
			
			self.showController_bl = self.props_obj.showController; 
			self.showController_bl = self.showController_bl == "no" ? false : true;
			
			self.showButtonsToolTip_bl = self.props_obj.showButtonsToolTips; 
			self.showButtonsToolTip_bl = self.showButtonsToolTip_bl == "no" ? false : true;
			if(self.isMobile_bl) self.showButtonsToolTip_bl = false;
			
			self.showButtonsToolTip_bl = self.props_obj.showButtonsToolTip; 
			self.showButtonsToolTip_bl = self.showButtonsToolTip_bl == "no" ? false : true;
			if(self.isMobile_bl) self.showButtonsToolTip_bl = false;
			
			self.addKeyboardSupport_bl = self.props_obj.addKeyboardSupport; 
			self.addKeyboardSupport_bl = self.addKeyboardSupport_bl == "no" ? false : true;

			self.useAToB = self.props_obj.useAToB == "yes" ? true : false;
			self.playsinline = self.props_obj.playsinline == "yes" ? true : false;

			self.atbTimeBackgroundColor = self.props_obj.atbTimeBackgroundColor || "transparent";
			self.atbTimeTextColorNormal = self.props_obj.atbTimeTextColorNormal ||  "#888888";
			self.atbTimeTextColorSelected = self.props_obj.atbTimeTextColorSelected || "#FFFFFF";
			self.atbButtonTextNormalColor = self.props_obj.atbButtonTextNormalColor || "#888888";
			self.atbButtonTextSelectedColor = self.props_obj.atbButtonTextSelectedColor || "#FFFFFF";
			self.atbButtonBackgroundNormalColor = self.props_obj.atbButtonBackgroundNormalColor || "#FFFFFF";
			self.atbButtonBackgroundSelectedColor = self.props_obj.atbButtonBackgroundSelectedColor || "#000000";
			
			self.addMouseWheelSupport_bl = self.props_obj.addMouseWheelSupport; 
			self.addMouseWheelSupport_bl = self.addMouseWheelSupport_bl == "no" ? false : true;
			
			self.showPlaylistsSearchInput_bl = self.props_obj.showPlaylistsSearchInput; 
			self.showPlaylistsSearchInput_bl = self.showPlaylistsSearchInput_bl == "yes" ? true : false;
			
			self.inputBackgroundColor_str = self.props_obj.inputBackgroundColor || "#FFFFFF";
			self.inputColor_str = self.props_obj.inputColor || "#FFFFFF"; 

			self.scrubbersToolTipLabelBackgroundColor = self.props_obj.scrubbersToolTipLabelBackgroundColor || "#FFFFFF";
			self.scrubbersToolTipLabelFontColor  = self.props_obj.scrubbersToolTipLabelFontColor || "#000000";
		
			self.autoPlay_bl = self.props_obj.autoPlay; 
			self.autoPlay_bl = self.autoPlay_bl == "yes" ? true : false;
			if(FWDUVPUtils.isMobile) self.autoPlay_bl = false;
			
			self.showNextAndPrevButtons_bl = self.props_obj.showNextAndPrevButtons; 
			self.showNextAndPrevButtons_bl = self.showNextAndPrevButtons_bl == "no" ? false : true;
			
			self.showPlaylistsButtonAndPlaylists_bl = self.props_obj.showPlaylistsButtonAndPlaylists;
			self.showPlaylistsButtonAndPlaylists_bl = self.showPlaylistsButtonAndPlaylists_bl == "no" ? false : true;
			
			self.showEmbedButton_bl = self.props_obj.showEmbedButton;
			self.showEmbedButton_bl = self.showEmbedButton_bl == "no" ? false : true;

			self.showScrubberWhenControllerIsHidden_bl = self.props_obj.showScrubberWhenControllerIsHidden; 
			self.showScrubberWhenControllerIsHidden_bl = self.showScrubberWhenControllerIsHidden_bl == "no" ? false : true;

			self.showMainScrubberToolTipLabel_bl = self.props_obj.showMainScrubberToolTipLabel;
			self.showMainScrubberToolTipLabel_bl = self.showMainScrubberToolTipLabel_bl == "yes" ? true : false;
			
			self.showPlaylistsByDefault_bl = self.props_obj.showPlaylistsByDefault; 
			self.showPlaylistsByDefault_bl = self.showPlaylistsByDefault_bl == "yes" ? true : false;
			
			self.loop_bl = self.props_obj.loop; 
			self.loop_bl = self.loop_bl == "yes" ? true : false;
			
			self.shuffle_bl = self.props_obj.shuffle; 
			self.shuffle_bl = self.shuffle_bl == "yes" ? true : false;
			
			self.showLoopButton_bl = self.props_obj.showLoopButton; 
			self.showLoopButton_bl = self.props_obj.showLoopButton == "no" ? false : true;
			
			self.showShuffleButton_bl = self.props_obj.showShuffleButton; 
			self.showShuffleButton_bl = self.props_obj.showShuffleButton == "no" ? false : true;
			
			self.showDownloadVideoButton_bl = self.props_obj.showDownloadButton; 
			self.showDownloadVideoButton_bl = self.showDownloadVideoButton_bl == "no" ? false : true;
			
			self.showDefaultControllerForVimeo_bl = self.props_obj.showDefaultControllerForVimeo; 
			self.showDefaultControllerForVimeo_bl = self.showDefaultControllerForVimeo_bl == "yes" ? true : false;
			
			self.showInfoButton_bl = self.props_obj.showInfoButton; 
			self.showInfoButton_bl = self.showInfoButton_bl == "no" ? false : true;
		
			self.showLogo_bl = self.props_obj.showLogo; 
			self.showLogo_bl = self.showLogo_bl == "yes" ? true : false;
			
			self.hideLogoWithController_bl = self.props_obj.hideLogoWithController;
			self.hideLogoWithController_bl = self.hideLogoWithController_bl == "yes" ? true : false;
			
			self.showPoster_bl = self.props_obj.showPoster; 
			self.showPoster_bl = self.showPoster_bl == "yes" ? true : false;
			
			self.useVectorIcons_bl = self.props_obj.useVectorIcons; 
			self.useVectorIcons_bl = self.useVectorIcons_bl == "yes" ? true : false;
			
			
			self.showVolumeButton_bl = self.props_obj.showVolumeButton; 
			self.showVolumeButton_bl = self.showVolumeButton_bl == "no" ? false : true;
			if(self.isMobile_bl) self.showVolumeButton_bl = false;
			
			self.showVolumeScrubber_bl = self.showVolumeButton_bl;
			
			self.showControllerWhenVideoIsStopped_bl = self.props_obj.showControllerWhenVideoIsStopped; 
			self.showControllerWhenVideoIsStopped_bl = self.showControllerWhenVideoIsStopped_bl == "yes" ? true : false;
			
			self.showNextAndPrevButtonsInController_bl = self.props_obj.showNextAndPrevButtonsInController; 
			self.showNextAndPrevButtonsInController_bl = self.showNextAndPrevButtonsInController_bl == "yes" ? true : false;
			
			self.showTime_bl = self.props_obj.showTime; 
			self.showTime_bl = self.showTime_bl == "no" ? false : true;
			
			self.showPopupAdsCloseButton_bl = self.props_obj.showPopupAdsCloseButton; 
			self.showPopupAdsCloseButton_bl = self.showPopupAdsCloseButton_bl == "no" ? false : true
			
			self.showFullScreenButton_bl = self.props_obj.showFullScreenButton; 
			self.showFullScreenButton_bl = self.showFullScreenButton_bl == "no" ? false : true;
			
			self.showRewindButton_bl = self.props_obj.showRewindButton; 
			self.showRewindButton_bl = self.showRewindButton_bl == "no" ? false : true;
			
			
			self.disableVideoScrubber_bl = self.props_obj.disableVideoScrubber; 
			self.disableVideoScrubber_bl = self.disableVideoScrubber_bl == "yes" ? true : false;
			
			self.showPlaybackRateButton_bl = self.props_obj.showPlaybackRateButton;
			self.showPlaybackRateButton_bl = self.showPlaybackRateButton_bl == "yes" ? true : false;
			
			self.defaultPlaybackRate_str = self.props_obj.defaultPlaybackRate;
			if(self.defaultPlaybackRate_str ==  undefined) self.defaultPlaybackRate_str = 1;
			self.defaultPlaybackRate_ar = ["0.25", "0.5", "1", "1.25", "1.5", "2"];
			
			self.defaultPlaybackRate_ar.reverse();
			var found_bl = false;
			for(var i=0; i<self.defaultPlaybackRate_ar.length; i++){
				if(self.defaultPlaybackRate_ar[i] == self.defaultPlaybackRate_str){
					found_bl = true;
					self.startAtPlaybackIndex = i;
				}
			}
			
			if(!found_bl){
				self.startAtPlaybackIndex = 3;
				self.defaultPlaybackRate_str = self.defaultPlaybackRate_ar[self.startAtPlaybackIndex];
			}
			
			self.showFullScreenButton_bl = self.props_obj.showFullScreenButton; 
			self.showFullScreenButton_bl = self.showFullScreenButton_bl == "no" ? false : true;
			
			self.repeatBackground_bl = self.props_obj.repeatBackground; 
			self.repeatBackground_bl = self.repeatBackground_bl == "no" ? false : true;
			
			//loggin
			self.playVideoOnlyWhenLoggedIn_bl = self.props_obj.playVideoOnlyWhenLoggedIn; 
			self.playVideoOnlyWhenLoggedIn_bl = self.playVideoOnlyWhenLoggedIn_bl == "yes" ? true : false;
			
			self.isLoggedIn_bl = self.props_obj.isLoggedIn; 
			self.isLoggedIn_bl = self.isLoggedIn_bl == "yes" ? true : false;
					
			self.loggedInMessage_str = self.props_obj.loggedInMessage || "Only loggedin users can view this video";
			
			self.showShareButton_bl = self.props_obj.showShareButton; 
			self.showShareButton_bl = self.showShareButton_bl == "no" ? false : true;
			
			self.openNewPageAtTheEndOfTheAds_bl =  self.props_obj.openNewPageAtTheEndOfTheAds;
			self.openNewPageAtTheEndOfTheAds_bl = self.openNewPageAtTheEndOfTheAds_bl == "yes" ? true : false;
			
			self.playAdsOnlyOnce_bl =  self.props_obj.playAdsOnlyOnce;
			self.playAdsOnlyOnce_bl = self.playAdsOnlyOnce_bl == "yes" ? true : false;
			
			self.startAtRandomVideo_bl =  self.props_obj.startAtRandomVideo;
			self.startAtRandomVideo_bl = self.startAtRandomVideo_bl == "yes" ? true : false;
			
			self.stopVideoWhenPlayComplete_bl =  self.props_obj.stopVideoWhenPlayComplete;
			self.stopVideoWhenPlayComplete_bl = self.stopVideoWhenPlayComplete_bl == "yes" ? true : false;
			
			self.showYoutubeQualityButton_bl = self.props_obj.showQualityButton; 
			self.showYoutubeQualityButton_bl = self.showYoutubeQualityButton_bl == "no" ? false : true;
			//if(FWDUVPlayer.useYoutube == "no" || self.isMobile_bl) self.showYoutubeQualityButton_bl = false;
			
			self.thumbnailsPreviewWidth = self.props_obj.thumbnailsPreviewWidth || 300
			self.thumbnailsPreviewHeight = self.props_obj.thumbnailsPreviewHeight || 168
			self.thumbnailsPreviewBackgroundColor =  self.props_obj.thumbnailsPreviewBackgroundColor || "#000";
			self.thumbnailsPreviewBorderColor =	self.props_obj.thumbnailsPreviewBorderColor || "#333";
			self.thumbnailsPreviewLabelBackgroundColor =	self.props_obj.thumbnailsPreviewLabelBackgroundColor || "#FFF";
			self.thumbnailsPreviewLabelFontColor =	self.props_obj.thumbnailsPreviewLabelFontColor || "#000";
			
			self.arrowN_str = self.skinPath_str + "combobox-arrow-normal.png";
			self.arrowS_str = self.skinPath_str + "combobox-arrow-selected.png";
			
			self.hlsPath_str = self.mainFolderPath_str  + "hls.js";
			self.threeJsPath_str = self.mainFolderPath_str  + "three.js";
			self.threeJsControlsPath_str = self.mainFolderPath_str  + "threeControled.js";
			
			self.logoPath_str = self.skinPath_str + "logo.png";
			self.adLinePat_str = self.skinPath_str + "ad-line.png";
			if(self.props_obj.logoPath) self.logoPath_str = self.props_obj.logoPath;
			
			self.mainScrubberDragLeftAddPath_str = self.skinPath_str + "scrubber-left-drag-add.png";
			self.mainScrubberDragMiddleAddPath_str = self.skinPath_str + "scrubber-middle-drag-add.png";
			
			/*if(self.useVectorIcons_bl){
				var element = document.createElement("link");
				element.setAttribute("rel", "stylesheet");
				element.setAttribute("type", "text/css");
				element.setAttribute("href", "https://use.fontawesome.com/releases/v5.3.1/css/all.css");
				document.getElementsByTagName("head")[0].appendChild(element);
			}*/
			
			self.mainPreloader_img = new Image();
			self.mainPreloader_img.onerror = self.onSkinLoadErrorHandler;
			self.mainPreloader_img.onload = self.onPreloaderLoadHandler;
			self.mainPreloader_img.src = self.skinPath_str + "preloader.jpg";
			

			self.skinPaths_ar = [
				 {img:self.skipIconPath_img = new Image(), src:self.skinPath_str + "skip-icon.png"},
                 {img:self.mainScrubberBkLeft_img = new Image(), src:self.skinPath_str + "scrubber-left-background.png"},
                 {img:self.mainScrubberDragLeft_img = new Image(), src:self.skinPath_str + "scrubber-left-drag.png"},
                 {img:self.mainScrubberLine_img = new Image(), src:self.skinPath_str + "scrubber-line.png"},
                 {img:self.progressLeft_img = new Image(), src:self.skinPath_str + "progress-left.png"},
				 {img:self.volumeScrubberDragBottom_img = new Image(), src:self.skinPath_str + "volume-scrubber-bottom-drag.png"},
				 {img:self.popwColseN_img = new Image(), src:self.skinPath_str + "popw-close-button.png"},
				 {img:self.embedColoseN_img = new Image(), src:self.skinPath_str + "embed-close-button.png"}

			];
			
			if(!self.useVectorIcons_bl){
				self.skinPaths_ar.push(
					 {img:self.prevN_img = new Image(), src:self.skinPath_str + "prev-video.png"},
					 {img:self.nextN_img = new Image(), src:self.skinPath_str + "next-video.png"},
					 {img:self.playN_img = new Image(), src:self.skinPath_str + "play.png"},
					 {img:self.pauseN_img = new Image(), src:self.skinPath_str + "pause.png"},
				     {img:self.volumeN_img = new Image(), src:self.skinPath_str + "volume.png"},
					 {img:self.largePlayN_img = new Image(), src:self.skinPath_str + "large-play.png"},
					 {img:self.categoriesN_img = new Image(), src:self.skinPath_str + "categories-button.png"},
					 {img:self.replayN_img = new Image(), src:self.skinPath_str + "replay-button.png"},
					 {img:self.shuffleN_img = new Image(), src:self.skinPath_str + "shuffle-button.png"},
					 {img:self.fullScreenN_img = new Image(), src:self.skinPath_str + "full-screen.png"},
					 {img:self.ytbQualityN_img = new Image(), src:self.skinPath_str + "youtube-quality.png"},
					 {img:self.shareN_img = new Image(), src:self.skinPath_str + "share.png"},
					 {img:self.facebookN_img = new Image(), src:self.skinPath_str + "facebook.png"},
					 {img:self.infoN_img = new Image(), src:self.skinPath_str + "info-button.png"},
					 {img:self.downloadN_img = new Image(), src:self.skinPath_str + "download-button.png"},
					 {img:self.normalScreenN_img = new Image(), src:self.skinPath_str + "normal-screen.png"},
					 {img:self.embedN_img = new Image(), src:self.skinPath_str + "embed.png"},
					 {img:self.passColoseN_img = new Image(), src:self.skinPath_str + "embed-close-button.png"},
					 {img:self.showSubtitleNPath_img = new Image(), src:self.skinPath_str + "show-subtitle-icon.png"},
					 {img:self.hideSubtitleNPath_img = new Image(), src:self.skinPath_str + "hide-subtitle-icon.png"},
					 {img:self.playbackRateNPath_img = new Image(), src:self.skinPath_str + "playback-rate-normal.png"}
				)

				if(self.useAToB){
					self.skinPaths_ar.push(
						{img:self.atbNPath_img = new Image(), src:self.skinPath_str + "a-to-b-button.png"}
					)
				}
			}
			
			if((self.showOpener_bl && parent.displayType == FWDUVPlayer.STICKY) 
				|| (self.stickyOnScrollShowOpener_bl && parent.stickyOnScroll)){
				self.skinPaths_ar.push(
					 {img:self.openerPauseN_img = new Image(), src:self.skinPath_str + "open-pause-button-normal.png"},
					 {img:self.openerPlayN_img = new Image(), src:self.skinPath_str + "open-play-button-normal.png"},
					 {img:self.animationPath_img = new Image(), src:self.skinPath_str + "equalizer.png"},
					 {img:self.closeN_img = new Image(), src:self.skinPath_str + "opener-close.png"},
					 {img:self.openTopN_img = new Image(), src:self.skinPath_str + "open-button-normal-top.png"},
					 {img:self.openBottomN_img = new Image(), src:self.skinPath_str + "open-button-normal-bottom.png"}
					 
				)
				self.openerPauseS_str = self.skinPath_str + "open-pause-button-selected.png";
				self.openerPlayS_str = self.skinPath_str + "open-play-button-selected.png";
				self.openerAnimationPath_str = self.skinPath_str + "equalizer.png";	
				self.openTopSPath_str = self.skinPath_str + "open-button-selected-top.png";	
				self.openBottomSPath_str = self.skinPath_str + "open-button-selected-bottom.png";	
				self.openTopSPath_str = self.skinPath_str + "open-button-selected-top.png";
				self.openBottomSPath_str = self.skinPath_str + "open-button-selected-bottom.png";
				
				self.closeSPath_str = self.skinPath_str + "opener-close-over.png"
			}
			
			if(self.showRewindButton_bl){
				self.skinPaths_ar.push(
					 {img:self.rewindN_img = new Image(), src:self.skinPath_str + "rewind.png"}
				)
				self.rewindSPath_str = self.skinPath_str + "rewind-over.png";
			}

			if(this.showInfoButton_bl){
				self.skinPaths_ar.push(
					{img:self.infoWindowClooseN_img = new Image(), src:self.skinPath_str + "embed-close-button.png"}
				)
			}
			
			if(self.showNextAndPrevButtonsInController_bl && !self.useVectorIcons_bl){
				
				self.skinPaths_ar.push(
					{img:self.next2N_img = new Image(), src:self.skinPath_str + "next-video.png"},
					{img:self.prev2N_img = new Image(), src:self.skinPath_str + "prev-video.png"}
					
				)
			}
			
			if(self.showShareButton_bl && !self.useVectorIcons_bl){
				
				self.skinPaths_ar.push(
					{img:self.shareClooseN_img = new Image(), src:self.skinPath_str + "embed-close-button.png"},
					{img:self.facebookN_img = new Image(), src:self.skinPath_str + "facebook.png"},
					{img:self.googleN_img = new Image(), src:self.skinPath_str + "google-plus.png"},
					{img:self.twitterN_img = new Image(), src:self.skinPath_str + "twitter.png"},
					{img:self.likedInkN_img = new Image(), src:self.skinPath_str + "likedin.png"},
					{img:self.bufferkN_img = new Image(), src:self.skinPath_str + "buffer.png"},
					{img:self.diggN_img = new Image(), src:self.skinPath_str + "digg.png"},
					{img:self.redditN_img = new Image(), src:self.skinPath_str + "reddit.png"},
					{img:self.thumbrlN_img = new Image(), src:self.skinPath_str + "thumbrl.png"}
				)
				
				self.facebookSPath_str = self.skinPath_str + "facebook-over.png";
				self.googleSPath_str = self.skinPath_str + "google-plus-over.png";
				self.twitterSPath_str = self.skinPath_str + "twitter-over.png";
				self.likedInSPath_str = self.skinPath_str + "likedin-over.png";
				self.bufferSPath_str = self.skinPath_str + "buffer-over.png";
				self.diggSPath_str = self.skinPath_str + "digg-over.png";
				self.redditSPath_str = self.skinPath_str + "reddit-over.png";
				self.thumbrlSPath_str = self.skinPath_str + "thumbrl-over.png";
			}
			
			//setup skin paths
			
			self.atbSPath_str = self.skinPath_str + "a-to-b-button-over.png";
			self.popwColseSPath_str = self.skinPath_str + "popw-close-button-over.png";
			self.popwWindowBackgroundPath_str = self.skinPath_str + "popw-window-background.png";
			self.popwBarBackgroundPath_str = self.skinPath_str + "popw-bar-background.png";
			self.playbackRateSPath_str = self.skinPath_str + "playback-rate-selected.png";
			self.prevSPath_str = self.skinPath_str + "prev-video-over.png"; 
			self.nextSPath_str = self.skinPath_str + "next-video-over.png"; 
			self.playSPath_str = self.skinPath_str + "play-over.png"; 
			self.pauseSPath_str = self.skinPath_str + "pause-over.png";
			self.bkMiddlePath_str = self.skinPath_str + "controller-middle.png";
			self.hdPath_str = self.skinPath_str + "hd.png";
			self.youtubeQualityArrowPath_str = self.skinPath_str + "youtube-quality-arrow.png";
			self.ytbQualityButtonPointerPath_str = self.skinPath_str + "youtube-quality-pointer.png";
			self.controllerBkPath_str = self.skinPath_str + "controller-background.png";
			self.skipIconSPath_str = self.skinPath_str + "skip-icon-over.png";
			self.adsBackgroundPath_str = self.skinPath_str + "ads-background.png";
			self.shareSPath_str = self.skinPath_str + "share-over.png";

			self.mainScrubberBkRightPath_str = self.skinPath_str + "scrubber-right-background.png";
			self.mainScrubberBkMiddlePath_str = self.skinPath_str + "scrubber-middle-background.png";
			self.mainScrubberDragMiddlePath_str = self.skinPath_str + "scrubber-middle-drag.png";
		
			self.volumeScrubberBkBottomPath_str = self.skinPath_str + "volume-scrubber-bottom-background.png"; 
			self.volumeScrubberBkMiddlePath_str = self.skinPath_str + "volume-scrubber-middle-background.png";
			self.volumeScrubberBkTopPath_str = self.skinPath_str + "volume-scrubber-top-background.png";
			self.volumeScrubberDragBottomPath_str = self.skinPath_str + "volume-scrubber-bottom-drag.png";
			self.volumeScrubberLinePath_str = self.skinPath_str + "volume-scrubber-line.png";
			self.volumeScrubberDragMiddlePath_str = self.skinPath_str + "volume-scrubber-middle-drag.png";	
		
			self.volumeSPath_str = self.skinPath_str + "volume-over.png";
			self.volumeDPath_str = self.skinPath_str + "volume-disabled.png";
			self.categoriesSPath_str = self.skinPath_str + "categories-button-over.png";
			self.replaySPath_str = self.skinPath_str + "replay-button-over.png";
			self.toopTipBk_str = self.skinPath_str + "tooltip-background.png"; 
			self.toopTipPointer_str = self.skinPath_str + "tooltip-pointer.png"; 
			self.shufflePathS_str = self.skinPath_str + "shuffle-button-over.png";
			self.passButtonNPath_str = self.skinPath_str + "pass-button.png";
			self.passButtonSPath_str = self.skinPath_str + "pass-button-over.png";
			
			self.largePlayS_str = self.skinPath_str + "large-play-over.png";
			self.fullScreenSPath_str = self.skinPath_str + "full-screen-over.png";
			self.ytbQualitySPath_str = self.skinPath_str + "youtube-quality-over.png";
			self.ytbQualityDPath_str = self.skinPath_str + "youtube-quality-hd.png";
			self.facebookSPath_str = self.skinPath_str + "facebook-over.png";
			self.infoSPath_str = self.skinPath_str + "info-button-over.png";
			self.downloadSPath_str = self.skinPath_str + "download-button-over.png";
			self.normalScreenSPath_str = self.skinPath_str + "normal-screen-over.png";
			self.progressMiddlePath_str = self.skinPath_str + "progress-middle.png";
			self.embedPathS_str = self.skinPath_str + "embed-over.png";
			self.embedWindowClosePathS_str = self.skinPath_str + "embed-close-button-over.png"; 
			self.embedWindowInputBackgroundPath_str = self.skinPath_str + "embed-window-input-background.png";
			self.embedCopyButtonNPath_str = self.skinPath_str + "embed-copy-button.png";
			self.embedCopyButtonSPath_str = self.skinPath_str + "embed-copy-button-over.png";
			self.sendButtonNPath_str = self.skinPath_str + "send-button.png";
			self.sendButtonSPath_str = self.skinPath_str + "send-button-over.png";
			self.embedWindowBackground_str = self.skinPath_str + "embed-window-background.png";
			self.showSubtitleSPath_str = self.skinPath_str + "show-subtitle-icon-over.png";
			self.hideSubtitleSPath_str = self.skinPath_str + "hide-subtitle-icon-over.png";
			self.inputArrowPath_str = self.skinPath_str + "input-arrow.png"; 
			
			if(self.showPlaylistsButtonAndPlaylists_bl){
				self.skinPaths_ar.push(
				    {img:new Image(), src:self.skinPath_str + "categories-background.png"}
				);
				
				if(!self.useVectorIcons_bl){
					self.skinPaths_ar.push(
						{img:self.catNextN_img = new Image(), src:self.skinPath_str + "categories-next-button.png"},
						{img:self.catPrevN_img = new Image(), src:self.skinPath_str + "categories-prev-button.png"},
						{img:self.catCloseN_img = new Image(), src:self.skinPath_str + "categories-close-button.png"}
					)
				}
				self.catBkPath_str = self.skinPath_str + "categories-background.png"; 
				self.catThumbBkPath_str = self.skinPath_str + "categories-thumbnail-background.png"; 
				self.catThumbBkTextPath_str = self.skinPath_str + "categories-thumbnail-text-backgorund.png"; 
				self.catNextSPath_str = self.skinPath_str + "categories-next-button-over.png"; 
				self.catPrevSPath_str = self.skinPath_str + "categories-prev-button-over.png"; 
				self.catCloseSPath_str = self.skinPath_str + "categories-close-button-over.png"; 
			}
			
			self.popupAddCloseNPath_str = self.skinPath_str + "close-button-normal.png"; 
			self.popupAddCloseSPath_str = self.skinPath_str + "close-button-selected.png"; 
			
			self.annotationAddCloseNPath_str = self.skinPath_str + "annotation-close-button-normal.png"; 
			self.annotationAddCloseSPath_str = self.skinPath_str + "annotation-close-button-selected.png";
			
			if(self.showPlaylistButtonAndPlaylist_bl){
				var prevThumbsSetNPath_str;
				
				self.playlistThumbnailsBkPath_str = self.skinPath_str + "playlist-thumbnail-background.png";
				self.playlistBkPath_str = self.skinPath_str + "playlist-background.png";
				
				if(self.playlistPosition_str == "bottom"){
					self.skinPaths_ar.push(
					    {img:self.hidePlaylistN_img = new Image(), src:self.skinPath_str + "hide-horizontal-playlist.png"},
					    {img:self.showPlaylistN_img = new Image(), src:self.skinPath_str + "show-horizontal-playlist.png"}
					);
					self.hidePlaylistSPath_str = self.skinPath_str + "hide-horizontal-playlist-over.png"; 
					self.showPlaylistSPath_str = self.skinPath_str + "show-horizontal-playlist-over.png"; 
				}else{
					self.skinPaths_ar.push(
					    {img:self.hidePlaylistN_img = new Image(), src:self.skinPath_str + "hide-vertical-playlist.png"},
					    {img:self.showPlaylistN_img = new Image(), src:self.skinPath_str + "show-vertical-playlist.png"}
					);
					self.hidePlaylistSPath_str = self.skinPath_str + "hide-vertical-playlist-over.png"; 
					self.showPlaylistSPath_str = self.skinPath_str + "show-vertical-playlist-over.png"; 
				}
				
				self.skinPaths_ar.push(
				    {img:self.scrBkTop_img = new Image(), src:self.skinPath_str + "playlist-scrollbar-background-top.png"},
				    {img:self.scrDragTop_img = new Image(), src:self.skinPath_str + "playlist-scrollbar-drag-top.png"},
				    {img:self.scrLinesN_img = new Image(), src:self.skinPath_str + "playlist-scrollbar-lines.png"}
				);
				
				self.scrBkMiddlePath_str = self.skinPath_str + "playlist-scrollbar-background-middle.png";
				self.scrBkBottomPath_str = self.skinPath_str + "playlist-scrollbar-background-bottom.png";
				self.scrDragMiddlePath_str = self.skinPath_str + "playlist-scrollbar-drag-middle.png";
				self.scrDragBottomPath_str = self.skinPath_str + "playlist-scrollbar-drag-bottom.png";
				self.scrLinesSPath_str = self.skinPath_str + "playlist-scrollbar-lines-over.png";
				self.inputArrowPath_str = self.skinPath_str + "input-arrow.png";
			}
			
			self.totalGraphics = self.skinPaths_ar.length;
			
			if(self.useAToB){
				var script = document.createElement('script');
				script.src =  self.mainFolderPath_str + 'java/FWDUVPATB.js'
				document.head.appendChild(script); //or something of the likes
				script.onerror = function(e){
					console.log(e);
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:'You have enabled the A to B feature<br>A to B js file named <font color="#FF0000">FWDUVPATB.js</font> is not found. Please make sure that the content folder contains the java folder that contains the <font color="#FF0000">FWDUVPATB.js</font> file. '});
					return;
				}
				
				script.onload = function () {
					self.loadSkin();
				}
			}else{
				self.loadSkin();	
			}
		};
		
		//####################################//
		/* Preloader load done! */
		//###################################//
		this.onPreloaderLoadHandler = function(){
			setTimeout(function(){
				self.dispatchEvent(FWDUVPData.PRELOADER_LOAD_DONE);
			}, 50);
		};
		
		//####################################//
		/* load buttons graphics */
		//###################################//
		self.loadSkin = function(){
			var img;
			var src;
			for(var i=0; i<self.totalGraphics; i++){
				img = self.skinPaths_ar[i].img;
				src = self.skinPaths_ar[i].src;
				img.onload = self.onSkinLoadHandler;
				img.onerror = self.onSkinLoadErrorHandler;
				img.src = src;
			}
		};
		
		this.onSkinLoadHandler = function(e){
			self.countLoadedSkinImages++;
			if(self.countLoadedSkinImages == self.totalGraphics){
				setTimeout(function(){
					self.dispatchEvent(FWDUVPData.SKIN_LOAD_COMPLETE);
				}, 50);
			}
		};
		
		self.onSkinLoadErrorHandler = function(e){
			if (FWDUVPUtils.isIEAndLessThen9){
				message = "Graphics image not found!";
			}else{
				message = "The skin icon with label <font color='#ff0000'>" + e.target.src + "</font> can't be loaded, check path!";
			}
			
			if(window.console) console.log(e);
			var err = {text:message};
			setTimeout(function(){
				self.dispatchEvent(FWDUVPData.LOAD_ERROR, err);
			}, 50);
		};
		
		//##########################################//
		/* Download video */
		//##########################################//
		this.downloadVideo = function(sourcePath, pName){
			
			if(document.location.protocol == "file:"){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Downloading video files local is not allowed or possible! To function properly please test online."});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			if(!sourcePath){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Not allowed to download this video!"});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			if(String(sourcePath.indexOf(".mp4")) == -1){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Only mp4 video files hosted on your server can be downloaded."});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			pName = pName.replace(/[^A-Z0-9\-\_\.]+/ig, "_");
			if(pName.length > 40) pName = pName.substr(0, 40) + "...";
			//if(!(/\.(video)$/i).test(pName)) pName+='.mp4';
		
			if(sourcePath.indexOf("http:") == -1){
				
				var path_ar = sourcePath.split(",");
				sourcePath = path_ar[0];
		
				sourcePath = sourcePath.substr(sourcePath.indexOf("/") + 1);
				sourcePath = encodeURIComponent(sourcePath);
			};
			
			var url = self.videoDownloaderPath_str;
			if(!self.dlIframe){
				self.dlIframe = document.createElement("IFRAME");
				self.dlIframe.style.display = "none";
				document.documentElement.appendChild(self.dlIframe);
			}
			
			if(self.isMobile_bl){
			
				var email = self.getValidEmail();
				if(!email) return;
				
				if(self.emailXHR != null){
					try{self.emailXHR.abort();}catch(e){}
					self.emailXHR.onreadystatechange = null;
					self.emailXHR.onerror = null;
					self.emailXHR = null;
				}
				
				self.emailXHR = new XMLHttpRequest();
				
				self.emailXHR.onreadystatechange = function(e){
					if(self.emailXHR.readyState == 4){
						if(self.emailXHR.status == 200){
							if(self.emailXHR.responseText == "sent"){
								alert("Email sent.");
							}else{
								alert("Error sending email, this is a server side error, the php file can't send the email!");
							}
							
						}else{
							alert("Error sending email: " + self.emailXHR.status + ": " + self.emailXHR.statusText);
						}
					}
				};
				
				self.emailXHR.onerror = function(e){
					try{
						if(window.console) console.log(e);
						if(window.console) console.log(e.message);
					}catch(e){};
					alert("Error sending email: " + e.message);
				};

				self.emailXHR.open("get", self.mailPath_str + "?mail=" + email + "&name=" + pName + "&path=" + sourcePath, true);
				self.emailXHR.send();
				return;
			}
		
			self.dlIframe.src = url + "?path="+ sourcePath +"&name=" + pName;
		};
		
		this.getValidEmail = function(){
			var email = prompt("Please enter your email address where the video download link will be sent:");
			var emailRegExp = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		
			while(!emailRegExp.test(email) || email == ""){
				if(email === null) return;
				email = prompt("Please enter a valid email address:");
			}
			return email;
		};
		
		//####################################//
		/* load playlist */
		//####################################//
		this.loadPlaylist = function(id){
			self.stopToLoadPlaylist();
			
			if(self.isPlaylistDispatchingError_bl) return;
			
			clearTimeout(self.dispatchPlaylistLoadCompleteWidthDelayId_to);
			var source = self.catsRef_ar[id];
		
			if(source === undefined){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"<font color='#ff0000'>loadPlaylist()</font> - Please specify a DOM playlist id or youtube playlist id!"});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			if(source === null){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"The playlist with id <font color='#ff0000'>" + self.cats_ar[id].source + "</font> is not found in the DOM."});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
		
			if(!isNaN(source)){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"<font color='#ff0000'>loadPlaylist()</font> - The parameter must be of type string!"});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			self.resetYoutubePlaylistLoader();
			self.isYoutbe_bl = false;
			self.playlist_ar = [];
			
			if(!source.length){
				self.parseDOMPlaylist(source, self.cats_ar[id].source);	
			}else if(source.indexOf("list=") != -1){
				self.isYoutbe_bl = true;
				self.loadYoutubePlaylist(source);
			}else if(source.indexOf("list=") != -1){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Loading youtube playlist is only possible by setting <font color='#ff0000'>useYoutube=\"yes\"</font>."});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}else if(source.indexOf("folder=") != -1){
				self.loadFolderPlaylist(source);
			}else if(source.indexOf(".xml") != -1
			  || source.indexOf("http:") != -1
			  || source.indexOf("https:") != -1
			  || source.indexOf("www.") != -1
			){
				self.loadXMLPlaylist(source);
			}
		};
		
		//#######################################//
		/* load XML playlist (warning this will will work only online on a web server,
		 * it is not working local!) */
		//######################################//
		this.loadXMLPlaylist = function(url){
			if(self.isPlaylistDispatchingError_bl) return;
			
			if(document.location.protocol == "file:"){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Loading XML files local is not allowed or possible!. To function properly please test online."});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			
			self.loadFromFolder_bl = false;
			self.sourceURL_str = url;
			self.xhr = new XMLHttpRequest();
			self.xhr.onreadystatechange = self.ajaxOnLoadHandler;
			self.xhr.onerror = self.ajaxOnErrorHandler;
			
			try{
				self.xhr.open("get", self.proxyPath_str + "?url=" +  self.sourceURL_str + "&rand=" + parseInt(Math.random() * 99999999), true);
				self.xhr.send();
			}catch(e){
				var message = e;
				if(e){if(e.message)message = e.message;}
				self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"XML file can't be loaded! <font color='#ff0000'>" + self.sourceURL_str + "</font>. " + message });
			}
		};
		
		//#######################################//
		/* load folder */
		//######################################//
		this.loadFolderPlaylist = function(url){
			if(self.isPlaylistDispatchingError_bl) return;
			
			if(document.location.protocol == "file:"){
				self.isPlaylistDispatchingError_bl = true;
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Creating a video playlist from a folder is not allowed or possible local! To function properly please test online."});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}	
			
			self.loadFromFolder_bl = true;
			self.sourceURL_str = url.substr(url.indexOf("=") + 1);
			self.xhr = new XMLHttpRequest();
			self.xhr.onreadystatechange = self.ajaxOnLoadHandler;
			self.xhr.onerror = self.ajaxOnErrorHandler;
			
			try{
				self.xhr.open("get", self.proxyFolderPath_str + "?dir=" +  encodeURIComponent(self.sourceURL_str) + "&videoLabel=" + self.folderVideoLabel_str  + "&rand=" + parseInt(Math.random() * 9999999), true);
				self.xhr.send();
			}catch(e){
				var message = e;
				if(e){if(e.message)message = e.message;}
				self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Folder proxy file path is not found: <font color='#ff0000'>" + self.proxyFolderPath_str + "</font>"});
			}
		};
		
		//##########################################//
		/* load youtube list */
		//##########################################//
		this.loadYoutubePlaylist = function(url){
			if(self.isPlaylistDispatchingError_bl && !self.isYoutbe_bl) return;
			
			if(!self.youtubeUrl_str){
				url = url.substr(url.indexOf("=") + 1);
				self.youtubeUrl_str = url;
			}
		
			
			self.loadFromFolder_bl = true;
			
			if(self.nextPageToken_str){
				self.sourceURL_str = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&pageToken=" + self.nextPageToken_str + "&playlistId=" + self.youtubeUrl_str + "&key=AIzaSyAlyhJ-C5POyo4hofPh3b7ECAxWy6t6lyg&maxResults=50&callback=" + parent.instanceName_str + ".data.parseYoutubePlaylist";
			}else{
				self.sourceURL_str = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + self.youtubeUrl_str + "&key=AIzaSyAlyhJ-C5POyo4hofPh3b7ECAxWy6t6lyg&maxResults=50&callback=" + parent.instanceName_str + ".data.parseYoutubePlaylist";
			}
			
			if(self.scs_el == null){
				try{
					self.scs_el = document.createElement('script');
					self.scs_el.src = self.sourceURL_str;
					self.scs_el.id = parent.instanceName_str + ".data.parseYoutubePlaylist";
					document.documentElement.appendChild(self.scs_el);
				}catch(e){}
			}
			self.JSONPRequestTimeoutId_to = setTimeout(self.JSONPRequestTimeoutError, 6000);
		
		};
		
		this.JSONPRequestTimeoutError = function(){
			self.stopToLoadPlaylist();
			self.isPlaylistDispatchingError_bl = true;
			showLoadPlaylistErrorId_to = setTimeout(function(){
				self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Error loading youtube playlist!<font color='#ff0000'>" + self.youtubeUrl_str + "</font>"});
				self.isPlaylistDispatchingError_bl = false;
			}, 50);
			return;
		};
		
		this.resetYoutubePlaylistLoader = function(){
			self.isYoutbe_bl = false;
			self.youtubeObject_ar = null;
			self.nextPageToken_str = null;
			self.youtubeUrl_str = null;
		};
		
		//######################################//
		/* Handle ajax response */
		//######################################//
		this.ajaxOnErrorHandler = function(e){
			try{
				if(window.console) console.log(e);
				if(window.console) console.log(e.message);
			}catch(e){};
			if(self.loadFromFolder_bl){
				self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Error loading file : <font color='#ff0000'>" + self.proxyFolderPath_str + "</font>. Make sure the path is correct"});
			}else{
				self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Error loading file : <font color='#ff0000'>" + self.proxyPath_str + "</font>. Make sure the path is correct"});
			}
		};
		
		this.ajaxOnLoadHandler = function(e){
			var response;
			var isXML = false;
			
			if(self.xhr.readyState == 4){
				if(self.xhr.status == 404){
					if(self.loadFromFolder_bl){
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Folder proxy file path is not found: <font color='#ff0000'>" + self.proxyFolderPath_str + "</font>"});
					}else{
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Proxy file path is not found: <font color='#ff0000'>" + self.proxyPath_str + "</font>"});
					}
					
				}else if(self.xhr.status == 408){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Proxy file request load timeout!"});
				}else if(self.xhr.status == 200){
					if(self.xhr.responseText.indexOf("<b>Warning</b>:") != -1){
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Error loading folder: <font color='#ff0000'>" + self.sourceURL_str + "</font>. Make sure that the folder path is correct!"});
						return;
					}
					
					if(window.JSON){
						response = JSON.parse(self.xhr.responseText);
					}else{
						response = eval('('+ self.xhr.responseText +')');
					}
					
					if(response.folder){
						self.parseFolderJSON(response);
					}else if(response.li){
						self.parseXML(response);
					}else if(response.error){//this applies only with proxy (xml and poscast)
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Error loading file: <font color='#ff0000'>" + self.sourceURL_str + "</font>. Make sure the file path (xml or podcast) is correct and well formatted!"});
					}
				}
			}
		};
		
		this.parseYoutubePlaylist = function(object){
			
			if(self.isPlaylistDispatchingError_bl || !self.isYoutbe_bl) return;
			
			if(object.error){
				self.JSONPRequestTimeoutError();
				if(console) console.dir(object);
				return;
			}
			
			self.playlist_ar = [];
			var tt;
			var item;
			var videoSource;
			
			if(!self.youtubeObject_ar){
				self.youtubeObject_ar = [];
			}
			
			for(var i=0; i<object.items.length; i++){
				self.youtubeObject_ar.push(object.items[i]);
			}
			
			tt = self.youtubeObject_ar.length;
			
			self.stopToLoadPlaylist();
			
			if(object.nextPageToken && tt < self.maxPlaylistItems){
				self.nextPageToken_str = object.nextPageToken;
				self.loadYoutubePlaylist();
				return;
			}
			
			for(var i=0; i< tt; i++){
				if(i > self.maxPlaylistItems - 1) break;
				
				var obj = {};
				item = self.youtubeObject_ar[i];
				if(item.snippet.thumbnails){
					obj.videoSource = item.snippet.resourceId.videoId;
					obj.startAtVideo = 0;
					obj.videoSource = [{source:"https://www.youtube.com/watch?v=" + item.snippet.resourceId.videoId}];
					obj.owner = item.snippet.channelTitle;
					obj.gaStr =  item.snippet.title;
					
					obj.title = "<p class='ytbChangeColor' style='color:" + self.youtubeAndFolderVideoTitleColor_str + ";margin:0px;padding:0px;margin-top:2px;margin-bottom:4x;line-height:16px;'>" + item.snippet.title + "</p>";
					var desc = item.snippet.description;
					if(obj.title.length > 165){
						desc = desc.substr(0, 60);
					}else{
						desc = desc.substr(0, 90);
					}
					
					desc = desc.substr(0, desc.lastIndexOf(" ")) + " ...";
					obj.title += "<p style='color:" + self.youtubeOwnerColor_str + ";margin:0px;padding:0px;margin-top:6px;margin-bottom:4x;line-height:16px;'> " + desc + "</p>";
				
					obj.titleText = item.snippet.title;
					obj.titleText = item.snippet.title;
					obj.desc = undefined;
					
					obj.desc = "<p style='color:" + self.youtubeAndFolderVideoTitleColor_str + ";margin:10px;margin-top:12px;margin-bottom:0px;padding:0px;'>" + item.snippet.title + "</p><p style='color:" + self.youtubeDescriptionColor_str + ";margin:0;padding:10px;padding-top:8px;line-height:16px;'>" + item.snippet.description + "</p>";
				
					obj.downloadable = false;
					try{
						obj.thumbSource = item.snippet.thumbnails["default"].url;
					}catch(e){}
					obj.posterSource =  "none";
					
					if(item.snippet.title.indexOf("eleted video") == -1 && item.snippet.title.indexOf("his video is unavailable") == -1){
						self.playlist_ar.push(obj);
						self.youtubelist_ar = self.playlist_ar;
					}
				}
			}
			
			clearTimeout(self.dispatchPlaylistLoadCompleteWidthDelayId_to);
			self.dispatchPlaylistLoadCompleteWidthDelayId_to = setTimeout(function(){
				self.dispatchEvent(FWDUVPData.PLAYLIST_LOAD_COMPLETE);
			}, 50);
	
			self.isDataLoaded_bl = true;
		};
		
		this.setYoutubePlaylistHEXColor = function(color){
			self.youtubeAndFolderVideoTitleColor_str = color;
		}
		
		this.closeJsonPLoader = function(){
			clearTimeout(self.JSONPRequestTimeoutId_to);
		};
		
		//##########################################//
		/* parse DOM playlist */
		//##########################################//
		this.parseDOMPlaylist = function(element, id){
			if(self.isPlaylistDispatchingError_bl) return;
		
			var children_ar = FWDUVPUtils.getChildren(element);
			var totalChildren = children_ar.length;
			var child;
			var has360Video = false;
			self.playlist_ar = [];
			
			if(totalChildren == 0){
				showLoadPlaylistErrorId_to = setTimeout(function(){
					self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"At least one video is required in the playlist with id: <font color='#ff0000'>" + id + "</font>"});
					self.isPlaylistDispatchingError_bl = false;
				}, 50);
				return;
			}
			
			for(var i=0; i<totalChildren; i++){
				var obj = {};
				var adsObj;
				child = children_ar[i];
				
				if(!FWDUVPUtils.hasAttribute(child, "data-thumb-source")){
					self.isPlaylistDispatchingError_bl = true;
					showLoadPlaylistErrorId_to = setTimeout(function(){
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Attribute <font color='#ff0000'>data-thumb-source</font> is required in the playlist at position <font color='#ff0000'>" + (i + 1)});
					}, 50);
					return;
				}
				
				if(!FWDUVPUtils.hasAttribute(child, "data-video-source")){
					self.isPlaylistDispatchingError_bl = true;
					showLoadPlaylistErrorId_to = setTimeout(function(){
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Attribute <font color='#ff0000'>data-video-source</font> is required in the playlist at position <font color='#ff0000'>" + (i + 1)});
					}, 50);
					return;
				}
				
				if(i > self.maxPlaylistItems - 1) break;
				
				obj.thumbSource = encodeURI(FWDUVPUtils.getAttributeValue(child, "data-thumb-source"));
				
				obj.videoSource = FWDUVPUtils.getAttributeValue(child, "data-video-source");
				obj.dataPlaybackRate  = FWDUVPUtils.getAttributeValue(child, "data-playback-rate");
				obj.startAtVideo = FWDUVPUtils.getAttributeValue(child, "data-start-at-video") || 0;
				obj.isLive = FWDUVPUtils.getAttributeValue(child, "data-is-live");
				obj.atb = FWDUVPUtils.getAttributeValue(child, "data-use-a-to-b") == "yes" ? true : false;
				obj.thumbnailsPreview = FWDUVPUtils.getAttributeValue(child, "data-thumbnails-preview");
				if(!self.useAToB) obj.atb = false;
				
				if(obj.isLive == "yes"){
					obj.isLive = true;
				}else{
					obj.isLive = false;
				}
				
				obj.isPrivate = FWDUVPUtils.getAttributeValue(child, "data-is-private");
				if(obj.isPrivate == "yes"){
					obj.isPrivate = true;
				}else{
					obj.isPrivate = false;
				}
				
				obj.redirectURL = FWDUVPUtils.getAttributeValue(child, "data-redirect-url");
				obj.redirectTarget = FWDUVPUtils.getAttributeValue(child, "data-redirect-target");
				
				obj.privateVideoPassword_str = FWDUVPUtils.getAttributeValue(child, "data-private-video-password");
				
				obj.startAtTime = FWDUVPUtils.getAttributeValue(child, "data-start-at-time");
				if(obj.startAtTime == "00:00:00" || !FWDUVPUtils.checkTime(obj.startAtTime)) obj.startAtTime = undefined;
				
				obj.stopAtTime = FWDUVPUtils.getAttributeValue(child, "data-stop-at-time");
				if(obj.stopAtTime == "00:00:00" || !FWDUVPUtils.checkTime(obj.stopAtTime)) obj.stopAtTime = undefined;
				
				if(obj.videoSource.indexOf("{source:") != -1){
					
					try{
						obj.videoLabels_ar = [];
						obj.videoSource = eval(obj.videoSource);
						for(var m=0; m<obj.videoSource.length; m++){
							obj.videoLabels_ar[m] = obj.videoSource[m]["label"];
						}
						for(var m=0; m<obj.videoSource.length; m++){
							obj.videoSource[m].source = encodeURI(obj.videoSource[m].source);
							obj.videoSource[m].source = decodeURI(obj.videoSource[m].source);
						}
						for(var m=0; m<obj.videoSource.length; m++){
							obj.videoSource[m].is360 = obj.videoSource[m]['is360'];
							if(obj.videoSource[m].is360 == "yes") obj.videoSource[m].is360 = true;
							if(obj.videoSource[m].is360 == "no") obj.videoSource[m].is360 = false;
							if(obj.videoSource[m].is360 == true) has360Video = true;
						}
						
						obj.videoLabels_ar.reverse();
					}catch(e){
						self.isPlaylistDispatchingError_bl = true;
						showLoadPlaylistErrorId_to = setTimeout(function(){
							self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Please make sure that the <font color='#ff0000'>data-video-source</font> attribute contains an array of videos at position <font color='#ff0000'>" + (i + 1) + "</font>"});
						}, 50);
						return;
					}
				}else{
					var tempSource = encodeURI(obj.videoSource);
					tempSource = decodeURI(obj.videoSource);
					obj.videoSource = [{source:tempSource}];
				}
				
				if(FWDUVPUtils.hasAttribute(child, "data-subtitle-soruce")){
					obj.subtitleSource = FWDUVPUtils.getAttributeValue(child, "data-subtitle-soruce");
					if(obj.subtitleSource.indexOf("{source:") != -1){
						obj.startAtSubtitle = FWDUVPUtils.getAttributeValue(child, "data-start-at-subtitle") || 0;
						if(obj.subtitleSource.indexOf("{source:") != -1){
							try{
								obj.subtitleSource = eval(obj.subtitleSource);
							}catch(e){
								self.isPlaylistDispatchingError_bl = true;
								showLoadPlaylistErrorId_to = setTimeout(function(){
									self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Please make sure that the <font color='#ff0000'>data-subtitle-source</font> attribute contains an array of subtitles at position <font color='#ff0000'>" + (i + 1) + "</font>"});
								}, 50);
								return;
							}
							obj.subtitleSource.splice(0,0, {source:"none", label:self.subtitlesOffLabel_str});
							obj.subtitleSource.reverse();
						}
					}else{
						obj.subtitleSource = [{source:obj.subtitleSource}];
					}
				}
				
				obj.dataAdvertisementOnPauseSource  = FWDUVPUtils.getAttributeValue(child, "data-advertisement-on-pause-source");
				obj.scrubAtTimeAtFirstPlay = FWDUVPUtils.getAttributeValue(child, "data-scrub-at-time-at-first-play") || "none";
				if(/^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)/g.test(obj.scrubAtTimeAtFirstPlay)){
					obj.scrubAtTimeAtFirstPlay = FWDUVPUtils.getSecondsFromString(obj.scrubAtTimeAtFirstPlay);
				}else{
					obj.scrubAtTimeAtFirstPlay = undefined;
				}
				
				if(FWDUVPUtils.hasAttribute(child, "data-poster-source")){
					obj.posterSource = encodeURI(FWDUVPUtils.getAttributeValue(child, "data-poster-source"));
				}else{
					obj.posterSource = "none";
				}
			
				obj.downloadPath = obj.videoSource[obj.startAtVideo];
				
				if(FWDUVPUtils.hasAttribute(child, "data-downloadable") && self.showDownloadVideoButton_bl){
					obj.downloadable = FWDUVPUtils.getAttributeValue(child, "data-downloadable") == "yes" ? true : false;
					if(obj.downloadPath.source.indexOf(".") ==  -1)  obj.downloadable = false;
				}else{
					obj.downloadable = false;
				}
				
				//video popup adds and annotations
				var annotations_ar;
				var mainPopupAds_ar = FWDUVPUtils.getChildren(child);
				var tempPopupAds_ar;
				var popupAds_ar;
				var popupOrAnnotationChild;
				var finalPopupChild;
				var popupObj;
				
				for(var k=0; k<mainPopupAds_ar.length; k++){
					
					popupOrAnnotationChild = mainPopupAds_ar[k];	
					if(FWDUVPUtils.hasAttribute(popupOrAnnotationChild, "data-add-popup")){
						tempPopupAds_ar = FWDUVPUtils.getChildren(popupOrAnnotationChild);
						popupAds_ar = [];
						for(var x=0; x<tempPopupAds_ar.length; x++){
							finalPopupChild = tempPopupAds_ar[x];
							if(finalPopupChild){
								popupObj = {};
								popupObj.source = encodeURI(FWDUVPUtils.getAttributeValue(finalPopupChild, "data-image-path"));
								popupObj.start = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(finalPopupChild, "data-time-start"));
								popupObj.end = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(finalPopupChild, "data-time-end"));
								popupObj.link = FWDUVPUtils.getAttributeValue(finalPopupChild, "data-link");
								popupObj.target = FWDUVPUtils.getAttributeValue(finalPopupChild, "data-target");
								popupObj.google_ad_width = parseInt(FWDUVPUtils.getAttributeValue(finalPopupChild, "data-google-ad-width")) || 600;
								popupObj.google_ad_height = parseInt(FWDUVPUtils.getAttributeValue(finalPopupChild, "data-google-ad-height")) || 200;
								popupObj.google_ad_client = FWDUVPUtils.getAttributeValue(finalPopupChild, "data-google-ad-client");
								popupObj.google_ad_slot = FWDUVPUtils.getAttributeValue(finalPopupChild, "data-google-ad-slot");
								popupAds_ar.push(popupObj);
							}
						}
						
						obj.popupAds_ar = popupAds_ar;
					}
					
					//ads
					if(FWDUVPUtils.hasAttribute(popupOrAnnotationChild, "data-ads")){
						adsData_ar = FWDUVPUtils.getChildren(popupOrAnnotationChild);
						ads_ar = [];
					
						var adsChild;
						var tt = adsData_ar.length;
						
						for(var m=0; m<tt; m++){
							var adsObj = {};
							adsChild = adsData_ar[m];
							
							
							adsObj.timeStart = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(adsChild, "data-time-start"));
							if(FWDUVPUtils.hasAttribute(adsChild, "data-add-duration")){
								adsObj.addDuration = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(adsChild, "data-add-duration"));
							}
							
							adsObj.thumbnailSource = FWDUVPUtils.getAttributeValue(adsChild, "data-thumbnail-source");
							if(adsObj.thumbnailSource == "" || adsObj.thumbnailSource == " ") adsObj.thumbnailSource = undefined;
							
							adsObj.timeToHoldAds = FWDUVPUtils.getAttributeValue(adsChild, "data-time-to-hold-ads") || 4;
							adsObj.source = FWDUVPUtils.getAttributeValue(adsChild,"data-source");
							adsObj.link = FWDUVPUtils.getAttributeValue(adsChild,"data-link");
							adsObj.target = FWDUVPUtils.getAttributeValue(adsChild,"data-target");
							
							ads_ar[m] = adsObj
						}
						
						obj.ads_ar = ads_ar
					}
					
					if(FWDUVPUtils.hasAttribute(child, "data-vast-url")){
						obj.ads_ar = undefined;
						obj.vastURL = FWDUVPUtils.getAttributeValue(child, "data-vast-url");
						obj.vastClickTroughTarget = FWDUVPUtils.getAttributeValue(child, "data-vast-clicktrough-target") || "_blank";
						obj.vastLinearStartTime = FWDUVPUtils.getAttributeValue(child, "data-vast-linear-astart-at-time") || "00:00:00";
					}
					
					//cuepoints
					if(FWDUVPUtils.hasAttribute(popupOrAnnotationChild, "data-cuepoints")){
						cuepointsData_ar = FWDUVPUtils.getChildren(popupOrAnnotationChild);
						cuepoints_ar = [];
					
						var cuepointsChild;
						var tt = cuepointsData_ar.length;
						
						for(var m=0; m<tt; m++){
							var cuepointsObj = {};
							cuepointsChild = cuepointsData_ar[m];
							
							cuepointsObj.timeStart = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(cuepointsChild, "data-time-start"));
							cuepointsObj.javascriptCall = FWDUVPUtils.getAttributeValue(cuepointsChild, "data-javascript-call");
							cuepointsObj.isPlayed_bl = false;
							cuepoints_ar[m] = cuepointsObj
						}
						
						obj.cuepoints_ar = cuepoints_ar
					}
					
					//annotation
					if(FWDUVPUtils.hasAttribute(popupOrAnnotationChild, "data-annotations")){
						annotations_ar = FWDUVPUtils.getChildren(popupOrAnnotationChild);
				
						var annotationChild;
						var tt = annotations_ar.length;
						
						for(var m=0; m<tt; m++){
							var annotationObj = {};
							annotationChild = annotations_ar[m];
							
							annotationObj.start = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(annotationChild, "data-start-time"));
							annotationObj.end = FWDUVPUtils.getSecondsFromString(FWDUVPUtils.getAttributeValue(annotationChild, "data-end-time"));
							annotationObj.left = parseInt(FWDUVPUtils.getAttributeValue(annotationChild, "data-left"), 10);
							annotationObj.top = parseInt(FWDUVPUtils.getAttributeValue(annotationChild, "data-top"), 10);
							
							annotationObj.showCloseButton_bl = FWDUVPUtils.getAttributeValue(annotationChild, "data-show-close-button") == "yes" ? true : false; 
							annotationObj.clickSource = FWDUVPUtils.getAttributeValue(annotationChild, "data-click-source");
							annotationObj.clickSourceTarget = FWDUVPUtils.getAttributeValue(annotationChild, "data-click-source-target");
					
							annotationObj.normalStateClass = FWDUVPUtils.getAttributeValue(annotationChild, "data-normal-state-class");
							annotationObj.selectedStateClass = FWDUVPUtils.getAttributeValue(annotationChild, "data-selected-state-class");
							
							annotationObj.content = annotationChild.innerHTML;
							
							annotations_ar[m] = annotationObj
						}
						
						obj.annotations_ar = annotations_ar
					}
				}
				
				//video description
				var descChidren_ar = FWDUVPUtils.getChildren(child);
				var descChild;
				obj.title = "not defined!";
				obj.titleText = "not defined!";
				
				
				for(var k=0; k<descChidren_ar.length; k++){
					descChild = descChidren_ar[k];	
					if(FWDUVPUtils.hasAttribute(descChild, "data-video-short-description")){
						obj.title =  descChild.innerHTML;
						obj.titleText = descChild.textContent;
						obj.titleText = obj.titleText.replace(/^\s+/g, '')
						
					}else if(FWDUVPUtils.hasAttribute(descChild, "data-video-long-description")){
						obj.desc = descChild.innerHTML;
					}
				}
				
			
				gaStr = obj.titleText.split('\n')
				for(var x=0; x<gaStr.length; x++){
				
					if(gaStr[x].length > 2){
						obj.gaStr = gaStr[x];
						break;
					}
				}
				
				
				if(FWDUVPUtils.hasAttribute(child, "data-ads-source")){
					adsObj = {};
					adsObj.source = FWDUVPUtils.getAttributeValue(child, "data-ads-source");
					adsObj.pageToOpen = FWDUVPUtils.getAttributeValue(child, "data-ads-page-to-open-url");
					adsObj.pageTarget = FWDUVPUtils.getAttributeValue(child, "data-ads-page-target") || "_blank";
					adsObj.timeToHoldAds = parseInt(FWDUVPUtils.getAttributeValue(child, "data-time-to-hold-ads")) || 0;
					obj.ads = adsObj;
				}
			
				self.playlist_ar[i] = obj;
			}
			
			
				
			clearTimeout(self.dispatchPlaylistLoadCompleteWidthDelayId_to);
			self.dispatchPlaylistLoadCompleteWidthDelayId_to = setTimeout(function(){
				self.dispatchEvent(FWDUVPData.PLAYLIST_LOAD_COMPLETE);
			}, 50);
	
			self.isDataLoaded_bl = true;
		};
		
		//####################################//
		/* parse folder JSON */
		//####################################//
		this.parseFolderJSON = function(response){
			self.playlist_ar = [];
			var obj;
			var obj_ar = response.folder;
			var counter = 0;
		
			for(var i=0; i<obj_ar.length; i++){
				obj = {};
				obj.videoSource = encodeURI(obj_ar[i]["@attributes"]["data-video-path"]);
				
				obj.videoSource =  obj_ar[i]["@attributes"]["data-video-path"];
				obj.dataPlaybackRate  = obj_ar[i]["@attributes"]["data-playback-rate"];
				obj.startAtVideo = obj_ar[i]["@attributes"]["data-start-at-video"] || 0;
				obj.videoSource = [{source:encodeURI(obj.videoSource)}];
				
				
				obj.thumbSource = encodeURI(obj_ar[i]["@attributes"]["data-thumb-path"]);
				obj.posterSource = encodeURI(obj_ar[i]["@attributes"]["data-poster-path"]);
				obj.downloadPath = encodeURIComponent(obj_ar[i]["@attributes"]["download-path"]);
				
				obj.downloadable = self.showDownloadVideoButton_bl;
				if(self.forceDisableDownloadButtonForFolder_bl) obj.downloadable = false;
				obj.titleText = "...";
				obj.title = "<p style='color:" + self.youtubeAndFolderVideoTitleColor_str + ";margin:0px;padding:0px;margin-top:2px;margin-bottom:4x;line-height:16px;'>...</p>";
					
				obj.titleText = obj_ar[i]["@attributes"]["data-title"];
				obj.title = "<p style='color:" + self.youtubeAndFolderVideoTitleColor_str + ";margin:0px;padding:0px;margin-top:2px;margin-bottom:4x;line-height:16px;'>" + obj_ar[i]["@attributes"]["data-title"] + "</p>";
				obj.desc = undefined;
				
				self.playlist_ar[i] = obj;
				if(i > self.maxPlaylistItems - 1) break;
			}
			
			
			clearTimeout(self.dispatchPlaylistLoadCompleteWidthDelayId_to);
			self.dispatchPlaylistLoadCompleteWidthDelayId_to = setTimeout(function(){
				self.dispatchEvent(FWDUVPData.PLAYLIST_LOAD_COMPLETE);
			}, 50);
	
			self.isDataLoaded_bl = true;
		};
		
		//####################################//
		/* parse xml JSON */
		//####################################//
		this.parseXML = function(response){
			self.playlist_ar = [];
			var obj;
			var obj_ar = response.li;
			var has360Video = false;
			
			if(!obj_ar.length) obj_ar = [obj_ar];
			
			for(var i=0; i<obj_ar.length; i++){
				obj = {};
				
				
				obj.videoSource =  obj_ar[i]["@attributes"]["data-video-source"];
				obj.startAtVideo = obj_ar[i]["@attributes"]["data-start-at-video"] || 0;

				obj.isLive =obj_ar[i]["@attributes"]["data-is-live"];
				obj.atb = obj_ar[i]["@attributes"]["data-use-a-to-b"] == "yes" ? true : false;
				if(!self.useAToB) obj.atb = false;
									
				obj.isPrivate = obj_ar[i]["@attributes"]["data-is-private"]; 
				if(obj.isPrivate == "yes"){
					obj.isPrivate = true;
				}else{
					obj.isPrivate = false;
				}
				
				
				obj.privateVideoPassword_str =  obj_ar[i]["@attributes"]["data-private-video-password"];
				
				obj.startAtTime =  obj_ar[i]["@attributes"]["data-start-at-time"];
				if(obj.startAtTime == "00:00:00" || !FWDUVPUtils.checkTime(obj.startAtTime)) obj.startAtTime = undefined;
				
				obj.stopAtTime =  obj_ar[i]["@attributes"]["data-stop-at-time"];
				if(obj.stopAtTime == "00:00:00" || !FWDUVPUtils.checkTime(obj.stopAtTime)) obj.stopAtTime = undefined;
				
				
				if(obj.videoSource.indexOf("{source:") != -1){
	
					try{
						obj.videoLabels_ar = [];
						obj.videoSource = eval(obj.videoSource);
						
						for(var m=0; m<obj.videoSource.length; m++){
							obj.videoLabels_ar[m] = obj.videoSource[m]["label"];
						}
						
						for(var m=0; m<obj.videoSource.length; m++){
							obj.videoSource[m].source = encodeURI(obj.videoSource[m].source);
						}
						
						for(var m=0; m<obj.videoSource.length; m++){
							obj.videoSource[m].is360 = obj.videoSource[m]['is360'];
							if(obj.videoSource[m].is360 == "yes") obj.videoSource[m].is360 = true;
							if(obj.videoSource[m].is360 == "no") obj.videoSource[m].is360 = false;
							if(obj.videoSource[m].is360 == true) has360Video = true;
						}
								
						obj.videoLabels_ar.reverse();
					}catch(e){
						self.isPlaylistDispatchingError_bl = true;
						showLoadPlaylistErrorId_to = setTimeout(function(){
							self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"Please make sure that the <font color='#ff0000'>data-video-source</font> attribute contains an array of videos at position <font color='#ff0000'>" + (i + 1) + "</font>"});
						}, 50);
						return;
					}
				}else{
					obj.videoSource = [{source:encodeURI(obj.videoSource)}];
				}
				
				obj.subtitleSource =  obj_ar[i]["@attributes"]["data-subtitle-soruce"];
				obj.startAtSubtitle = obj_ar[i]["@attributes"]["data-start-at-subtitle"] || 0;
				if(obj.subtitleSource){
					if(obj.subtitleSource.indexOf("{source:") != -1){
						if(obj.subtitleSource.indexOf("{source:") != -1){
							try{
								obj.subtitleSource = eval(obj.subtitleSource);
							}catch(e){
								self.isPlaylistDispatchingError_bl = true;
								showLoadPlaylistErrorId_to = setTimeout(function(){
									self.dispatchEvent(FWDRVPData.LOAD_ERROR, {text:"Please make sure that the <font color='#ff0000'>data-subtitle-source</font> attribute contains an array of subtitles at position <font color='#ff0000'>" + (i + 1) + "</font>"});
								}, 50);
								return;
							}
							obj.subtitleSource.splice(0,0, {source:"none", label:self.subtitlesOffLabel_str});
							obj.subtitleSource.reverse();
						}
					}else{
						obj.subtitleSource = [{source:obj.subtitleSource}];
					}
				}
				
				obj.dataAdvertisementOnPauseSource  = obj_ar[i]["@attributes"]["data-advertisement-on-pause-source"];
				obj.scrubAtTimeAtFirstPlay =  obj_ar[i]["@attributes"]["data-scrub-at-time-at-first-play"];
				if(obj.scrubAtTimeAtFirstPlay){
					if(/^((?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d$)/g.test(obj.scrubAtTimeAtFirstPlay)){
						obj.scrubAtTimeAtFirstPlay = FWDUVPUtils.getSecondsFromString(obj.scrubAtTimeAtFirstPlay);
					}
				}
				
				
				obj.downloadPath = obj.videoSource[obj.startAtVideo];
				obj.downloadable = obj_ar[i]["@attributes"]["data-downloadable"] == "yes" ? true : false;
				if(obj.videoSource[0]["source"].indexOf(".") == -1) obj.downloadable = false;
				obj.posterSource = encodeURI(obj_ar[i]["@attributes"]["data-poster-source"]);
				obj.thumbSource = obj_ar[i]["@attributes"]["data-thumb-source"];
				obj.title = obj_ar[i]["@attributes"]["data-title"];
				obj.titleText = obj_ar[i]["@attributes"]["data-title"];
				obj.desc = obj_ar[i]["@attributes"]["data-desc"];
				
				obj.gaStr = obj.titleText;
				
				//ads
				if(obj_ar[i]["@attributes"]["data-ads-source"]){
					adsObj = {};
					adsObj.source = obj_ar[i]["@attributes"]["data-ads-source"];
					adsObj.pageToOpen = obj_ar[i]["@attributes"]["data-ads-page-to-open-url"];
					adsObj.pageTarget = obj_ar[i]["@attributes"]["data-ads-page-target"] || "_blank";
					adsObj.timeToHoldAds = obj_ar[i]["@attributes"]["data-time-to-hold-ads"]  || 0;
					obj.ads = adsObj;
				}
				
				//cuepoints
				if(obj_ar[i]["@attributes"]["data-cuepoints"]){
					adsObj = {};
					adsObj.timeStart = obj_ar[i]["@attributes"]["data-time-start"];
					adsObj.javascriptCall = obj_ar[i]["@attributes"]["data-javascript-call"];
					adsObj.isPlayed_bl = false
					obj.cuepoints_ar = adsObj;
				}
			
				self.playlist_ar[i] = obj;
				if(i > self.maxPlaylistItems - 1) break;
			}
			
			
				
			clearTimeout(self.dispatchPlaylistLoadCompleteWidthDelayId_to);
			self.dispatchPlaylistLoadCompleteWidthDelayId_to = setTimeout(function(){
				self.dispatchEvent(FWDUVPData.PLAYLIST_LOAD_COMPLETE);
			}, 50);
	
			self.isDataLoaded_bl = true;
		};
		
		//####################################//
		/* load vast */
		//####################################//
		this.loadVast = function(xmlSource){
			var vastObj = [];
			var tempObj;
			
			self.vastXHR = new XMLHttpRequest();
			self.vastXHR.onreadystatechange = function(e){
				if(self.vastXHR.readyState == 4){
					if(self.vastXHR.status == 200){
						var respObj = FWDUVPUtils.xmlToJson(self.vastXHR.responseXML).VAST;
						//console.log(respObj);
						//console.log("##########################");
						if(!respObj["Ad"]){
							self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"No <font color='#FF0000'> &lt;ad&gt; </font> tag was found in the VAST xml file."});
							return;
						}else{
							if(!respObj["Ad"].length) respObj["Ad"] = [respObj["Ad"]];
							var ad_ar = [];
							for(var i=0; i< respObj["Ad"].length; i++){
								tempObj = {};
								tempObj.id = respObj["Ad"][i]["@attributes"]["id"];
								tempObj.sequence = respObj["Ad"][i]["@attributes"]["sequence"];
								if(!tempObj.sequence) tempObj.sequence = i;
								
								
								
								if(!respObj["Ad"][i]["InLine"]){
									self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"No <font color='#FF0000'> &lt;InLine&gt; </font>tag was found in the VAST xml file."});
									return;
								}
								tempObj["InLine"] = {};
								
								//impression
								tempObj["InLine"]["Impression"] = undefined;
								if(respObj["Ad"][i]["InLine"]["Impression"]){
									if(respObj["Ad"][i]["InLine"]["Impression"]["#cdata-section"]){
										tempObj["InLine"]["Impression"] = respObj["Ad"][i]["InLine"]["Impression"]["#cdata-section"];
									}else{
										tempObj["InLine"]["Impression"] = respObj["Ad"][i]["InLine"]["Impression"]["#text"];
									}
									
								}
								
								if(!respObj["Ad"][i]["InLine"]["Creatives"]["Creative"].length){
									respObj["Ad"][i]["InLine"]["Creatives"]["Creative"] = [respObj["Ad"][i]["InLine"]["Creatives"]["Creative"]]
								}
								
							
								if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"].length){
									for(var j=0; j<respObj["Ad"][i]["InLine"]["Creatives"]["Creative"].length; j++){
										//linear ads
										if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]){
											
											if(!respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["MediaFiles"]["MediaFile"].length){
												respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["MediaFiles"]["MediaFile"] = [respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["MediaFiles"]["MediaFile"]];
											}
											
											
											tempObj["InLine"]["Linear"] = {};
										
											//video source
											var allVideosObj = [];
											for(var k = 0; k<respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["MediaFiles"]["MediaFile"].length; k++){
												allVideosObj.push(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["MediaFiles"]["MediaFile"][k])
											}
											
											var videoSource;
											var correctIndex = 0;
											
											prop:for(var m=0;  m<allVideosObj.length; m++){
												if(window["innerWidth"] >= allVideosObj[m]["@attributes"]["width"]){
													correctIndex = m;
													break prop;
												}
											}
											
											if(allVideosObj[correctIndex]["#cdata-section"]){
												tempObj["InLine"]["Linear"]["videoSource"]  = allVideosObj[correctIndex]["#cdata-section"];
											}else{
												tempObj["InLine"]["Linear"]["videoSource"] = allVideosObj[correctIndex]["#text"];
											}
											
											//duration
											if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["Duration"]){
												if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["Duration"]["#cdata-section"]){
													tempObj["InLine"]["Linear"]["Duration"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["Duration"]["#cdata-section"];
												}else if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["Duration"]["#text"]){
													tempObj["InLine"]["Linear"]["Duration"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["Duration"]["#text"];
												}
											}
											
										
											//skip offset
											tempObj["InLine"]["Linear"]["skipoffset"] = undefined;
											if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["@attributes"]
											   && respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["@attributes"]["skipoffset"]
											){
												tempObj["InLine"]["Linear"]["skipoffset"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["@attributes"]["skipoffset"];
											}
											
											
											if(tempObj["InLine"]["Linear"]["skipoffset"]){
												tempObj["InLine"]["Linear"]["skipoffset"] = tempObj["InLine"]["Linear"]["skipoffset"].substr(0, 8);
												if(tempObj["InLine"]["Linear"]["Duration"] && tempObj["InLine"]["Linear"]["skipoffset"].indexOf("%") != -1){
													var tempSkipOffset =  Math.round(FWDUVPUtils.getSecondsFromString(tempObj["InLine"]["Linear"]["Duration"]) * (tempObj["InLine"]["Linear"]["skipoffset"].substr(0, tempObj["InLine"]["Linear"]["skipoffset"].length -1)/100));
													tempObj["InLine"]["Linear"]["skipoffset"] = FWDUVPUtils.formatTime(tempSkipOffset, true);
												}
												
											}
											if(tempObj["InLine"]["Linear"]["skipoffset"]){
												
												tempObj["InLine"]["Linear"]["skipoffset"] = FWDUVPUtils.getSecondsFromString(tempObj["InLine"]["Linear"]["skipoffset"]);
												if(tempObj["InLine"]["Linear"]["Duration"] && FWDUVPUtils.getSecondsFromString(tempObj["InLine"]["Linear"]["Duration"]) <= tempObj["InLine"]["Linear"]["skipoffset"]){
													tempObj["InLine"]["Linear"]["skipoffset"] = undefined;
												}
											}
											
											
											//tracking events
											tempObj["InLine"]["Linear"]["TrackingEvents"] = undefined;
											if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["TrackingEvents"]["Tracking"]){
												tempObj["InLine"]["Linear"]["TrackingEvents"] = [];
												for(var p=0; p<respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["TrackingEvents"]["Tracking"].length; p++){
													
													
													tempObj["InLine"]["Linear"]["TrackingEvents"].push({
														event:respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["TrackingEvents"]["Tracking"][p]["@attributes"]["event"]
													})
													
													if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["TrackingEvents"]["Tracking"][p]["#cdata-section"]){
														tempObj["InLine"]["Linear"]["TrackingEvents"][p].URI = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["TrackingEvents"]["Tracking"][p]["#cdata-section"];
													}else{
														tempObj["InLine"]["Linear"]["TrackingEvents"][p].URI = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["TrackingEvents"]["Tracking"][p]["#text"];
													}
												}
											}
											
											//video clicks
											if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]){
												if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickThrough"]){
													if(tempObj["InLine"]["Linear"]["ClickThrough"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickThrough"]["#cdata-section"]){
														tempObj["InLine"]["Linear"]["ClickThrough"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickThrough"]["#cdata-section"];
													}else{
														tempObj["InLine"]["Linear"]["ClickThrough"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickThrough"]["#text"]
													}
												}

												if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickTracking"]){
													if(respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickTracking"]["#cdata-section"]){
														tempObj["InLine"]["Linear"]["ClickTracking"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickTracking"]["#cdata-section"];
													}else{
														tempObj["InLine"]["Linear"]["ClickTracking"] = respObj["Ad"][i]["InLine"]["Creatives"]["Creative"][j]["Linear"]["VideoClicks"]["ClickTracking"]["#text"];
													}
												}
											}							
										}
									}
								}	
								vastObj.push(tempObj);
							}
						}
						
						FWDUVPUtils.storArrayBasedOnObjectValue(vastObj, "sequence");
						//console.log(vastObj);

						//create ads object
						var ads=[];
						for(var i=0; i<vastObj.length; i++){
							var adsObj = {};
							adsObj.source = vastObj[i]["InLine"]["Linear"]["videoSource"];
							adsObj.timeStart = FWDUVPUtils.getSecondsFromString(self.playlist_ar[parent.id]["vastLinearStartTime"]);
							if(vastObj[i].startTime) adsObj.timeStart = FWDUVPUtils.getSecondsFromString(vastObj[i].startTime);
							if(vastObj[i]["InLine"]["Linear"]["skipoffset"]) adsObj.timeToHoldAds = vastObj[i]["InLine"]["Linear"]["skipoffset"];
							adsObj.link = vastObj[i]["InLine"]["Linear"]["ClickThrough"];
							if(vastObj[i]["InLine"]["Linear"]["ClickTracking"]) adsObj.ClickTracking = vastObj[i]["InLine"]["Linear"]["ClickTracking"];
							adsObj.target = self.playlist_ar[parent.id]["vastClickTroughTarget"];
							if(vastObj[i]["InLine"]["Impression"]) adsObj.Impression = vastObj[i]["InLine"]["Impression"];
							if(vastObj[i]["InLine"]["Linear"]["TrackingEvents"]) adsObj.TrackingEvents = vastObj[i]["InLine"]["Linear"]["TrackingEvents"];
							ads.push(adsObj);
						}
						
						self.adsSource_ar = ads;
						self.isVastXMLParsed_bl = true;
						self.dispatchEvent(FWDUVPData.VAST_LOADED, {ads:ads});
					}else{
						self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"vast XML file can't be loaded " +  self.vastXHR.statusText});
					}
				}
			};
			
			self.vastXHR.onerror = function(e){
				try{
					if(window.console) console.log(e);
					if(window.console) console.log(e.message);
				}catch(e){};
			};
			
			if(xmlSource.indexOf("http") != -1 || xmlSource.indexOf("https") != -1){
				xmlSource = "https://cors-anywhere.herokuapp.com/" + xmlSource;
			}
			
			self.vastXHR.open("get", xmlSource, true);
			self.vastXHR.send();
		}
		
		
		//####################################//
		/* show error if a required property is not defined */
		//####################################//
		self.showPropertyError = function(error){
			self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"The property called <font color='#FF0000'>" + error + "</font> is not defined."});
		};
		
		
		//####################################//
		/* stop to load current playlist... */
		//####################################//
		this.stopToLoadPlaylist = function(){
			self.closeJsonPLoader();
			try{
				self.scs_el.src = null;
				document.documentElement.removeChild(self.scs_el);
				self.scs_el = null;
			}catch(e){}
			
			if(self.xhr != null){
				try{self.xhr.abort();}catch(e){}
				self.xhr.onreadystatechange = null;
				self.xhr.onerror = null;
				self.xhr = null;
			}
		};
		
		//####################################//
		/* show error if a required property is not defined */
		//####################################//
		self.showPropertyError = function(error){
			self.dispatchEvent(FWDUVPData.LOAD_ERROR, {text:"The property called <font color='#ff0000'>" + error + "</font> is not defined."});
		};
		
		self.init();
	};
	
	/* set prototype */
	FWDUVPData.setPrototype = function(){
		FWDUVPData.prototype = new FWDUVPEventDispatcher();
	};
	
	FWDUVPData.prototype = null;
	
	FWDUVPData.VAST_LOADED = "vastLoaded";
	FWDUVPData.PLAYLIST_LOAD_COMPLETE = "playlistLoadComplete";
	FWDUVPData.PRELOADER_LOAD_DONE = "onPreloaderLoadDone";
	FWDUVPData.LOAD_DONE = "onLoadDone";
	FWDUVPData.LOAD_ERROR = "onLoadError";
	FWDUVPData.IMAGE_LOADED = "onImageLoaded";
	FWDUVPData.SKIN_LOAD_COMPLETE = "onSkinLoadComplete";
	FWDUVPData.SKIN_PROGRESS = "onSkinProgress";
	FWDUVPData.IMAGES_PROGRESS = "onImagesPogress";
	
	window.FWDUVPData = FWDUVPData;
}(window));