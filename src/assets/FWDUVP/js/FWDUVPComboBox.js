/* combo box */
(function (window){
	
	var FWDUVPComboBox = function(parent, props_obj){
		
		var self = this;
		var prototype = FWDUVPComboBox.prototype;
		
		this.categories_ar = props_obj.categories_ar;
		this.buttons_ar = [];
		
		this.mainHolder_do = null;
		this.selector_do = null;
		this.mainButtonsHolder_do = null;
		this.buttonsHolder_do = null;
		
		this.arrowW = props_obj.arrowW;
		this.arrowH = props_obj.arrowH;
		
		
		self.useHEXColorsForSkin_bl = parent.data.useHEXColorsForSkin_bl; 
		self.normalButtonsColor_str = parent.data.normalButtonsColor_str;
		self.selectedButtonsColor_str = parent.data.selectedButtonsColor_str;
	
		this.arrowN_str = props_obj.arrowN_str 
		this.arrowS_str = props_obj.arrowS_str;
		
		this.selectorLabel_str = props_obj.selectorLabel;
		this.selectorBkColorN_str = props_obj.selectorBackgroundNormalColor;
		this.selectorBkColorS_str = props_obj.selectorBackgroundSelectedColor;
		this.selectorTextColorN_str = props_obj.selectorTextNormalColor;
		this.selectorTextColorS_str = props_obj.selectorTextSelectedColor;
		
		this.itemBkColorN_str = props_obj.buttonBackgroundNormalColor;
		this.itemBkColorS_str = props_obj.buttonBackgroundSelectedColor;
		this.itemTextColorN_str = props_obj.buttonTextNormalColor;
		this.itemTextColorS_str = props_obj.buttonTextSelectedColor;
		
		this.scrollBarHandlerFinalY = 0;
		
		
		this.finalX;
		this.finalY;
		this.totalButtons = self.categories_ar.length;
		this.curId = props_obj.startAtPlaylist;
		this.buttonsHolderWidth = 0;
		this.buttonsHolderHeight = 0;
		this.totalWidth = parent.stageWidth;
		this.buttonHeight = props_obj.buttonHeight;
		
		this.totalButtonsHeight = 0;
		this.sapaceBetweenButtons = 1;
		this.thumbnailsFinalY = 0;
		this.vy = 0;
		this.vy2 = 0;
		this.friction = .9;
		
		this.hideMenuTimeOutId_to;
		this.getMaxWidthResizeAndPositionId_to;
		
		this.isShowed_bl = false;
		this.addMouseWheelSupport_bl = parent.data.addMouseWheelSupport_bl;
		this.scollbarSpeedSensitivity = parent.data.scollbarSpeedSensitivity;
		this.isOpened_bl = false;
		this.hasPointerEvent_bl = FWDUVPUtils.hasPointerEvent;
		this.isMobile_bl = FWDUVPUtils.isMobile;
		
		this.init = function(){
			
			//self.setVisible(false);
			self.setOverflow("visible");
			self.setupMainContainers();
			self.setupScrollLogic();
			self.getMaxWidthResizeAndPosition();
			self.mainButtonsHolder_do.setVisible(false);
			self.bk_do.setVisible(false);
			
		};
		
		//#####################################//
		/* setup main containers */
		//####################################//
		this.setupMainContainers = function(){
			var button_do;
			
			self.mainHolder_do = new FWDUVPDisplayObject("div");
			self.mainHolder_do.setOverflow("visible");
			self.addChild(self.mainHolder_do);
			
			self.bk_do = new FWDUVPDisplayObject("div");
			self.bk_do.setY(self.buttonHeight);
			self.bk_do.setBkColor(parent.playlistBackgroundColor_str);
			self.bk_do.setAlpha(0);
			
			self.mainHolder_do.addChild(self.bk_do);
			
			self.mainButtonsHolder_do = new FWDUVPDisplayObject("div");
			self.mainButtonsHolder_do.setY(self.buttonHeight);
			self.mainHolder_do.addChild(self.mainButtonsHolder_do);
			
			if(parent.repeatBackground_bl){
				self.dummyBk_do =  new FWDUVPDisplayObject("div");
				self.dummyBk_do.getStyle().background = "url('" + parent.bkPath_str +  "')";
			}else{
				self.dummyBk_do = new FWDUVPDisplayObject("img");
				var imageBk_img = new Image();
				imageBk_img.src = parent.bkPath_str;
				self.dummyBk_do.setScreen(imageBk_img);
			}
	
			self.dummyBk_do.setHeight(self.buttonHeight);
			self.mainHolder_do.addChild(self.dummyBk_do);
			
			self.buttonsHolder_do = new FWDUVPDisplayObject("div");
			self.mainButtonsHolder_do.addChild(self.buttonsHolder_do);
			
			var selLabel = self.selectorLabel_str;
			
			if (self.selectorLabel_str == "default")
			{
				selLabel = self.categories_ar[self.curId];
			}
			
			
			FWDUVPComboBoxSelector.setPrototype();
			self.selector_do = new FWDUVPComboBoxSelector(
					11,
					6,
					props_obj.arrowN_str,
					props_obj.arrowS_str,
					selLabel,
					self.selectorBkColorN_str,
					self.selectorBkColorS_str,
					self.selectorTextColorN_str,
					self.selectorTextColorS_str,
					self.buttonHeight,
					self.useHEXColorsForSkin_bl,
					self.normalButtonsColor_str,
					self.selectedButtonsColor_str);
			self.mainHolder_do.addChild(self.selector_do);
			self.selector_do.setNormalState(false);
			self.selector_do.addListener(FWDUVPComboBoxSelector.CLICK, self.openMenuHandler);
			
			for(var i=0; i<self.totalButtons; i++){
				FWDUVPComboBoxButton.setPrototype();
				button_do = new FWDUVPComboBoxButton(
						self,
						self.categories_ar[i],
						self.itemBkColorN_str,
						self.itemBkColorS_str,
						self.itemTextColorN_str,
						self.itemTextColorS_str,
						i,
						self.buttonHeight);
				self.buttons_ar[i] = button_do;
				button_do.addListener(FWDUVPComboBoxButton.CLICK, self.buttonOnMouseDownHandler);
				self.buttonsHolder_do.addChild(button_do);
			}
		};
		this.buttonOnMouseDownHandler = function(e){
			self.curId = e.target.id;
			
			//self.setButtonsStateBasedOnId(self.curId);
			clearTimeout(self.hideMenuTimeOutId_to);
			self.hide(true);
			self.selector_do.enable(); 
			if(self.hasPointerEvent_bl){
				window.removeEventListener("pointerdown", self.checkOpenedMenu);
			}else{
				window.removeEventListener("touchstart", self.checkOpenedMenu);
				if(!self.isMobile_bl){
					window.removeEventListener("mousedown", self.checkOpenedMenu);
					window.removeEventListener("mousemove", self.checkOpenedMenu);
				}
			}
			
			self.selector_do.setText(self.buttons_ar[self.curId].label1_str);
			self.dispatchEvent(FWDUVPComboBox.BUTTON_PRESSED, {id:self.curId});
		};
		this.openMenuHandler = function(e){
			if(FWDAnimation.isTweening(self.mainButtonsHolder_do)) return;
			if(self.isShowed_bl){
				self.checkOpenedMenu(e.e, true);
			}else{
				self.selector_do.disable();
				self.show(true);
				self.startToCheckOpenedMenu();
				self.dispatchEvent(FWDUVPComboBox.OPEN);
			}
		};
		
		//#######################################//
		/* Disable or enable buttons */
		//#######################################//
		this.setButtonsStateBasedOnId = function(id){
			self.curId = id;
			for(var i=0; i<self.totalButtons; i++){
				button_do = self.buttons_ar[i];
				if(i == self.curId){
					button_do.disable();
				}else{
					button_do.enable();
				}
			}
			self.selector_do.setText(self.buttons_ar[self.curId].label1_str);
			if(self.scrHandler_do){
				self.updateScrollBarSizeActiveAndDeactivate();
				self.updateScrollBarHandlerAndContent(false, true);
			}else{
				self.thumbnailsFinalY = 0;
			}
		};
		
		this.setValue = function(id)
		{
			self.curId = id;
			self.setButtonsStateBasedOnId();
		};
		//#######################################//
		/* Start to check if mouse is over menu */
		//#######################################//
		this.startToCheckOpenedMenu = function(e){
			if(self.hasPointerEvent_bl){
				window.addEventListener("pointerdown", self.checkOpenedMenu);
			}else{
				window.addEventListener("touchstart", self.checkOpenedMenu);
				if(!self.isMobile_bl){
					window.addEventListener("mousedown", self.checkOpenedMenu);
				}
			}
		};
		this.checkOpenedMenu = function(e, forceHide){
			if(e.preventDefault) e.preventDefault();
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);	
			var hideDelay  = 1000;
			if(e.type == "mousedown") hideDelay = 0;
			if(!FWDUVPUtils.hitTest(self.screen, viewportMouseCoordinates.screenX, viewportMouseCoordinates.screenY) &&
			   !FWDUVPUtils.hitTest(self.mainButtonsHolder_do.screen, viewportMouseCoordinates.screenX, viewportMouseCoordinates.screenY)
			   || forceHide
			){
				
				self.hide(true);
				self.selector_do.enable();
			
				if(self.hasPointerEvent_bl){
					window.removeEventListener("pointerdown", self.checkOpenedMenu);
				}else{
					if(!self.isMobile_bl){
					window.removeEventListener("touchstart", self.checkOpenedMenu);
					window.removeEventListener("mousemove", self.checkOpenedMenu);
					}
					window.removeEventListener("mousedown", self.checkOpenedMenu);
				}
			}else{
				clearTimeout(self.hideMenuTimeOutId_to);
			}
		};
		
		
		//########################################//
		/* Get max width and position */
		//#######################################//
		self.getMaxWidthResizeAndPosition = function(){
			
			var button_do;
			var finalX;
			var finalY;
			self.totalButtonsHeight = 0;		
			
			for(var i=0; i<self.totalButtons; i++){
				button_do = self.buttons_ar[i];
				button_do.setY(1 + (i * (button_do.totalHeight + self.sapaceBetweenButtons)));
				if(self.allowToScrollAndScrollBarIsActive_bl && !self.isMobile_bl){
					self.totalWidth = parent.stageWidth - 6;
				}else{
					self.totalWidth = parent.stageWidth;
				}
				
				button_do.totalWidth =  self.totalWidth;
				button_do.setWidth(self.totalWidth);
				button_do.centerText();
			}
		
			self.totalButtonsHeight = button_do.getY() + button_do.totalHeight - self.sapaceBetweenButtons;
			
			self.dummyBk_do.setWidth(self.totalWidth + 6);
			self.setWidth(self.totalWidth);
			self.setHeight(self.buttonHeight);
			self.selector_do.totalWidth =  self.totalWidth + 6;
			self.selector_do.setWidth(self.totalWidth + 6);
			self.selector_do.centerText();
			self.buttonsHolder_do.setWidth(self.totalWidth);
			self.buttonsHolder_do.setHeight(self.totalButtonsHeight);
			
		};
		//######################################//
		/* position */
		//######################################//
		this.position = function(){		
			if (FWDUVPUtils.isAndroid){
				self.setX(Math.floor(self.finalX));
				self.setY(Math.floor(self.finalY-1));
				setTimeout(self.poscombo-box, 100);
			}else{
				self.poscombo-box();
			}
		};
			
		this.resizeAndPosition = function(){
			self.stageWidth = parent.stageWidth;
			self.stageHeight = parent.stageHeight;
			self.bk_do.setWidth(self.stageWidth);
			self.bk_do.setHeight(self.stageHeight - parent.removeFromThumbsHolderHeight + 3);
			self.mainButtonsHolder_do.setWidth(self.stageWidth);
			self.mainButtonsHolder_do.setHeight(self.stageHeight - parent.removeFromThumbsHolderHeight + 3);
			
			if(self.totalButtonsHeight > self.mainButtonsHolder_do.h){
				self.allowToScrollAndScrollBarIsActive_bl = true;
			}else{
				self.allowToScrollAndScrollBarIsActive_bl = false;
			}
			
			if(!self.allowToScrollAndScrollBarIsActive_bl && self.scrMainHolder_do){
				self.scrMainHolder_do.setVisible(false);
			}else if(self.allowToScrollAndScrollBarIsActive_bl && self.scrMainHolder_do && self.isShowed_bl){
				self.scrMainHolder_do.setVisible(true);
			}
			
			if(self.scrHandler_do) self.updateScrollBarSizeActiveAndDeactivate();
			this.getMaxWidthResizeAndPosition();
			self.updateScrollBarHandlerAndContent();
		};
		
		this.hide = function(animate, overwrite){
			if(!self.isShowed_bl && !overwrite) return;
			FWDAnimation.killTweensOf(this);
			self.isShowed_bl = false;
			FWDAnimation.killTweensOf(self.mainButtonsHolder_do);
			FWDAnimation.killTweensOf(self.bk_do);
			if(animate){
				FWDAnimation.to(self.mainButtonsHolder_do, .8, {y:-self.totalButtonsHeight, ease:Expo.easeInOut, onComplete:self.hideComplete});	
				FWDAnimation.to(self.bk_do, .8, {alpha:0});	
			}else{
				self.mainButtonsHolder_do.setY(self.buttonHeight - self.totalButtonsHeight);
				self.bk_do.setAlpha(0);
				self.setHeight(self.buttonHeight);
			}
		};
		
		this.hideComplete = function(){
			self.mainButtonsHolder_do.setVisible(false);
			self.bk_do.setVisible(false);
		}
		this.show = function(animate, overwrite){
			if(self.isShowed_bl && !overwrite) return;
			FWDAnimation.killTweensOf(this);
			self.mainButtonsHolder_do.setY(- self.totalButtonsHeight);
			self.isShowed_bl = true;
			self.mainButtonsHolder_do.setVisible(true);
			self.bk_do.setVisible(true);
			self.resizeAndPosition();
			FWDAnimation.killTweensOf(self.mainButtonsHolder_do);
			FWDAnimation.killTweensOf(self.bk_do);
			if(self.scrMainHolder_do && self.allowToScrollAndScrollBarIsActive_bl) self.scrMainHolder_do.setVisible(true);
			if(animate){
				FWDAnimation.to(self.bk_do, .8, {alpha:1});
				FWDAnimation.to(self.mainButtonsHolder_do, .8, {y:self.buttonHeight, ease:Expo.easeInOut});
			}else{
				self.bk_do.setAlpha(1);
				self.mainButtonsHolder_do.setY(self.buttonHeight);
			}
		};
		this.setupScrollLogic = function(){
			
			self.setupMobileScrollbar();
			if(!self.isMobile_bl) self.setupScrollbar();
			if(self.addMouseWheelSupport_bl) self.addMouseWheelSupport();
		};
		
		//##########################################//
		/* setup mobile scrollbar */
		//##########################################//
		this.setupMobileScrollbar = function(){
			if(self.hasPointerEvent_bl){
				self.mainButtonsHolder_do.screen.addEventListener("pointerdown", self.scrollBarTouchStartHandler);
			}else{
				self.mainButtonsHolder_do.screen.addEventListener("touchstart", self.scrollBarTouchStartHandler);
			}
			//self.mainButtonsHolder_do.screen.addEventListener("mousedown", self.scrollBarTouchStartHandler);
			if(self.isMobile_bl) self.updateMobileScrollBarId_int = setInterval(self.updateMobileScrollBar, 16);
		};
		this.scrollBarTouchStartHandler = function(e){
			if(e.preventDefault) e.preventDefault();
			self.isScrollingOnMove_bl = false;
			FWDAnimation.killTweensOf(self.buttonsHolder_do);
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);		
			self.isDragging_bl = true;
			self.lastPresedY = viewportMouseCoordinates.screenY;
			self.checkLastPresedY = viewportMouseCoordinates.screenY;
			if(self.hasPointerEvent_bl){
				window.addEventListener("pointerup", self.scrollBarTouchEndHandler);
				window.addEventListener("pointermove", self.scrollBarTouchMoveHandler);
			}else{
				window.addEventListener("touchend", self.scrollBarTouchEndHandler);
				window.addEventListener("touchmove", self.scrollBarTouchMoveHandler);
			}
			window.addEventListener("mouseup", self.scrollBarTouchEndHandler);
			window.addEventListener("mousemove", self.scrollBarTouchMoveHandler);
			clearInterval(self.updateMoveMobileScrollbarId_int);
			self.updateMoveMobileScrollbarId_int = setInterval(self.updateMoveMobileScrollbar, 20);
		};
		this.scrollBarTouchMoveHandler = function(e){
			if(e.preventDefault) e.preventDefault();
			e.stopImmediatePropagation();
			if(self.totalButtonsHeight < self.mainButtonsHolder_do.h) return;
			parent.parent.showDisable();
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);	
			if(viewportMouseCoordinates.screenY >= self.checkLastPresedY + 6 || viewportMouseCoordinates.screenY <= self.checkLastPresedY - 6) self.isScrollingOnMove_bl = true;
			var toAdd = viewportMouseCoordinates.screenY - self.lastPresedY;
			self.thumbnailsFinalY += toAdd;
			self.thumbnailsFinalY = Math.round(self.thumbnailsFinalY);
			self.lastPresedY = viewportMouseCoordinates.screenY;
			self.vy = toAdd  * 2;
			
			if(!self.isMobile){
				if(self.thumbnailsFinalY > 0){
					self.thumbnailsFinalY = 0;
				}else if(self.thumbnailsFinalY < self.mainButtonsHolder_do.h - self.totalButtonsHeight){
					self.thumbnailsFinalY = self.mainButtonsHolder_do.h - self.totalButtonsHeight;
				}	
				
				
				var percentScrolled = Math.max(0,(self.thumbnailsFinalY/(self.mainButtonsHolder_do.h - self.totalButtonsHeight)));
				
				if(self.scrMainHolder_do){
					self.scrollBarHandlerFinalY = Math.round((self.scrMainHolder_do.h - self.scrHandler_do.h) * percentScrolled);
					
					if(self.scrollBarHandlerFinalY < 0){
						self.scrollBarHandlerFinalY = 0;
					}else if(self.scrollBarHandlerFinalY > self.scrMainHolder_do.h - self.scrHandler_do.h - 1){
						self.scrollBarHandlerFinalY = self.scrMainHolder_do.h - self.scrHandler_do.h - 1;
					}
					
					FWDAnimation.killTweensOf(self.scrHandler_do);
					FWDAnimation.killTweensOf(self.scrHandlerLines_do);
					
						self.scrHandler_do.setY(self.scrollBarHandlerFinalY);
						self.scrHandlerLines_do.setY(self.scrollBarHandlerFinalY + parseInt((self.scrHandler_do.h - self.scrHandlerLinesN_do.h)/2));
					
				}
			}
		};
		this.scrollBarTouchEndHandler = function(e){
			self.isDragging_bl = false;
			clearInterval(self.updateMoveMobileScrollbarId_int);
			clearTimeout(self.disableOnMoveId_to);
			self.disableOnMoveId_to = setTimeout(function(){
				parent.parent.hideDisable();
			},100);
			if(self.hasPointerEvent_bl){
				window.removeEventListener("pointerup", self.scrollBarTouchEndHandler);
				window.removeEventListener("pointermove", self.scrollBarTouchMoveHandler);
			}else{
				window.removeEventListener("touchend", self.scrollBarTouchEndHandler);
				window.removeEventListener("touchmove", self.scrollBarTouchMoveHandler);
			}
			window.removeEventListener("mousemove", self.scrollBarTouchMoveHandler);
		};
		this.updateMoveMobileScrollbar = function(){
			self.buttonsHolder_do.setY(self.thumbnailsFinalY);
		};
		this.updateMobileScrollBar = function(animate){
			if(!self.isDragging_bl){
				//if(self.isSearched_bl){
					//self.thumbnailsFinalY = 0;
					//self.buttonsHolder_do.setY(Math.round(self.thumbnailsFinalY));
					//return;
				//}
				if(self.totalButtonsHeight < self.mainButtonsHolder_do.h) self.thumbnailsFinalY = 0.01;
				self.vy *= self.friction;
				self.thumbnailsFinalY += self.vy;	
				if(self.thumbnailsFinalY > 0){
					self.vy2 = (0 - self.thumbnailsFinalY) * .3;
					self.vy *= self.friction;
					self.thumbnailsFinalY += self.vy2;
				}else if(self.thumbnailsFinalY < self.mainButtonsHolder_do.h - self.totalButtonsHeight){
					self.vy2 = (self.mainButtonsHolder_do.h - self.totalButtonsHeight - self.thumbnailsFinalY) * .3;
					self.vy *= self.friction;
					self.thumbnailsFinalY += self.vy2;
				}
				self.buttonsHolder_do.setY(Math.round(self.thumbnailsFinalY));
			}
		};
		//#################################//
		/* setup mouse scrollbar */
		//#################################//
		this.setupScrollbar = function(){
			self.scrMainHolder_do = new FWDUVPDisplayObject("div");
			self.scrMainHolder_do.setVisible(false);
			
			self.scrMainHolder_do.setWidth(parent.scrWidth);
			
			//track
			self.scrTrack_do = new FWDUVPDisplayObject("div");
			self.scrTrack_do.setWidth(parent.scrWidth);
			
			var scrBkTop_img = new Image();
			scrBkTop_img.src = parent.scrBkTop_img.src;
			self.scrTrackTop_do = new FWDUVPDisplayObject("img");
			self.scrTrackTop_do.setWidth(parent.scrTrackTop_do.w);
			self.scrTrackTop_do.setHeight(parent.scrTrackTop_do.h);
			self.scrTrackTop_do.setScreen(scrBkTop_img);
			
			
			self.scrTrackMiddle_do = new FWDUVPDisplayObject("div");
			self.scrTrackMiddle_do.getStyle().background = "url('" + parent.data.scrBkMiddlePath_str + "')";
			self.scrTrackMiddle_do.setWidth(parent.scrWidth);
			self.scrTrackMiddle_do.setY(self.scrTrackTop_do.h);
		
			var scrTrackBottomImage_img = new Image();
			scrTrackBottomImage_img.src = parent.data.scrBkBottomPath_str;
			self.scrTrackBottom_do = new FWDUVPDisplayObject("img");
			self.scrTrackBottom_do.setScreen(scrTrackBottomImage_img);
			self.scrTrackBottom_do.setWidth(self.scrTrackTop_do.w);
			self.scrTrackBottom_do.setHeight(self.scrTrackTop_do.h);
			
			//handler
			self.scrHandler_do = new FWDUVPDisplayObject("div");
			self.scrHandler_do.setWidth(parent.scrWidth);
			
		
			self.scrDragTop_img = new Image();
			self.scrDragTop_img.src = parent.scrDragTop_img.src;
			self.scrDragTop_img.width = parent.scrDragTop_img.width;
			self.scrDragTop_img.height = parent.scrDragTop_img.height;
			
			self.scrHandlerTop_do = new FWDUVPDisplayObject("img");
			if(self.useHEXColorsForSkin_bl){
				self.scrHandlerTop_do = new FWDUVPDisplayObject("div");
				self.scrHandlerTop_do.setWidth(self.scrDragTop_img.width);
				self.scrHandlerTop_do.setHeight(self.scrDragTop_img.height);
				self.mainScrubberDragTop_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.scrDragTop_img, self.normalButtonsColor_str).canvas;
				self.scrHandlerTop_do.screen.appendChild(self.mainScrubberDragTop_canvas);	
			}else{
				self.scrHandlerTop_do = new FWDUVPDisplayObject("img");
				self.scrHandlerTop_do.setScreen(self.scrDragTop_img);
			}
			
			
			self.scrHandlerMiddle_do = new FWDUVPDisplayObject("div");
			self.middleImage = new Image();
			self.middleImage.src = parent.data.scrDragMiddlePath_str;
			if(self.useHEXColorsForSkin_bl){
				self.middleImage.onload = function(){
					self.scrubberDragMiddle_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.middleImage, self.normalButtonsColor_str, true);
					self.scrubberDragImage_img = self.scrubberDragMiddle_canvas.image;
					self.scrHandlerMiddle_do.getStyle().background = "url('" + self.scrubberDragImage_img.src + "') repeat-y";
				}
			}else{
				self.scrHandlerMiddle_do.getStyle().background = "url('" + parent.data.scrDragMiddlePath_str + "')";
			}
	
			self.scrHandlerMiddle_do.setWidth(parent.scrWidth);
			self.scrHandlerMiddle_do.setY(self.scrHandlerTop_do.h);
			
			self.scrHandlerBottom_do = new FWDUVPDisplayObject("div");
			self.bottomImage = new Image();
			self.bottomImage.src = parent.data.scrDragMiddlePath_str;
			if(self.useHEXColorsForSkin_bl){
				self.bottomImage.onload = function(){
					self.scrubberDragBottom_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.bottomImage, self.normalButtonsColor_str, true);
					self.scrubberDragBottomImage_img = self.scrubberDragBottom_canvas.image;
					self.scrHandlerBottom_do.getStyle().background = "url('" + self.scrubberDragBottomImage_img.src + "') repeat-y";
					
				}
			}else{
				self.scrHandlerBottom_do.getStyle().background = "url('" + parent.data.scrDragBottomPath_str + "')";
			}
			self.scrHandlerBottom_do.setWidth(parent.scrWidth);
			self.scrHandlerBottom_do.setY(self.scrHandlerTop_do.h);
			
			
			self.scrHandlerBottom_do.setWidth(self.scrHandlerTop_do.w);
			self.scrHandlerBottom_do.setHeight(self.scrHandlerTop_do.h);
			self.scrHandler_do.setButtonMode(true);
			
			self.scrLinesN_img = new Image();
			self.scrLinesN_img.src = parent.scrLinesN_img.src;
			self.scrLinesN_img.width = parent.scrLinesN_img.width;
			self.scrLinesN_img.height = parent.scrLinesN_img.height;
			
			if(self.useHEXColorsForSkin_bl){
				self.scrHandlerLinesN_do = new FWDUVPDisplayObject("div");
				self.scrHandlerLinesN_do.setWidth(self.scrLinesN_img.width);
				self.scrHandlerLinesN_do.setHeight(self.scrLinesN_img.height);
				self.mainhandlerN_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.scrLinesN_img, self.selectedButtonsColor_str).canvas;
				self.scrHandlerLinesN_do.screen.appendChild(self.mainhandlerN_canvas);	
			}else{
				self.scrHandlerLinesN_do = new FWDUVPDisplayObject("img");
				self.scrHandlerLinesN_do.setScreen(self.scrLinesN_img);
			}
			
			
			self.scrHandlerLinesS_img = new Image();
			self.scrHandlerLinesS_img.src = parent.data.scrLinesSPath_str;
			if(self.useHEXColorsForSkin_bl){
				self.scrHandlerLinesS_do = new FWDUVPDisplayObject("div");
				self.scrHandlerLinesS_img.onload = function(){
					self.scrHandlerLinesS_do.setWidth(self.scrHandlerLinesN_do.w);
					self.scrHandlerLinesS_do.setHeight(self.scrHandlerLinesN_do.h);
					self.scrubberLines_s_canvas = FWDUVPUtils.getCanvasWithModifiedColor(self.scrHandlerLinesS_img, self.selectedButtonsColor_str, true);
					self.scrubbelinesSImage_img = self.scrubberLines_s_canvas.image;
					self.scrHandlerLinesS_do.getStyle().background = "url('" + self.scrubbelinesSImage_img.src + "') repeat-y";
				}
			}else{
				self.scrHandlerLinesS_do = new FWDUVPDisplayObject("img");
				//self.scrHandlerMiddle_do.getStyle().background = "url('" + data.scrLinesSPath_str + "')";
				self.scrHandlerLinesS_do.setScreen(self.scrHandlerLinesS_img);
				self.scrHandlerLinesS_do.setWidth(self.scrHandlerLinesN_do.w);
				self.scrHandlerLinesS_do.setHeight(self.scrHandlerLinesN_do.h);
			}
			self.scrHandlerLinesS_do.setAlpha(0);
			
			
			self.scrHandlerLines_do = new FWDUVPDisplayObject("div");
			self.scrHandlerLines_do.setWidth(self.scrHandlerLinesN_do.w);
			self.scrHandlerLines_do.setHeight(self.scrHandlerLinesN_do.h);
			self.scrHandlerLines_do.setButtonMode(true);
				
			self.scrTrack_do.addChild(self.scrTrackTop_do);
			self.scrTrack_do.addChild(self.scrTrackMiddle_do);
			self.scrTrack_do.addChild(self.scrTrackBottom_do);
			self.scrHandler_do.addChild(self.scrHandlerTop_do);
			self.scrHandler_do.addChild(self.scrHandlerMiddle_do);
			self.scrHandler_do.addChild(self.scrHandlerBottom_do);
			self.scrHandlerLines_do.addChild(self.scrHandlerLinesN_do);
			self.scrHandlerLines_do.addChild(self.scrHandlerLinesS_do);
			self.scrMainHolder_do.addChild(self.scrTrack_do);
			self.scrMainHolder_do.addChild(self.scrHandler_do);
			self.scrMainHolder_do.addChild(self.scrHandlerLines_do);
			self.mainButtonsHolder_do.addChild(self.scrMainHolder_do);
			
			
			if(self.scrHandler_do.screen.addEventListener){
				self.scrHandler_do.screen.addEventListener("mouseover", self.scrollBarHandlerOnMouseOver);
				self.scrHandler_do.screen.addEventListener("mouseout", self.scrollBarHandlerOnMouseOut);
				self.scrHandler_do.screen.addEventListener("mousedown", self.scrollBarHandlerOnMouseDown);
				self.scrHandlerLines_do.screen.addEventListener("mouseover", self.scrollBarHandlerOnMouseOver);
				self.scrHandlerLines_do.screen.addEventListener("mouseout", self.scrollBarHandlerOnMouseOut);
				self.scrHandlerLines_do.screen.addEventListener("mousedown", self.scrollBarHandlerOnMouseDown);
			}else if(self.scrHandler_do.screen.attachEvent){
				self.scrHandler_do.screen.attachEvent("onmouseover", self.scrollBarHandlerOnMouseOver);
				self.scrHandler_do.screen.attachEvent("onmouseout", self.scrollBarHandlerOnMouseOut);
				self.scrHandler_do.screen.attachEvent("onmousedown", self.scrollBarHandlerOnMouseDown);
				self.scrHandlerLines_do.screen.attachEvent("onmouseover", self.scrollBarHandlerOnMouseOver);
				self.scrHandlerLines_do.screen.attachEvent("onmouseout", self.scrollBarHandlerOnMouseOut);
				self.scrHandlerLines_do.screen.attachEvent("onmousedown", self.scrollBarHandlerOnMouseDown);
			}
		};
		
		this.scrollBarHandlerOnMouseOver = function(e){
			if(!self.allowToScrollAndScrollBarIsActive_bl) return; 
			FWDAnimation.killTweensOf(self.scrHandlerLinesN_do);
			FWDAnimation.killTweensOf(self.scrHandlerLinesS_do);
			FWDAnimation.to(self.scrHandlerLinesN_do, .8, {alpha:0, ease:Expo.easeOut});
			FWDAnimation.to(self.scrHandlerLinesS_do, .8, {alpha:1, ease:Expo.easeOut});
		};
		
		this.scrollBarHandlerOnMouseOut = function(e){
			if(self.isDragging_bl || !self.allowToScrollAndScrollBarIsActive_bl) return;
			FWDAnimation.killTweensOf(self.scrHandlerLinesN_do);
			FWDAnimation.killTweensOf(self.scrHandlerLinesS_do);
			FWDAnimation.to(self.scrHandlerLinesN_do, .8, {alpha:1, ease:Expo.easeOut});
			FWDAnimation.to(self.scrHandlerLinesS_do, .8, {alpha:0, ease:Expo.easeOut});
		};
		
		this.scrollBarHandlerOnMouseDown = function(e){
			if(!self.allowToScrollAndScrollBarIsActive_bl) return;
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);		
			self.isDragging_bl = true;
			self.yPositionOnPress = self.scrHandler_do.y;
			self.lastPresedY = viewportMouseCoordinates.screenY;
			FWDAnimation.killTweensOf(self.scrHandler_do);
			parent.parent.showDisable();
			
			if(window.addEventListener){
				window.addEventListener("mousemove", self.scrollBarHandlerMoveHandler);
				window.addEventListener("mouseup", self.scrollBarHandlerEndHandler);	
			}else if(document.attachEvent){
				document.attachEvent("onmousemove", self.scrollBarHandlerMoveHandler);
				document.attachEvent("onmouseup", self.scrollBarHandlerEndHandler);
			}
		};
		
		this.scrollBarHandlerMoveHandler = function(e){
			if(e.preventDefault) e.preventDefault();
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);	
			var linesY = self.scrollBarHandlerFinalY + parseInt((self.scrHandler_do.h - self.scrHandlerLines_do.h)/2);
	
			self.scrollBarHandlerFinalY = Math.round(self.yPositionOnPress + viewportMouseCoordinates.screenY - self.lastPresedY);
			if(self.scrollBarHandlerFinalY >= self.scrTrack_do.h - self.scrHandler_do.h){
				self.scrollBarHandlerFinalY = self.scrTrack_do.h -  self.scrHandler_do.h;
			}else if(self.scrollBarHandlerFinalY <= 0){
				self.scrollBarHandlerFinalY = 0;
			}
			
			self.scrHandler_do.setY(self.scrollBarHandlerFinalY);
			FWDAnimation.killTweensOf(self.scrHandler_do);
			FWDAnimation.to(self.scrHandlerLines_do, .8, {y:linesY, ease:Quart.easeOut});
			self.updateScrollBarHandlerAndContent(true);
		};
		
		self.scrollBarHandlerEndHandler = function(e){
			var viewportMouseCoordinates = FWDUVPUtils.getViewportMouseCoordinates(e);	
			self.isDragging_bl = false;
			
			if(!FWDUVPUtils.hitTest(self.scrHandler_do.screen, viewportMouseCoordinates.screenX, viewportMouseCoordinates.screenY)){
				FWDAnimation.killTweensOf(self.scrHandlerLinesN_do);
				FWDAnimation.killTweensOf(self.scrHandlerLinesS_do);
				FWDAnimation.to(self.scrHandlerLinesN_do, .8, {alpha:1, ease:Expo.easeOut});
				FWDAnimation.to(self.scrHandlerLinesS_do, .8, {alpha:0, ease:Expo.easeOut});
			}
			
			parent.parent.hideDisable();
			FWDAnimation.killTweensOf(self.scrHandler_do);
			FWDAnimation.to(self.scrHandler_do, .4, {y:self.scrollBarHandlerFinalY, ease:Quart.easeOut});
			
			if(window.removeEventListener){
				window.removeEventListener("mousemove", self.scrollBarHandlerMoveHandler);
				window.removeEventListener("mouseup", self.scrollBarHandlerEndHandler);	
			}else if(document.detachEvent){
				document.detachEvent("onmousemove", self.scrollBarHandlerMoveHandler);
				document.detachEvent("onmouseup", self.scrollBarHandlerEndHandler);
			}
		};
		
		this.updateScrollBarSizeActiveAndDeactivate = function(){
			if(self.disableForAWhileAfterThumbClick_bl) return;
		
			if(self.allowToScrollAndScrollBarIsActive_bl){
				self.allowToScrollAndScrollBarIsActive_bl = true;
				self.scrMainHolder_do.setX(self.stageWidth - self.scrMainHolder_do.w);
				self.scrMainHolder_do.setHeight(self.mainButtonsHolder_do.h);
				self.scrTrack_do.setHeight(self.scrMainHolder_do.h);
				self.scrTrackMiddle_do.setHeight(self.scrTrack_do.h - (self.scrTrackTop_do.h * 2));
				self.scrTrackBottom_do.setY(self.scrTrackMiddle_do.y + self.scrTrackMiddle_do.h);
				self.scrMainHolder_do.setAlpha(1);
				self.scrHandler_do.setButtonMode(true);
				self.scrHandlerLines_do.setButtonMode(true);
			}else{
				self.allowToScrollAndScrollBarIsActive_bl = false;
				self.scrMainHolder_do.setX(self.stageWidth - self.scrMainHolder_do.w);
				self.scrMainHolder_do.setHeight(self.mainButtonsHolder_do.h);
				self.scrTrack_do.setHeight(self.scrMainHolder_do.h);
				self.scrTrackMiddle_do.setHeight(self.scrTrack_do.h - (self.scrTrackTop_do.h * 2));
				self.scrTrackBottom_do.setY(self.scrTrackMiddle_do.y + self.scrTrackMiddle_do.h);
				self.scrMainHolder_do.setAlpha(.5);
				self.scrHandler_do.setY(0);
				self.scrHandler_do.setButtonMode(false);
				self.scrHandlerLines_do.setButtonMode(false);
			}
			
			self.scrHandler_do.setHeight(Math.max(120, Math.round(Math.min(1,(self.scrMainHolder_do.h/self.totalButtonsHeight)) * self.scrMainHolder_do.h)));
			self.scrHandlerMiddle_do.setHeight(self.scrHandler_do.h - (self.scrHandlerTop_do.h * 2));
			self.scrHandlerBottom_do.setY(self.scrHandlerMiddle_do.y + self.scrHandlerMiddle_do.h);
			FWDAnimation.killTweensOf(self.scrHandlerLines_do);
			self.scrHandlerLines_do.setY(self.scrollBarHandlerFinalY + parseInt((self.scrHandler_do.h - self.scrHandlerLines_do.h)/2));
			self.scrHandlerBottom_do.setY(self.scrHandler_do.h - self.scrHandlerBottom_do.h);
		};
		
		//###########################################//
		/* Add mousewheel support */
		//###########################################//
		this.addMouseWheelSupport = function(){
			if(self.screen.addEventListener){
				self.screen.addEventListener('DOMMouseScroll', self.mouseWheelHandler);
				self.screen.addEventListener ("mousewheel", self.mouseWheelHandler);
			}else if(self.screen.attachEvent){
				self.screen.attachEvent('onmousewheel', self.mouseWheelHandler);
			}
		};
	
		self.mouseWheelHandler = function(e){
			if(e.preventDefault) e.preventDefault();
			if(self.disableMouseWheel_bl || self.isDragging_bl) return false;
			var dir = e.detail || e.wheelDelta;	
			if(e.wheelDelta) dir *= -1;
			//if(FWDUVPUtils.isOpera) dir *= -1;
			if(dir > 0){
				self.scrollBarHandlerFinalY += Math.round((160 * self.scollbarSpeedSensitivity)  * (self.mainButtonsHolder_do.h/self.totalButtonsHeight));
			}else if(dir < 0){
				self.scrollBarHandlerFinalY -= Math.round((160 * self.scollbarSpeedSensitivity)  * (self.mainButtonsHolder_do.h/self.totalButtonsHeight));
			}
			
			if(self.scrollBarHandlerFinalY >= self.scrTrack_do.h - self.scrHandler_do.h){
				self.scrollBarHandlerFinalY = self.scrTrack_do.h -  self.scrHandler_do.h;
			}else if(self.scrollBarHandlerFinalY <= 0){
				self.scrollBarHandlerFinalY = 0;
			}
			
			var linesY = self.scrollBarHandlerFinalY + parseInt((self.scrHandler_do.h - self.scrHandlerLines_do.h)/2);
			FWDAnimation.killTweensOf(self.scrHandler_do);
			FWDAnimation.killTweensOf(self.scrHandlerLines_do);
			FWDAnimation.to(self.scrHandlerLines_do, .8, {y:linesY, ease:Quart.easeOut});
			FWDAnimation.to(self.scrHandler_do, .5, {y:self.scrollBarHandlerFinalY, ease:Quart.easeOut});
			self.isDragging_bl = true;
			self.updateScrollBarHandlerAndContent(true);
			self.isDragging_bl = false;
		
			if(e.preventDefault){
				e.preventDefault();
			}else{
				return false;
			}
		};
		
		this.updateScrollBarHandlerAndContent = function(animate, overwrite){
			if(self.disableForAWhileAfterThumbClick_bl) return;
			if(!self.allowToScrollAndScrollBarIsActive_bl && !overwrite) return;
			var percentScrolled = 0;
			var thumb;
			if(self.isDragging_bl && !self.isMobile_bl){
				percentScrolled = (self.scrollBarHandlerFinalY/(self.scrMainHolder_do.h - self.scrHandler_do.h));
				if(percentScrolled == "Infinity"){
					percentScrolled = 0;
				}else if(percentScrolled >= 1){
					scrollPercent = 1;
				}
				self.thumbnailsFinalY = Math.round(percentScrolled * (self.totalButtonsHeight - self.mainButtonsHolder_do.h)) * -1;
			}else{
				percentScrolled = self.curId/(self.totalButtons - 1);
				self.thumbnailsFinalY = Math.min(0, Math.round(percentScrolled * (self.totalButtonsHeight - self.mainButtonsHolder_do.h)) * -1);
				if(self.scrMainHolder_do){
					self.scrollBarHandlerFinalY = Math.round((self.scrMainHolder_do.h - self.scrHandler_do.h) * percentScrolled);
					if(self.scrollBarHandlerFinalY < 0){
						self.scrollBarHandlerFinalY = 0;
					}else if(self.scrollBarHandlerFinalY > self.scrMainHolder_do.h - self.scrHandler_do.h - 1){
						self.scrollBarHandlerFinalY = self.scrMainHolder_do.h - self.scrHandler_do.h - 1;
					}
					
					FWDAnimation.killTweensOf(self.scrHandler_do);
					FWDAnimation.killTweensOf(self.scrHandlerLines_do);
					if(animate){
						FWDAnimation.to(self.scrHandler_do, .4, {y:self.scrollBarHandlerFinalY, ease:Quart.easeOut});
						FWDAnimation.to(self.scrHandlerLines_do, .8, {y:self.scrollBarHandlerFinalY + parseInt((self.scrHandler_do.h - self.scrHandlerLinesN_do.h)/2), ease:Quart.easeOut});
					}else{
						self.scrHandler_do.setY(self.scrollBarHandlerFinalY);
						self.scrHandlerLines_do.setY(self.scrollBarHandlerFinalY + parseInt((self.scrHandler_do.h - self.scrHandlerLinesN_do.h)/2));
					}
				}
			}
			
			if(self.lastThumbnailFinalY != self.thumbnailsFinalY){
				FWDAnimation.killTweensOf(self.buttonsHolder_do);
				if(animate){
					FWDAnimation.to(self.buttonsHolder_do, .5, {y:self.thumbnailsFinalY, ease:Quart.easeOut});
				}else{
					self.buttonsHolder_do.setY(self.thumbnailsFinalY);
				}
			}
			
			self.lastThumbnailFinalY = self.thumbnailsFinalY;
		};
		
		
		
		
		this.init();
	};
	
	/* set prototype */
	FWDUVPComboBox.setPrototype =  function(){
		FWDUVPComboBox.prototype = new FWDUVPDisplayObject("div");
	};
	FWDUVPComboBox.OPEN = "open";
	FWDUVPComboBox.HIDE_COMPLETE = "infoWindowHideComplete";
	FWDUVPComboBox.BUTTON_PRESSED = "buttonPressed";
	FWDUVPComboBox.prototype = null;
	window.FWDUVPComboBox = FWDUVPComboBox;
	
}(window));