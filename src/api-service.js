import axios from 'axios';
import {AppSettings} from './config';

export class apiService {


    /** Get App Setting Information */
    getAppSettingInfo = () => {
        return axios.get(AppSettings.apiBase + 'userApi/site_settings');
    };
    getAppFooterSettingInfo = () => {
        return axios.post(AppSettings.apiBase + 'userApi/privacy-footer');
    };
    getApptermSettingInfo = () => {
        return axios.post(AppSettings.apiBase + 'userApi/terms-footer');
    };
    getApplegalSettingInfo = () => {
        return axios.post(AppSettings.apiBase + 'userApi/legal-footer');
    };
    getAppcommunitySettingInfo = () => {
        return axios.post(AppSettings.apiBase + 'userApi/community-footer');
    };
    getAppaboutSettingInfo = () => {
        return axios.post(AppSettings.apiBase + 'userApi/aboutsite-footer');
    };
    /** Signin */
    signIn = (SendData) => {
        return axios.post(AppSettings.apiBase + 'userApi/login', SendData);
    };

    /** SignUp */
    signUp = (SendData) => {
        return axios.post(AppSettings.apiBase + 'userApi/register', SendData);
    };
    // User id
    userIn = (SendData) => {
        return axios.post(AppSettings.apiBase + 'userApi/userlogin', SendData);
    };

    GetPage = (id) => {
        return axios.get(AppSettings.apiBase + 'userApi/getPage/' + id);
    };

    /** Logout */
    Logout = (SendData) => {
        return axios.post(AppSettings.apiBase + 'userApi/logout', SendData);
    };

    checkAuth = () => {
        return localStorage.getItem('logged_in') && localStorage.getItem('logged_in') === 'true';
    };
    checkUser = () => {
        return localStorage.getItem('logged_save') && localStorage.getItem('logged_save') === 'true';
    };

    /** Get User Detail */
    getUserDetail = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/userDetails', sessionStorage);
    };


    /** Get Profiles */
    getProfiles = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/active-profiles', sessionStorage);
    };

    /** Get Subscriptions */
    getSubscription = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/get-subscription', sessionStorage);
    };

    /** Get Single Subscription */
    getSingleSubscription = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/view-sub-profile', sessionStorage);
    };

    /** Edit Profile */
    sendProfileInfo = (formData) => {
        return axios.post(AppSettings.apiBase + 'userApi/edit-sub-profile', formData);
    };

    /** Add new profile */
    addNewProfile = (formData) => {
        return axios.post(AppSettings.apiBase + 'userApi/add-profile', formData);
    };
    /** Delete Profile */
    sendDeleteProfile = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/delete-sub-profile', {
            sub_profile_id: sessionStorage.profileId,
            id: sessionStorage.user_id,
            token: sessionStorage.access_token
        });
    };

    /** Get Static Pages for footer */
    getAllPages = () => {
        return axios.get(AppSettings.apiBase + 'userApi/allPages');
    };
    /** Set watch cound */
    setWatchCount = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/watchCount', sessionStorage);
    };


    getSingleVideo = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/singleVideo', sessionStorage);
    };

    ApplyCouponPpv = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/apply/coupon/ppv', sessionStorage);
    };


    Payppv = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/pay_ppv', sessionStorage);
    };


    Stripeppv = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/stripe_ppv', sessionStorage);
    };

    RedNotice = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/red-notifications', sessionStorage);
    };

    getActiveCategories = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/active-categories', sessionStorage);
    };
    notifications = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/notifications', sessionStorage);
    };
    // privacy = (sessionStorage) => {
    //     return axios.post(AppSettings.apiBase + 'userApi/privacy-footer', sessionStorage);
    // };
    getAllChannels = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/all_channels', sessionStorage);
    };

    getSingleChannel = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/single_channel', sessionStorage);
    };

    AddWishList = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/addWishlist', sessionStorage);
    };

    deleteWishlist = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/deleteWishlist', sessionStorage);
    };
    getHomeData = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/home', sessionStorage);
    };

    AddSpam = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/add_spam', sessionStorage);
    };
    GetSpam = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/getspam', sessionStorage);
    };
    DeleteSpam = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/removespam', sessionStorage);
    };
    likeVideo = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/like_video', sessionStorage);
    };

    dislikeVideo = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/dis_like_video', sessionStorage);
    };
    spamReasons = () => {
        return axios.get(AppSettings.apiBase + 'userApi/spam-reasons');
    };
    getVideoData = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/browse', sessionStorage);
    };

    GetGenreList=(sessionStorage)=> {
        return axios.post(AppSettings.apiBase+'userApi/genre_list', sessionStorage);
    }
    getDetails = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/details', sessionStorage);
    }

    SubscriptionIndex = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/subscription_index', sessionStorage);
    }
    PlanDetail = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/plan_detail', sessionStorage);
    }
    ApplyCouponSubscription = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/apply/coupon/subscription', sessionStorage);
    }
    ZeroPlan = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/zero_plan', sessionStorage);
    }
    StripePayment = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/stripe_payment', sessionStorage);
    }
    ChangePassword = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/changePassword', sessionStorage);
    }
    AcitvePlan = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/active_plan', sessionStorage);
    }
    EmailNotification = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/email/notification', sessionStorage);
    }
    PpvList = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/ppv_list', sessionStorage);
    }
    UpdateProfile = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/updateProfile', sessionStorage);
    }
    DeleteAccount = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/deleteAccount', sessionStorage);
    }
    SubscribedPlans = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/subscribed_plans', sessionStorage);
    }
    CancelSubscription = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/cancel/subscription', sessionStorage);
    }
    AutoRenewalEnable = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/autorenewal/enable', sessionStorage);
    }
    SearchVideo = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/searchVideo', sessionStorage);
    }
    deleteCard = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/delete_card', sessionStorage);
    }
    paymentAddCard = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/payment_card_add', sessionStorage);
    }
    forgotPassword = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/forgotpassword', sessionStorage);
    }
    cardDetails = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/card_details', sessionStorage);
    }
    defaultCard = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/default_card', sessionStorage);
    }
    CastAndCrewVideos = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/cast_crews/videos', sessionStorage);
    }
    ContinueWatchVideo = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/watching/video', sessionStorage);
    }
    ContinueWatchGetDuration = (sessionStorage) => {
        return axios.post(AppSettings.apiBase + 'userApi/watching/video/duration', sessionStorage);
    }
}
